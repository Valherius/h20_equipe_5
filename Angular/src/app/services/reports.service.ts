import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Mentor } from '../models/mentor.model';
import { Room } from '../models/room.model';
import { Book } from '../models/book.model';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { formatDate } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {

  private _mentors: Mentor[];
  private _rooms: Room[];
  private _books: Book[];

  constructor(private httpClient: HttpClient, private router:Router)
  { 
    this._mentors = [];
    this._rooms = [];
    this._books = [];
  }

  private getUrl(query: string) {
    return `/api/${query}.json`;
  }
  bookReport(start:string,end:string): Observable<any[]> 
  {
    return this.httpClient.post<any>(this.getUrl('bookReport'), {start,end}).pipe(
      map(response => {
        //console.log(response);
        //console.log(response.results);
        if(response.results)
        {
         return response.results;
        }
        else
        {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  mentorReport(start:string,end:string): Observable<any[]> 
  {
  
    return this.httpClient.post<any>(this.getUrl('mentorReport'), {start,end}).pipe(
      map(response => {
        //console.log(response.results);
        if(response.results)
        {
         return response.results;
        }
        else
        {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  roomReport(start:string,end:string): Observable<any[]> 
  {
  
    return this.httpClient.post<any>(this.getUrl('roomReport'), {start,end}).pipe(
      map(response => {
        //console.log(response.results);
        if(response.results)
        {
         return response.results;
        }
        else
        {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }


  

  get rooms(): Observable<Room[]> 
  {
    return this.select('rooms', {}).pipe(
      map(rooms =>{
        //console.log(rooms);
        return rooms.rooms;

      })
    );

  }

  get books(): Observable<Book[]> 
  {
    return this.select('books', {}).pipe(
      map(books =>{
        //console.log(books);
        return books.books;

      })
    );

  }

  select(query: string, body: any): Observable<any> {
    return this.httpClient.get<any>(this.getUrl(query), body).pipe(
      map(response => {
        if (response && response) {
          return response;

        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
}
}

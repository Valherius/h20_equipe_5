import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { stringify } from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
   currentUser: User;
   userName: string;
   password: string;

  constructor(private httpClient: HttpClient) { 
    this.currentUser = JSON.parse(localStorage.getItem("CU"));
  }

  logout(){
    this.currentUser = null;
    localStorage.setItem ("CU", null);
  }

  //***** */query: string, body: any
  login(username: string, password: string ): Observable<any> {
    this.userName = username;
    this.password = password;
    return this.httpClient.post<any>(this.getUrl('login'), { username, password }).pipe(
      map(data => {
        //console.log(data.isAdmin,data.userName);
        if (data.isAdmin!=34) {
          this.currentUser = new User(data.user.PK_id,data.user.username, data.user.firstname,data.user.lastname,data.user.address,data.user.image,data.user.isAdmin);
          localStorage.setItem("CU", JSON.stringify(this.currentUser));
          return this.currentUser;

        } else {
          this.currentUser = new User(data.user.PK_id,data.user.username, data.user.firstname,data.user.lastname,data.user.address,data.user.image,data.user.isAdmin);
          localStorage.setItem("CU", JSON.stringify(this.currentUser));
          return this.currentUser;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  private getUrl(query: string) {
    return `/api/${query}.json`;
  }


}

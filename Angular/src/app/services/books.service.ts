import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Book } from '../models/book.model';
import { AuthService } from './auth.service';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { SubjectService } from './subjects.service';
import { Subject } from '../models/subject.model';
import { Item } from '../models/item.model';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  private _books: Book[];
  private _items: Item[];
 


  constructor(private authService:AuthService,private router:Router,private httpClient:HttpClient) {

    this._books = [];
    this._items = [];
   
   }
   private getUrl(query: string) {
    return `/api/${query}.json`;
  }

   
  get books(): Observable<Book[]> 
  {
    return this.select('books', {}).pipe(
      map(books =>{
        //console.log(books);
        return books.books;

      })
    );

  }

  get items(): Observable<Item[]> 
  {
    return this.select('listB', {}).pipe(
      map(items =>{
        //console.log(items.items);
        return items.items;
      })
    );
  }


  create(formData: FormData): Observable<boolean>
  {
    //console.log(formData);
    return this.insert('books/add', formData).pipe(
      map(result => {
        //console.log(result);
        if(result)
        {
          return true; 
        }else{
          return null;
        }
      }
    ));
  }


  view(id: number):Book {
    //console.log(this.books);
    return this._books.find(i => i.PK_id === id);
  }


  get(id: number): Book {
    return this._books.find(i => i.PK_id === id);
  }


  delete(book: Book): Observable<boolean>
  {
    //console.log(book.PK_id);
    return this.deletebook('books/delete/'+book.PK_id,{}).pipe(
      map(success => {
        if(success)
        {
          return success;
        }else{
          return null;
        }
      })
    );
  }

  update(query:string,formData:FormData):Observable<any>
  {
    formData.forEach((key,value)=>{
      //console.log(`${key}:${value}`);
    });
  
    return this.httpClient.post<any>(this.getUrl(query), formData).pipe(
      map(response => {
        //console.log(response);
        if(response)
        {
          return response;
        }
        else
        {
          return null;
        }
      }
    )); 
  }

  updatebook(PK_id: number, formData:FormData): Observable<boolean> 
  {
    return this.update('books/edit/'+PK_id,formData).pipe(
      map(success => {
        //console.log(success);
        if(success)
        {
          return success;
        }
        else
        {
          return null;
        }
      })
    );
  }


  getbook(PK_id: number): Observable<Book>{
             
    return this.httpClient.post<any>(this.getUrl('books/view/'+PK_id), {}).pipe(
      map(response => {
        if(response)
        {
        //console.log(response);
          const bookData = response.book;
          //console.log(response.book);
          const b = new Book(bookData.PK_id, bookData.name, bookData.author, bookData.synopsis, bookData.image, bookData.published_date,bookData.subjects);
         // console.log(b);
          return b;
        }
        else
        {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  //------------------------------to api----------------------------------//

  search(){}

  select(query: string, body: any): Observable<any> {
    return this.httpClient.get<any>(this.getUrl(query), body).pipe(
      map(response => {
        if (response && response) {
          return response;

        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
}


insert(query: string, body: any): Observable<number> {
  //console.log(body);
  return this.httpClient.post<any>(this.getUrl(query), body).pipe(
    map(response => {
      //console.log(response.book)
      const bookId=response.book;
      if (response.book =="EC1") {
        return null;
      } else {
        let test=[1,2,3,4];
        this.httpClient.post<any>(this.getUrl('mentor-skills/add'), {bookId, test}).pipe(map(result => {console.log(result);}));
        return true;
      }
    }),
    catchError((error: any): Observable<any> => {
      console.error(error);
      return of(null);
    })
  );
}

deletebook(query: string, body: any): Observable<boolean> {
  return this.httpClient.post<any>(this.getUrl(query), body).pipe(
   map(response => {
      if (response) {
        return response;
      } else {
        return null;
      }
    }),
    catchError((error: any): Observable<any> => {
      console.error(error);
      return of(null);
    })
  );
}

  fullTextSearch(query:string,body:any): Observable<any>
  {

    return this.httpClient.post<any>(this.getUrl(query),body).pipe(
    
      map(response => {
        //console.log('searchservice', response);
        if (response) {
          return response;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );

  }

}

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Mentor } from '../models/mentor.model';
import { Tag } from '../models/tag.model';
import { Item } from '../models/item.model';


@Injectable({
  providedIn: 'root'
})
export class  MentorsService{

  private _mentors: Mentor[];
  private _items: Item[];
 

  constructor(private authService:AuthService,private router:Router,private httpClient:HttpClient) {
    this._mentors = [];
    this._items = [];
   }

   private getUrl(query: string) {
    return `/api/${query}.json`;
  }

  get mentors(): Observable<Mentor[]> 
  {
    return this.select('mentors', {}).pipe(
      map(mentors =>{
        //console.log(mentors);
        return mentors.mentors;
      })
    );

  }

  get items(): Observable<Item[]> 
  {
    return this.select('listM', {}).pipe(
      map(items =>{
        //console.log(items.items);
        return items.items;
      })
    );

  }
 

   create(formData: FormData): Observable<boolean>
  {
    //formData.append('skills',mentorValue.firstname);
    //formData['skills'].append({'_ids':[1,2,3,4]});
    return this.insert('mentors/add', formData).pipe(
      map(result => {
       // console.log(result);
        if(result)
        {
          return true;
        }else
        {
          return null;
        }
      }
    ));
  }

  view(id: number):Mentor {
    return this._mentors.find(i => i.PK_id === id);
  }

  get(id: number): Mentor {
    return this._mentors.find(i => i.PK_id === id);
  }

 
  delete(mentor: Mentor): Observable<boolean>
  {
    //console.log(mentor.PK_id);
    return this.deletementor('mentors/delete/'+mentor.PK_id,{}).pipe(
      map(success => {
        if(success)
        {
          return success;
        }else{
          return null;
        }
      })
    );
  }

///
update(query: string,PK_id: number, formdata: FormData): Observable<any> 
{
  return this.httpClient.post<any>(this.getUrl(query+PK_id), formdata).pipe(
    map(response => {
      //console.log(response);
      if(response)
      {
        return response;
      }
      else
      {
        return null;
      }
    }
  ));
}  
updatementor(PK_id: number, formdata: FormData): Observable<boolean> 
  {
    return this.update('mentors/edit/',PK_id, formdata).pipe(
      map(success => {
        //console.log(success);
        if(success)
        {
          return success;
        }
        else
        {
          return null;
        }
      })
    );
  }

  getmentor(PK_id: number): Observable<Mentor>{        
    return this.httpClient.post<any>(this.getUrl('mentors/view/'+PK_id), {}).pipe(
      map(response => {
        
        if(response)
        {
          const mentorData = response.mentor;
          const m = new Mentor(mentorData.PK_id, mentorData.firstname,mentorData.lastname,mentorData.contactNumber,mentorData.email,mentorData.image, mentorData.skills);
          //console.log(m);
          return m;
        }
        else
        {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  //------------------------------to api----------------------------------//

  search(){}

  select(query: string, body: any): Observable<any> {
    return this.httpClient.get<any>(this.getUrl(query), body).pipe(
      map(response => {
        if (response && response) {
          return response;

        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
}

insert(query: string, body: any): Observable<number> {
  return this.httpClient.post<any>(this.getUrl(query), body).pipe(
    map(response => {
      //console.log(response);
      const mentorId=response.mentor;
      if (response.mentor=="EC1") {
        return null;
      } else {
        this.httpClient.post<any>(this.getUrl('mentor-skills/add'), {mentorId}).pipe(map(result => {console.log(result);}));
        return true;
      }
    }),
    catchError((error: any): Observable<any> => {
      console.error(error);
      return of(null)
    })
  );
}

deletementor(query: string , body: any): Observable<boolean> {
  return this.httpClient.post<any>(this.getUrl(query), body).pipe(
    map(response => {
       if (response) {
         return response;
       } else {
         return null;
       }
     }),
     catchError((error: any): Observable<any> => {
       console.error(error);
       return of(null);
     })
   );
}


fullTextSearch(query:string,body:any): Observable<any>
{
  return this.httpClient.post<any>(this.getUrl(query),body).pipe(
  
    map(response => {
      //console.log('searchservice', response);
      if (response) {
        return response.mentors;
      } else {
        return null;
      }
    }),
    catchError((error: any): Observable<any> => {
      console.error(error);
      return of(null);
    })
  );

}

}

import { Injectable } from '@angular/core';
import { Tag } from '../models/tag.model';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { stringify } from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class TagsService {
tagtype :string;
private _tags: Tag[];

  constructor( private httpClient: HttpClient) 
  { }

  private getUrl(query: string) {
    return `/api/${query}.json`;
  }

  /*getskill(PK_id: number): Tag{
    return this._tags.find(i => i.PK_id === PK_id)
  }*/

  getskill(PK_id:number): Observable<Tag>{
    
    return this.httpClient.post<any>(this.getUrl('skills/view/'+PK_id), {}).pipe(
      map(response => {
        //(response);
        if(response)
        {
          const tagData = response.skill;
          const s = new Tag(tagData.PK_id, tagData.name, tagData.description);
          //console.log(s);
          return s;
        }
        else
        {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }
  
  get skills(): Observable<Tag[]> {
    return this.select('skills', { }).pipe(
      map(tags => {
        //console.log(tags);
        return tags;

      })
        
       
    );
  }
  



    select(query: string, body: any):Observable<any>
    {
      return this.httpClient.post<any>(this.getUrl(query), body).pipe(
        map(response => {
          //console.log(response.skills);
          if(response.skills!='EC1')
          {
            return response.skills;
          }
          else
          {
            return null;
          }
        }),
        catchError((error: any): Observable<any> => {
          console.error(error);
          return of(null);
        })
      );
    }
 
  createskills(name:string,description:string): Observable<Tag[]> {
    return this.create('skills/add', name, description).pipe(
      map(response => {
        //console.log(response);
        if(response.skill == 'EC1'){

          return null;
         
        } else{

           return response.skill;
        }
      })
    );
  }

  create(query: string,name: string, description: string): Observable<any> {
//Generic Create
    return this.httpClient.post<any>(this.getUrl(query), {name, description }).pipe(
      map(response => {
        //console.log(response);
        if(response)
        {
          return response;
        }
        else
        {
          return null;
        }
      }
    ));
  }


  update(query: string,PK_id: number, name: string, description: string): Observable<any> {
    //Generic Update
        return this.httpClient.post<any>(this.getUrl(query+PK_id), {PK_id, name, description }).pipe(
          map(response => {
            //console.log(response);
            if(response)
            {
              return response;
            }
            else
            {
              return null;
            }
          }
        ));
      }

      delete(query: string,PK_id: number): Observable<any> {
        //Generic delete
        return this.httpClient.post<any>(this.getUrl(query+PK_id), 
      {}).pipe(
          map(response => {
            //console.log(response);
            if(response)
            {
              return response;
            }
            else
            {
              return null;
            }
          }),
          catchError((error: any): Observable<any> => {
            console.error(error);
            return of(null);
          })
        );
          }

  updateskill(PK_id: number, name: string, description: string): Observable<boolean> 
  {
    return this.update('skills/edit/', PK_id, name, description).pipe(
      map(success => {
        if(success)
        {
          return success;
        }
        else
        {
          return null;
        }
      })
    );
  }

  deleteskill(PK_id:number): Observable<boolean>{
    return this.delete('skills/delete/', PK_id).pipe(
      map(success => {
        if(success)
        {
          return success;
        }
        else
        {
          return null;
        }
      })
    );
  }

 

////////////////////////////////subject part/////////////////////////////

  getsubject(PK_id:number): Observable<Tag>{
      
    return this.httpClient.post<any>(this.getUrl('subjects/view/'+PK_id), {}).pipe(
      map(response => {
       // console.log(response);
        if(response)
        {
          const tagData = response.subject;
          const s = new Tag(tagData.PK_id, tagData.name, tagData.description);
          //console.log(s);
          return s;
        }
        else
        {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }


  get subjects(): Observable<Tag[]> {
    return this.selectsub('subjects', { }).pipe(
      map(tags => {
        //console.log(tags);
        return tags;

      })
        
       
    );
  }

  selectsub(query: string, body: any):Observable<any>
    {
      return this.httpClient.post<any>(this.getUrl(query), body).pipe(
        map(response => {
         // console.log(response.subjects);
          if(response.subjects!='EC1')
          {
            return response.subjects;
          }
          else
          {
            return null;
          }
        }),
        catchError((error: any): Observable<any> => {
          console.error(error);
          return of(null);
        })
      );
    }

  createsubjects(name:string,description:string): Observable<Tag[]> {
    return this.create('subjects/add', name, description).pipe(
      map(response => {
        //console.log(response);
        if(response.subject == 'EC1'){
          return null;
        } else{
           return response.subject;
        }
      })   
    );
  }

  updatesubject(PK_id: number, name: string, description: string): Observable<boolean> 
  {
    return this.update('subjects/edit/', PK_id, name, description).pipe(
      map(success => {
        if(success)
        {
          return success;
        }
        else
        {
          return null;
        }
      })
    );
  }

  deletesubject(PK_id:number): Observable<boolean>{
    //console.log('Service');
    return this.delete('subjects/delete/', PK_id).pipe(
      map(success => {
        //console.log(success);
        if(success)
        {
          return success;
        }
        else
        {
          return null;
        }
      })
    );
  }

  fullTextSearchSubject(query:string,body:any): Observable<any>
  {
     return this.httpClient.post<any>(this.getUrl(query),body).pipe(
      map(response => {
        //console.log(response);
        if (response) {
          return response.subjects;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }


  ////////////////////////////////service part/////////////////////////////

  getservice(PK_id:number): Observable<Tag>{
      
    return this.httpClient.post<any>(this.getUrl('services/view/'+PK_id), {}).pipe(
      map(response => {
        //console.log(response);
        if(response)
        {
          const tagData = response.service;
          const s = new Tag(tagData.PK_id, tagData.name, tagData.description);
          //console.log(s);
          return s;
        }
        else
        {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }


  get services(): Observable<Tag[]> {
    return this.selectser('services', { }).pipe(
      map(tags => {
        //console.log(tags);
        return tags;
      })
    );
  }

  selectser(query: string, body: any):Observable<any>
    {
      return this.httpClient.post<any>(this.getUrl(query), body).pipe(
        map(response => {
         // console.log(response.services);
          if(response.services!='EC1')
          {
            return response.services;
          }
          else
          {
            return null;
          }
        }),
        catchError((error: any): Observable<any> => {
          console.error(error);
          return of(null);
        })
      );
    }

  createservices(name:string,description:string): Observable<Tag[]> {
    return this.create('services/add', name, description).pipe(
      map(response => {
        //console.log(response);
        if(response.service =='EC1'){
          return false;
        }else{
          return true;
        }
        return response;
      })
    );
  }

  fullTextSearchService(query:string,body:any): Observable<any>
  {

    return this.httpClient.post<any>(this.getUrl(query),body).pipe(
    
      map(response => {
        if (response) {
          return response.services;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );


  }

  updateservice(PK_id: number, name: string, description: string): Observable<boolean> 
  {
    return this.update('services/edit/', PK_id, name, description).pipe(
      map(success => {
        if(success)
        {
          return success;
        }
        else
        {
          return null;
        }
      })
    );
  }

  deleteservice(PK_id:number): Observable<boolean>{
    if(this.services.pipe(map(rooms => rooms.length)))
    {
      return this.delete('services/delete/', PK_id).pipe(
        map(success => {
          if(success)
          {
            return success;
          }
          else
          {
            return null;
          }
        })
      );
    }

  }


  fullTextSearchSkills(query:string,body:any): Observable<any>
  {

    return this.httpClient.post<any>(this.getUrl(query),body).pipe(
    
      map(response => {
        if (response) {
          return response.skills;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );


  }

}


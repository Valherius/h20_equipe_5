import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Room } from '../models/room.model';
import { AuthService } from './auth.service';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Item } from '../models/item.model';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  private _rooms: Room[];
  private _items: Item[];

  constructor(private authService:AuthService,private router:Router,private httpClient:HttpClient)
  {
    this._rooms = [];
    this._items = [];
  }

  private getUrl(query: string) {
    return `/api/${query}.json`;
  }

  get rooms(): Observable<Room[]> 
  {
    return this.select('rooms', {}).pipe(
      map(rooms =>{
        return rooms.rooms;
      })
    );

  }

  get items(): Observable<Item[]> 
  {
    return this.select('listR', {}).pipe(
      map(items =>{
        //console.log(items.items);
        return items.items;
      })
    );
  }

  create(formData: FormData): Observable<boolean>
  {
    return this.insert('rooms/add', formData).pipe(
      map(result => {
        //console.log(result);
        if(result)
        {
          return true;
        }else
        {
          return null;
        }
      }
    ));
  }

  roomsg(PK_id: number): Observable<Room>{
    return this.httpClient.post<any>(this.getUrl('rooms/view/'+PK_id), {}).pipe(
      map(response => {
        //console.log(response);
        if(response)
        {
          const roomData = response.room;
          const room = new Room(roomData.PK_id, roomData.roomIdentifier, roomData.location, roomData.size, roomData.image, roomData.services);
          //console.log(room);
          return room;
        }
        else
        {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  get(id: number): Room {
    //console.log(this._rooms);
    return this._rooms.find(i => i.PK_id === id);
  }

  deleteroom(query: string, body: any): Observable<boolean> {
    //console.log(body);
    return this.httpClient.post<any>(this.getUrl(query), body).pipe(
     map(response => {
        if (response) {
          return response;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  update(query: string, formData: FormData): Observable<any> 
  { 
    formData.forEach((key,value)=>{
      //console.log(`${key}:${value}`);
    });
    return this.httpClient.post<any>(this.getUrl(query), formData).pipe(
      map(response => {
        if(response)
        {
          //console.log(response);
          return response;
        }
        else
        {
          return null;
        }
      }
    ));
  } 

  updateroom(PK_id: number, formData: FormData): Observable<boolean> 
  {
    return this.update('rooms/edit/'+ PK_id, formData).pipe(
      map(success => {
        if(success)
        {
          
          return success;
        }
        else
        {
          return null;
        }
      })
    );
  }

  //------------------------------to api----------------------------------//

  search(){}

  select(query: string, body: any): Observable<any> {
    return this.httpClient.get<any>(this.getUrl(query), body).pipe(
      map(response => {
        if (response && response) {
          return response;

        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }


  insert(query: string, body: any): Observable<number> {
    //console.log(body);
    return this.httpClient.post<any>(this.getUrl(query), body).pipe(
      map(response => {
        //console.log(response.room);
        const roomID=response.room;
        if (response.room == "EC1") {
          return null;
        } else {
          
          this.httpClient.post<any>(this.getUrl('room-services/add'), {roomID}).pipe(map(result => {console.log(result);}));
          return true;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  fullTextSearch(query:string,body:any): Observable<any>
  {
    console.log(query);
    return this.httpClient.post<any>(this.getUrl(query),body).pipe(
      map(response => {
        console.log(response);
        if (response) {
          return response;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );

  }

  delete(room: Room): Observable<boolean>
    {
      //console.log(room.PK_id);
      return this.deleteroom('rooms/delete/'+room.PK_id, {}).pipe(
        map(success => {
          if(success)
          {
            return success;
          }else{
            return null;
          }
        })
      );
    }
}

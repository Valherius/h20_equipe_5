import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Loan } from '../models/loan.model';

@Injectable({
  providedIn: 'root'
})
export class LoansService {

    loantype: string;
    private _loans : Loan[];
    idIncrement : number;

    constructor(private authService:AuthService,private router:Router,private httpClient:HttpClient) {

    this._loans= [];
    }

    private getUrl(query: string) {
      return `/api/${query}.json`;
    }

    
    get loans(): Observable<Loan[]> 
    {
      return this.select('renting', {}).pipe(
        map(loans =>{
          //console.log("a");
         // console.log(loans);
          return loans.renting;

        })
      );

    }

    create(item_type:number, FK_item_id:number, FK_user_id:number, rent_beginDate:string, rent_endDate:string): Observable<boolean>
    {

      return this.insert('renting/add', {item_type,FK_item_id,FK_user_id,rent_beginDate,rent_endDate}).pipe(
        map(result => {
          //console.log(result);
          return result;
        }
      ));
    }

    view(id: number):Loan {
      //console.log(this.loans);
      return this._loans.find(i => i.FK_user_id === id);
    }


    get(id: number): Loan {
     // console.log("test", this._loans)
      return this._loans.find(i => i.FK_user_id === id);
    }
  
  
    delete(loan: Loan): Observable<boolean>
    {
      //console.log(loan.PK_rent_id);
      return this.deleteloan('renting/delete/'+loan.PK_rent_id,{}).pipe(
        map(success => {
          if(success)
          {
            return success;
          }else{
            return null;
          }
        })
      );
    }

    return(loan: Loan): Observable<boolean>
    {
      return this.deleteloan('renting/return/'+loan.PK_rent_id,{}).pipe(
        map(success => {
          //console.log(success);
          if(success)
          {
            return success;
          }
          else
          {
            return null;
          }
        })
      );
    }

    setIsPaid(loan: Loan): Observable<boolean>
    {
      return this.deleteloan('renting/setIsPaid/'+loan.PK_rent_id,{}).pipe(
        map(success => {
          //console.log(success);
          if(success)
          {
            return success;
          }
          else
          {
            return null;
          }
        })
      );
    }
  
    update(query:string,PK_rent_id: number, item_type: number,FK_item_id:number, FK_user_id: number,rent_beginDate:Date, rent_endDate:Date, rent_returnDate:Date, fine:string):Observable<any>
    {
    
      return this.httpClient.post<any>(this.getUrl(query+PK_rent_id), {PK_rent_id,FK_item_id,FK_user_id,rent_beginDate,rent_endDate,rent_returnDate,fine}).pipe(
        map(response => {
          //console.log(response);
          if(response)
          {
            return response;
          }
          else
          {
            return null;
          }
        }
      )); 
    }
  
    updateloan(query:string,PK_rent_id: number, item_type: number,FK_item_id:number, FK_user_id: number,rent_beginDate:Date, rent_endDate:Date, rent_returnDate:Date, fine:string): Observable<boolean> 
    {
      return this.update('renting/edit/',PK_rent_id,FK_item_id,item_type,FK_user_id,rent_beginDate,rent_endDate,rent_returnDate,fine).pipe(
        map(success => {
          //console.log(success);
          if(success)
          {
            return success;
          }
          else
          {
            return null;
          }
        })
      );
    }
  
  
    getloan(FK_user_id: number): Observable<Loan>{
           
      return this.httpClient.post<any>(this.getUrl('renting/view'+FK_user_id), {}).pipe(
        map(response => {
          if(response)
          {
            const loanData = response.loan;
            const l = new Loan(loanData.PK_rent_id, loanData.item_type,  loanData.FK_item_id, loanData.FK_user_id, loanData.rent_beginDate, loanData.rent_endDate, loanData.rent_returnDate,loanData.fine);
            //console.log(l);
            return l;
          }
          else
          {
            return null;
          }
        }),
        catchError((error: any): Observable<any> => {
          console.error(error);
          return of(null);
        })
      );
    }

    getloans(FK_user_id: Number): Observable<Loan[]>{
           
      return this.httpClient.post<any>(this.getUrl('urenting/'+FK_user_id), {}).pipe(
        map(response => {
          if(response.urenting)
          {console.log(response.urenting);
            return response.urenting;
          }
          else
          {
            return null;
          }
        }),
        catchError((error: any): Observable<any> => {
          console.error(error);
          return of(null);
        })
      );
    }
  
    //------------------------------to api----------------------------------//
  
    search(){}
  
    select(query: string, body: any): Observable<any> {
      //console.log(query);
      return this.httpClient.get<any>(this.getUrl(query), body).pipe(
        map(response => {
          if (response) {
            return response;
  
          } else {
            return null;
          }
        }),
        catchError((error: any): Observable<any> => {
          console.error(error);
          return of(null);
        })
      );
  }
  
  
  insert(query: string, body: any): Observable<boolean> {
    //console.log(body);
    return this.httpClient.post<any>(this.getUrl(query), body).pipe(
      map(response => {
        //console.log(response.renting)
        if (response.renting =="SC1") {
          return true;
        } else {
          return false;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }
  

  deleteloan(query: string, body: any): Observable<boolean> {
    return this.httpClient.post<any>(this.getUrl(query), body).pipe(
     map(response => {
        if (response) {
          return response;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }


  getselectedloans(PK_id:number,item_type:number): Observable<Loan[]>
  {
      return this.httpClient.post<any>(this.getUrl('irenting'), {item_type,PK_id}).pipe(
      map(response => {
        console.log(response);
         if (response) 
         {
           return response.irenting;
         } 
         else 
         {
           return null;
         }
        }
      ));

  }

}








   

   
  
   
 


 


 



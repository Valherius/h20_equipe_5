import { Injectable } from '@angular/core';
import {  Observable, of} from 'rxjs';
import { AuthService } from './auth.service';
import { map, catchError } from 'rxjs/operators';
import { Subject } from '../models/subject.model';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class SubjectService {
 
 

  private _subject:Subject[];
  private idIncrement: number;
 


  get subjects(): Observable<Subject[]> {
    return this.select('subjects', {}).pipe(
      map(subject => subject || [])
    )
  }


  constructor(private authService:AuthService,private router: Router,private httpClient: HttpClient ) {

    this._subject = JSON.parse(localStorage.getItem('subjects')) || [];
    this.idIncrement = +localStorage.getItem('AI') || 0;

   }

   create(id:number, name:string, description:string): Observable<Subject> {
    return this.insert('subject-add', {id,name,description}).pipe(
      map(id => {
        if (id) {
          return new Subject( id,name, description);
        } else {
          return null;
        }
      })
    );
  }


  view(id: number): Subject {
    return this._subject.find(i => i.PK_id === id);
  }

  delete(subject: Subject): Observable<boolean> {
    return this.deletesubject('subject-delete', {'id': subject.PK_id});
  }

 
  
  update(id:number,name: string, description: string,){
   // this.router.navigate(['/subjects/']);
    return this.insert('update', { id,name, description}).pipe(
      map(item=>{
        if(item){
          return new Subject( id, name, description );
        }
      })
    )      
  }
  //------------------------- to api------------------------------------//

  search(){}

    select(query: string, body: any): Observable<any> {
      return this.httpClient.get<any>(this.getUrl(query), body).pipe(
        map(response => {
          if (response && response) {
            return response;

          } else {
            return null;
          }
        }),
        catchError((error: any): Observable<any> => {
          console.error(error);
          return of(null);
        })
      );
  }
  private getUrl(query: string) {
    return `/api/${query}.json`;
  }

  insert(query: string, body: any): Observable<number> {
    return this.httpClient.post<any>(this.getUrl(query), body).pipe(
      map(response => {
        if (response && response.lastInsertId) {
          return response.lastInsertId;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  deletesubject(query: string, body: any): Observable<boolean> {
    return this.httpClient.post<any>(this.getUrl(query), body).pipe(
      map(response => {
        if (response) {
          return response;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

}

import { Injectable } from '@angular/core';
import { User } from '../models/user.model';
import { Observable, of } from 'rxjs';
import { AuthService } from './auth.service';
import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private _users: User[];
  idIncrement: number;

  constructor(private authService: AuthService,private httpClient: HttpClient) 
  {
    this._users = [];
  }

  get users(): Observable<User[]>
  {   
   
    return this.select('users', {}).pipe(
      map(users =>{ 
        //console.log(users);
        return users.users;

      })
    );
  }

  usersg(PK_id: number): Observable<User>{
    return this.httpClient.post<any>(this.getUrl('users/view/'+PK_id), {}).pipe(
      map(response => {
        //console.log(response);
        if(response)
        {
          const userData = response.user;
          const u = new User(userData.PK_id, userData.username, userData.firstname, userData.lastname, userData.address, userData.image, userData.isAdmin);
          //console.log(u);
          return u;
        }
        else
        {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  get(id: number): User {
    //console.log(this._users);
    return this._users.find(i => i.PK_id === id);
  }

  create(formData: FormData): Observable<boolean>
  {
    return this.insert('users/add', formData).pipe(
      map(result => {
        //console.log(result);
        if(result)
        {
          return true; 
        }else{
          return null;
        }
      }
    ));
  }

  update(query: string,PK_id: number, username: string, password: string, firstname: string, lastname: string, address: string, image: string, isAdmin:boolean): Observable<any> 
  {
    return this.httpClient.post<any>(this.getUrl(query+PK_id), {PK_id, username,password, firstname, lastname, address, image, isAdmin }).pipe(
      map(response => {
        //console.log(response);
        if(response)
        {
          return response;
        }
        else
        {
          return null;
        }
      }
    ));
  }  

  delete(user: User): Observable<boolean>
  {
    //console.log(user.PK_id);
    return this.deleteuser('users/delete/'+user.PK_id, {}).pipe(
      map(success => {
        if(success)
        {
          return success;
        }else{
          return null;
        }
      })
    );
  }

//----------------------------for api--------------------//
getuser(PK_id:number): Observable<User>
  {   
   
    return this.select('users/view/'+PK_id, {}).pipe(
      map(response =>{ 
       // console.log(response);
        if(response)
        {
          //console.log(response);
          const userData = response.user;
          const u= new User(userData.PK_id, userData.username, userData.firstname, userData.lastname, userData.address, userData.image, userData.isAdmin);
          return u;
        }
        else{return null;}
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

    select(query: string, body: any): Observable<any> {
      return this.httpClient.get<any>(this.getUrl(query), body).pipe(
        map(response => {
          if (response && response) {
            return response;

          } else {
            return null;
          }
        }),
        catchError((error: any): Observable<any> => {
          console.error(error);
          return of(null);
        })
      );
  }
  private getUrl(query: string) {
    return `/api/${query}.json`;
  }

  insert(query: string, body: any): Observable<number> {
    return this.httpClient.post<any>(this.getUrl(query), body).pipe(
      map(response => {
        if (response.user=="SC1") {
          return true;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null)
      })
    );
  }

  updateuser(PK_id: number, username: string, password: string, firstname: string, lastname: string, address: string, image: string, isAdmin:boolean): Observable<boolean> 
  {
    return this.update('users/edit/', PK_id, username,password, firstname, lastname, address, image, isAdmin).pipe(
      map(success => {
        if(success)
        {
          return success;
        }
        else
        {
          return null;
        }
      })
    );
  }

  deleteuser(query: string, body: any): Observable<boolean> {
    return this.httpClient.post<any>(this.getUrl(query), body).pipe(
     map(response => {
        if (response) {
          return response;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );
  }

  fullTextSearch(query:string,body:any): Observable<any>
  {

    return this.httpClient.post<any>(this.getUrl(query),body).pipe(
    
      map(response => {
        //console.log('searchUserservice', response);
        if (response) {
          return response;
        } else {
          return null;
        }
      }),
      catchError((error: any): Observable<any> => {
        console.error(error);
        return of(null);
      })
    );


  }
}

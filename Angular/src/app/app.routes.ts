import { Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { UserViewComponent } from './components/user-view/user-view.component';
import { AuthGuardService } from './services/auth-guard.service';
import { UsersComponent } from './components/users/users.component';
import { UserLoansComponent } from './components/user-loans/user-loans.component';
import { SkillListComponent } from './components/skill-list/skill-list.component';
import { SkillViewComponent } from './components/skill-view/skill-view.component';
import { SkillAddComponent } from './components/skill-add/skill-add.component';
import { SubjectListComponent } from './components/subject-list/subject-list.component';
import { SubjectAddComponent } from './components/subject-add/subject-add.component';
import { SubjectViewComponent } from './components/subject-view/subject-view.component';
import { ServiceListComponent } from './components/service-list/service-list.component';
import { ServiceAddComponent } from './components/service-add/service-add.component';
import { ServiceViewComponent } from './components/service-view/service-view.component';
import { UserAddComponent } from './components/user-add/user-add.component';
import { MentorAddComponent } from './components/mentor-add/mentor-add.component';
import { MentorListComponent } from './components/mentor-list/mentor-list.component';
import { MentorViewComponent } from './components/mentor-view/mentor-view.component';
import { BookAddComponent } from './components/book-add/book-add.component';
import { BookViewComponent } from './components/book-view/book-view.component';
import { RoomAddComponent } from './components/room-add/room-add.component';
import { RoomViewComponent } from './components/room-view/room-view.component';
import { BooksListComponent } from './components/book-list/book-list.component';
import { RoomsListComponent } from './components/room-list/room-list.component';
import { GenerateReportComponent } from './components/generate-report/generate-report.component';
import { RoomReportComponent } from './components/room-report/room-report.component';
import { LoanAddComponent } from './components/loan-add/loan-add.component';

export const routes: Routes = [
    
    { path: '', component: LoginComponent },

    { path: '', canActivate: [AuthGuardService], children: [
        { path: 'users', component: UsersComponent },
        { path: 'users/add', component: UserAddComponent },
        { path: 'users/loans', component: UserLoansComponent },
        { path: 'users/loans/:id', component: UserLoansComponent },
        { path: 'users/newloan/:id', component: LoanAddComponent },
        { path: 'users/:id', component: UserViewComponent },
        { path: 'skills', component: SkillListComponent},       
        { path: 'skills/add', component: SkillAddComponent},
        { path: 'skills/:id', component: SkillViewComponent},
        { path: 'subjects', component: SubjectListComponent},       
        { path: 'subjects/add', component: SubjectAddComponent},
        { path: 'subjects/:id', component: SubjectViewComponent},
        { path: 'services', component: ServiceListComponent},       
        { path: 'services/add', component: ServiceAddComponent},
        { path: 'services/:id', component: ServiceViewComponent},
        { path: 'mentors', component: MentorListComponent},       
        { path: 'mentors/add', component: MentorAddComponent},
        { path: 'mentors/:id', component: MentorViewComponent},
        { path: 'books', component: BooksListComponent},       
        { path: 'books/add', component: BookAddComponent},
        { path: 'books/:id', component: BookViewComponent},
        { path: 'rooms', component: RoomsListComponent},       
        { path: 'rooms/add', component: RoomAddComponent},
        { path: 'rooms/:id', component: RoomViewComponent},
        { path: 'reports', component: GenerateReportComponent},
        { path: 'reports/rooms', component: RoomReportComponent},
    ]},
  

    { path: '**', component: NotFoundComponent }
];
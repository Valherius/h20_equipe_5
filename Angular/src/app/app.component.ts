import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
declare const $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent 
{
  title = 'Marbles';
  constructor(public translate: TranslateService) 
  {
    translate.addLangs(['english', 'francais']);
    translate.setDefaultLang('english');
    translate.use('english');
  }
}

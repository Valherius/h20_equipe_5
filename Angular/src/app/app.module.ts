import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HttpClient, HttpClientXsrfModule } from "@angular/common/http";
import { TranslateModule, TranslateLoader, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NumberDirective } from './numbers-only.directive';
import { routes } from './app.routes';

import { DatePipe } from '@angular/common'
import { AppComponent } from './app.component';
import { UserAddComponent } from './components/user-add/user-add.component';
import { UserLoansComponent } from './components/user-loans/user-loans.component';
import { GeneralNavbarComponent } from './components/general-navbar/general-navbar.component';
import { BookNavbarComponent } from './components/book-navbar/book-navbar.component';
import { MentorNavbarComponent } from './components/mentor-navbar/mentor-navbar.component';
import { BookViewComponent } from './components/book-view/book-view.component';
import { SubjectNavbarComponent } from './components/subject-navbar/subject-navbar.component';
import { SkillNavbarComponent } from './components/skill-navbar/skill-navbar.component';
import { RoomNavbarComponent } from './components/room-navbar/room-navbar.component';
import { ServiceNavbarComponent } from './components/service-navbar/service-navbar.component';
import { LoginComponent } from './components/login/login.component';
import { SubjectListComponent } from './components/subject-list/subject-list.component';
import { SubjectViewComponent } from './components/subject-view/subject-view.component';
import { BookAddComponent } from './components/book-add/book-add.component';
import { SubjectAddComponent } from './components/subject-add/subject-add.component';
import { RoomViewComponent } from './components/room-view/room-view.component';
import { RoomsListComponent } from './components/room-list/room-list.component';
import { ServiceListComponent } from './components/service-list/service-list.component';
import { RoomAddComponent } from './components/room-add/room-add.component';
import { ServiceAddComponent } from './components/service-add/service-add.component';
import { ServiceViewComponent } from './components/service-view/service-view.component';
import { MentorListComponent } from './components/mentor-list/mentor-list.component';
import { MentorViewComponent } from './components/mentor-view/mentor-view.component';
import { MentorAddComponent } from './components/mentor-add/mentor-add.component';
import { SkillListComponent } from './components/skill-list/skill-list.component';
import { SkillViewComponent } from './components/skill-view/skill-view.component';
import { SkillAddComponent } from './components/skill-add/skill-add.component';
import { UserViewComponent } from './components/user-view/user-view.component';
import { BooksListComponent } from './components/book-list/book-list.component';
import { RoomsComponent } from './components/rooms/rooms.component';
import { MentorsComponent } from './components/mentors/mentors.component';
import { ServicesCComponent } from './components/services-c/services-c.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { SkillRowComponent } from './components/skill-row/skill-row.component';
import { UsersComponent } from './components/users/users.component';
import { UserRowComponent } from './components/user-row/user-row.component';
import { SubjectRowComponent } from './components/subject-row/subject-row.component';
import { UserNavbarComponent } from './components/user-navbar/user-navbar.component';
import { ServiceRowComponent } from './components/service-row/service-row.component';
import { BookRowComponent } from './components/book-row/book-row.component';
import { MentorRowComponent } from './components/mentor-row/mentor-row.component';
import { RoomRowComponent } from './components/room-row/room-row.component';
import { RoomReportComponent } from './components/room-report/room-report.component';
import { GenerateReportComponent } from './components/generate-report/generate-report.component';

import { ModalComponent } from './components/modal/modal.component'

import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { BooksService } from './services/books.service';
import { MentorsService } from './services/mentors.service';
import { RoomsService } from './services/rooms.service';
import { TagsService } from './services/tags.service';
import {Ng2SearchPipeModule } from 'ng2-search-filter';
import { ReportsService } from './services/reports.service';
import { MentorReportRowComponent } from './components/mentor-report-row/mentor-report-row.component';
import { UserLoansRowComponent } from './components/user-loans-row/user-loans-row.component';
import { LoanAddComponent } from './components/loan-add/loan-add.component';
import { BookReportRowComponent } from './components/book-report-row/book-report-row.component';
import { RoomReportRowComponent } from './components/room-report-row/room-report-row.component';
import {BrowserAnimationsModule}  from '@angular/platform-browser/animations';
import { ItemRowComponent } from './components/item-row/item-row.component';
import { LoanRowComponent } from './components/loan-row/loan-row.component';
import { AlertDialogComponent } from './components/alert-dialog/alert-dialog.component';
import { BasicDialogComponent } from './components/basic-dialog/basic-dialog.component';

import { MatDialogModule, MatFormFieldModule, MatInputModule, MatSelectModule, MatToolbarModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';



export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    
    LoanRowComponent,
    ModalComponent,
    GeneralNavbarComponent,
    BookNavbarComponent,
    RoomNavbarComponent,
    MentorNavbarComponent,
    BookViewComponent,
    SubjectNavbarComponent,
    SubjectViewComponent,
    MentorNavbarComponent,
    SkillNavbarComponent,
    RoomNavbarComponent,
    ServiceNavbarComponent,
    LoginComponent,
    BookViewComponent,
    SubjectListComponent,
    SubjectViewComponent,
    BookAddComponent,
    SubjectAddComponent,
    RoomsComponent,
    RoomViewComponent,
    RoomAddComponent,
    ServiceListComponent,
    ServiceViewComponent,
    ServiceAddComponent,
    MentorListComponent,
    MentorViewComponent,
    MentorAddComponent,
    SkillListComponent,
    SkillViewComponent,
    SkillAddComponent,
    UserViewComponent,
    UserAddComponent,
    UserLoansComponent,
    BooksListComponent,
    RoomsListComponent,
    MentorsComponent,
    ServicesCComponent,
    UsersComponent,
    NotFoundComponent,
    SkillRowComponent,
    UserRowComponent,
    SubjectRowComponent,
    UserNavbarComponent,
    ServiceRowComponent,
    BookRowComponent,
    MentorRowComponent,
    RoomRowComponent,
    UserLoansComponent,
    RoomReportComponent,
    GenerateReportComponent,
    NumberDirective,
    MentorReportRowComponent,
    UserLoansRowComponent,
    LoanAddComponent,
    BookReportRowComponent,
    ItemRowComponent,
    RoomReportRowComponent,
    AlertDialogComponent,
    BasicDialogComponent
    
   

  ],
  
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatToolbarModule,
    Ng2SearchPipeModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
          useFactory: HttpLoaderFactory, 
          deps: [HttpClient]
        }
      }),
      
    HttpClientXsrfModule.withOptions({
      cookieName: 'csrfToken',    // Cookie recu
      headerName: 'X-CSRF-Token', // Header attendu par le backend
      }),
    BrowserAnimationsModule,
  ],
  entryComponents: [
    AlertDialogComponent,
    BasicDialogComponent
  ],
  providers: [AuthService,DatePipe, AuthGuardService,ReportsService, BooksService, MentorsService, RoomsService, TranslateService, TagsService, ReportsService],
  bootstrap: [AppComponent]
})
export class AppModule { }

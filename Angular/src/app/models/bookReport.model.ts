

export class BookReport {

    name: string;
    author:string;
    totalrent:number;
    lastrent:Date;
    Gtotal:number;
    LastRentDate:Date;
    
  
  
    
  
    constructor(PK_id: number,name: string,author:string,  totalrent:number, lastRent:Date, Gtotal:number, LastRentDate:Date) {
      this.name = name;
      this.author = author;
      this.totalrent = totalrent;
      this.lastrent = lastRent;
      this.Gtotal= Gtotal;
      this.LastRentDate=LastRentDate;
    }
  
  }
export class Item {
    PK_id: number;
    name: string;
    info1: string;
    info2: string;
  
    constructor(PK_id: number, name:string) {
      this.PK_id = PK_id;
      this.name = name;
 
    }
  
  }
  
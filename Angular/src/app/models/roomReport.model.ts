export class RoomReport {
    roomIdentifier: string;
    location: string;
    size: string;
    nbTimesLent: Number;
    percent: Number;
    total: Number;

    
  
    constructor(roomIdentifier: string,location:string,size:string, nbTimesLent: number, percent:number, total: number) {
      this.roomIdentifier = roomIdentifier;
      this.location = location;
      this.size = size;
      this.nbTimesLent = nbTimesLent;
      this.percent = percent;
      this.total = total;
    }
  }
  
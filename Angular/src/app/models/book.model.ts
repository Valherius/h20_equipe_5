import { Subject } from './subject.model';

export class Book {
    PK_id: number;
    name: string;
    author: string;
    synopsis: string;
    image: string;
    createdAt:Date;
    modifiedAt:Date;
    deletedAt:Date;
    published_date:String;
    subjects: Subject[];
  
    
  
    constructor(PK_id: number, name: string,author:string, synopsis: string,image:string, published_date:String, subjects: Subject[]) {
      this.PK_id= PK_id;
      this.name = name;
      this.author = author;
      this.synopsis = synopsis;
      this.image = image;
      this.published_date = published_date;
      this.subjects = subjects;
    }
  
  }
  
export class Service {
    PK_id: number;
    name:string;
    description:string;  
    createdAt:Date;
    modifiedAt:Date;
   
    
  
    constructor(PK_id: number,name:string ,description: string, createdAt:Date,modifiedAt:Date) {
      this.PK_id = PK_id;
      this.name = name;
      this.description= description;
      this.createdAt = createdAt;
      this.modifiedAt = modifiedAt;
    }
  
  }
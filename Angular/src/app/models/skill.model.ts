export class skill {
    PK_id: number;
    name:string;
    description:string;  
    createdAt:Date;
    modifiedAt:Date;
   
    
  
    constructor(id: number,name:string ,description: string, createdAt:Date,modifiedAt:Date) {
      this.PK_id = id;
      this.name = name;
      this.description= description;
      this.createdAt = createdAt;
      this.modifiedAt = modifiedAt;
     
     
    }
  
  }
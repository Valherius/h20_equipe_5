import { skill } from './skill.model';

export class Mentor {
    PK_id: number;
    firstname: string;
    lastname: string;
    contactNumber: string;
    email: string;
    image: string;
    skills: skill[];

    
  
    constructor(PK_id: number,firstname:string,lastname:string,contactNumber:string, email: string, image:string, skills:skill[]) {
      this.PK_id = PK_id;
      this.firstname = firstname;
      this.lastname = lastname;
      this.contactNumber = contactNumber;
      this.email = email;
      this.image = image;  
      this.skills = skills   
    }
  
  }
  
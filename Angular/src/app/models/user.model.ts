export class User {
    PK_id: number;
    username: string;
    firstname: string;
    lastname: string;
    address: string;
    image: string;
    isAdmin: boolean;
    

    constructor(PK_id: number,username: string,firstname:string,lastname:string,address:string,image:string,isAdmin:boolean) {
      this.PK_id = PK_id;
      this.username = username; 
      this.firstname= firstname;
      this.lastname=lastname;
      this.address=address;
      this.image = image;
      this.isAdmin= isAdmin;
    }
  }
  
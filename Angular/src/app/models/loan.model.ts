import { Book } from './book.model';
import { Mentor } from './mentor.model';
import { Room } from './room.model';
import { User } from './user.model';

export class Loan{
    PK_rent_id: number;
    item_type:number;
    FK_item_id:number;
    FK_user_id:number;
    rent_beginDate:Date;
    rent_endDate:Date;
    rent_returnDate:Date;
    fine:string;
    room:Room;
    mentor:Mentor;
    book:Book;
    user:User;
   
  
    constructor(PK_rent_id: number, item_type: number,FK_item_id:number, FK_user_id: number,rent_beginDate:Date, rent_endDate:Date, rent_returnDate:Date, fine:string) {
      this.PK_rent_id = PK_rent_id;
      this.item_type = item_type;
      this.FK_item_id = FK_item_id;
      this.FK_user_id = FK_user_id;
      this.rent_beginDate = rent_beginDate;
      this.rent_beginDate = rent_beginDate;
      this.rent_returnDate = rent_returnDate;
      this.fine = fine;
    }
  }
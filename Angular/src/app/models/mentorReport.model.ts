export class MentorReport {
    firstname: string;
    lastname: string;
    total: Number;
    average: Number;
    min: Number;
    max: Number;

    
  
    constructor(firstname: string,lastname:string,total:number, average: number, min:number, max:number) {
      this.firstname = firstname;
      this.lastname = lastname;
      this.total = total;
      this.average = average;
      this.min = min;
      this.max = max 
    }
  }
  
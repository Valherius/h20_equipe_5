import { Service } from './service.model';

export class Room {
    PK_id: number;
    roomIdentifier: string;
    location: string;
    size: string;
    image: string;
    services: Service[];
 
    
  
    constructor(PK_id: number, roomIdentifier:string, location: string, size:string, image:string, services:Service[]) {
      this.PK_id = PK_id;
      this.roomIdentifier = roomIdentifier;
      this.location= location;
      this.size =size;
      this.image = image;
      this.services = services; 
    }
  
  }
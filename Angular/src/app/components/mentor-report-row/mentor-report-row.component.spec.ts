import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorReportRowComponent } from './mentor-report-row.component';

describe('MentorReportRowComponent', () => {
  let component: MentorReportRowComponent;
  let fixture: ComponentFixture<MentorReportRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorReportRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorReportRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

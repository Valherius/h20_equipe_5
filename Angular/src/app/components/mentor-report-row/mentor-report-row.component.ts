import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MentorReport } from 'src/app/models/mentorReport.model';

@Component({
  selector: '[app-mentor-report-row]',
  templateUrl: './mentor-report-row.component.html',
  styleUrls: ['./mentor-report-row.component.css']
})

 
export class MentorReportRowComponent implements OnInit {
  @Input() mdata: MentorReport;
  public Hammertime:number; //Hours of the average
  public Mastertime:number; //Mintes of the average

  constructor() {

    
   }

  ngOnInit() {
    this.Hammertime=Math.trunc(Number(this.mdata.average)/60);
    this.Mastertime=Number(this.mdata.average)%60;
  }

}

import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { TagsService } from 'src/app/services/tags.service';
import { EventEmitter } from '@angular/core';
import { Tag } from 'src/app/models/tag.model';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-service-add',
  templateUrl: './service-add.component.html',
  styleUrls: ['./service-add.component.css']
})
export class ServiceAddComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @ViewChild('form', {static: true}) formElement: NgForm;
  @Output() readonly tagCreate: EventEmitter<Tag> = new EventEmitter();
  tagForm: FormGroup;
  nameControl: FormControl;

  constructor(formBuilder: FormBuilder, private tagsService: TagsService,private router:Router, public translate: TranslateService) 
  { 
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.nameControl = new FormControl('', Validators.required);

    this.tagForm = formBuilder.group(
      { 
        name: this.nameControl,
        description: new FormControl('')  
      }
    );
  }

  create() {
    const tagValue = this.tagForm.value;
    if(this.tagForm.valid)
    {
      this.tagsService.createservices(tagValue.name, tagValue.description)
      .subscribe(newTag => {
        //console.log(newTag);
        if(newTag)
        {
          if(this.translate.currentLang == 'english')
          {
            this.myModal.showBasic('Success', 'Successfully created the service');
          } else 
          {
            this.myModal.showBasic('Succès', 'Le service a été créé avec succès');
          }
          
          this.formElement.resetForm();
          //this.router.navigate(['/services/']);
        }
        else
        {
          if(this.translate.currentLang == 'english')
          {
            this.myModal.showBasic('Error', 'Unable to create the service, name already exists');
          } else 
          {
            this.myModal.showBasic('Erreur', 'Le service na pas pu être créé, le nom existe déjà');
          }
        }
      });
      //this.tagForm.reset();
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
      } else 
      {
        this.myModal.showBasic('Oops', 'Veuillez remplir le formulaire correctement');
      }
    }
  }

  return()
  {
    this.router.navigate(['/services/']);
  }

  ngOnInit() {
  }

}

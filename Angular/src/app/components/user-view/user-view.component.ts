import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/models/user.model';
import { FormGroup, FormControl, NgForm, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { ModalComponent } from '../modal/modal.component';
@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['./user-view.component.css']
})

export class UserViewComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @ViewChild('form', { static: true }) formElement: NgForm;
  @Output() readonly userUpdate: EventEmitter<User> = new EventEmitter();
  userForm: FormGroup;
  public user: User;
  

  constructor(formBuilder: FormBuilder , private router: Router, private route: ActivatedRoute, private usersService: UsersService, public translate: TranslateService) 
  {    
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    
     route.paramMap.subscribe(params => {
    this.user = this.usersService.get(Number(params.get('PK_id')));
    //console.log(this.user);
     });
     
    this.userForm = formBuilder.group(
      {
          username: new FormControl('', Validators.required),
          firstname: new FormControl('', Validators.required),
          lastname: new FormControl('', Validators.required),
          address: new FormControl('', Validators.required),
          image: new FormControl('', Validators.required),
          isAdmin: new FormControl('', Validators.required),
          newPass: new FormControl(''),
          confirmPass: new FormControl('')

      });


    this.usersService.usersg(Number(this.route.snapshot.paramMap.get('id'))).subscribe(user => {
      this.user = user;
      this.userForm.controls['username'].setValue(this.user.username);
      this.userForm.controls['firstname'].setValue(this.user.firstname);
      this.userForm.controls['lastname'].setValue(this.user.lastname);
      this.userForm.controls['address'].setValue(this.user.address);
      this.userForm.controls['image'].setValue(this.user.image);
      this.userForm.controls['isAdmin'].setValue(this.user.isAdmin);
    
      this.userForm.controls['newPass'].setValue('');
      this.userForm.controls['confirmPass'].setValue('');

      })
 }

  update()
  {
    const userValue = this.userForm.value;
    //console.log(userValue);
    if(this.userForm.valid)
    {
      if(userValue.newPass!=='' || userValue.username !== this.user.username || userValue.firstname !== this.user.firstname || userValue.lastname !== this.user.lastname || userValue.address !== this.user.address || userValue.image !== this.user.image || userValue.isAdmin !== this.user.isAdmin)
      {
        this.usersService.updateuser(this.user.PK_id, userValue.username, userValue.newPass, userValue.firstname, userValue.lastname, userValue.address, userValue.image, userValue.isAdmin)
        .subscribe(success => { 
          if(this.translate.currentLang == 'english')
          {
            this.myModal.showBasic('Success', 'User successfully modified');
          } else 
          {
            this.myModal.showBasic('Succès', 'Utilisateur modifié avec succès');
          }
          
          
        });
      }
      else
      {         
        if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'You need to make changes to be able to save.');
      } else 
      {
        this.myModal.showBasic('Oups', 'Afin de faire une mise à jour, il doit y avoir des modifications.');
      }
        
      }
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
      } else 
      {
        this.myModal.showBasic('Oups', 'Veuillez remplir le formulaire');
      }
      
    }

  }

  userLoans()
  {
    this.router.navigate(['/users/loans/', this.user.PK_id]);
  }
  newLoan()
  {
    this.router.navigate(['/users/newloan/', this.user.PK_id]);
  }

  return()
  {
    this.router.navigate(['/users/']);
  }

  ngOnInit() 
  {
  }
}

import { Component, Input, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { TranslateService } from '@ngx-translate/core';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { ThemePalette } from '@angular/material/core';
import { MatDialog } from '@angular/material';

import { ModalComponent } from '../modal/modal.component';
@Component({
  selector: '[app-user-row]',
  templateUrl: './user-row.component.html',
  styleUrls: ['./user-row.component.css']
})
export class UserRowComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @Output() readonly userDelete: EventEmitter<any> = new EventEmitter();
  @Input() user: User;
  msg : string;


  constructor(private usersService: UsersService, private router: Router, private dialog: MatDialog,
    private translate: TranslateService) 
  {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
  }

  ngOnInit() {
  }

  delete()
  {
    
      this.translate.get(['USER_MSG.DELETE'])
      .subscribe(translations => {
        this.msg =translations['USER_MSG.DELETE'] + this.user.firstname + ' ' + this.user.lastname ;
      });
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      data: this.msg
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.usersService.delete(this.user)
        .subscribe(success => 
          {this.userDelete.emit()
          });
      }
    });
   
  }

  view()
  {
    this.router.navigate(['/users/', this.user.PK_id]);
  }

}




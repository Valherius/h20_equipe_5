import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Room } from 'src/app/models/room.model';
import { RoomsService } from 'src/app/services/rooms.service';
import { Router } from '@angular/router';
import { NgForm, FormGroup, FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-room-list',
  templateUrl: './room-list.component.html',
  styleUrls: ['./room-list.component.css']
})
export class RoomsListComponent implements OnInit {

  @Output() readonly search: EventEmitter<any> = new EventEmitter();
  @ViewChild('form', {static: true}) formElement: NgForm;
  public rooms: Room[]=[];
  roomSearch:any;
  searchForm: FormGroup;
  searchControl: FormControl;

  constructor(public roomsService: RoomsService, public translate: TranslateService, private router:Router, private formBuilder:FormBuilder) 
  { 
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    roomsService.rooms.subscribe(rooms =>{
      this.rooms = rooms
      //console.log(this.rooms);
    });

    this.searchControl = new FormControl('');
    this.searchForm = formBuilder.group({
      search: this.searchControl,
    });
  }

  onSubmit(){

    const searchData = this.searchForm.value.search;
    console.log(searchData);

    this.roomsService.fullTextSearch(`rooms`,{searchData}).subscribe(Response => { 
      console.log(Response);
      if (Response) {
        this.rooms = Response.rooms;
      }
    });
  }

  ngOnInit() {
  }

  roomCreated() {
    this.roomsService.rooms.subscribe(rooms => this.rooms = rooms);
  }

  roomDeleted() {
    this.roomsService.rooms.subscribe(rooms => this.rooms = rooms);
    //console.log(this.rooms);
  }

}

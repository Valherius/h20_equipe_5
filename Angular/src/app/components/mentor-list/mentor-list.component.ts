import { NgForm, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { MentorsService } from 'src/app/services/mentors.service';
import { Mentor } from 'src/app/models/mentor.model';
import { TranslateService } from '@ngx-translate/core';

import { ModalComponent } from '../modal/modal.component';
@Component({
  selector: 'app-mentor-list',
  templateUrl: './mentor-list.component.html',
  styleUrls: ['./mentor-list.component.css']
})
export class MentorListComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @Output() readonly search: EventEmitter<any> = new EventEmitter();
  public mentors: Mentor[]=[];
  searchForm: FormGroup;
  searchControl: FormControl;
  
  constructor(private formBuilder:FormBuilder  ,private router:Router,private mentorsService:MentorsService, public translate: TranslateService) { 

    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    mentorsService.mentors.subscribe(mentors =>{
    this.mentors= mentors;
     //console.log(this.mentors);
    });

    this.searchControl = new FormControl('');
    this.searchForm = formBuilder.group({
      search: this.searchControl,
    });
   
  }

  onSubmit(){

    const searchData = this.searchForm.value.search;
    //console.log(searchData);

    this.mentorsService.fullTextSearch(`mentors`,{searchData}).subscribe(Response => { 
      //console.log(Response);
      if (Response) {
        this.mentors = Response;
      }
    });
  }

  mentorCreated() {
    this.mentorsService.mentors.subscribe(mentors => this.mentors = mentors);
  }

  mentorDeleted() {
    this.mentorsService.mentors.subscribe(mentors => this.mentors = mentors);
    //console.log(this.mentors);
  }
  
  ngOnInit() {
  }

}

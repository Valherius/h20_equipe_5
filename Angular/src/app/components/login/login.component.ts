import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/models/user.model';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  returnUrl: string;
  logged: User=null;
  constructor(private authService: AuthService, private router: Router, public translate: TranslateService, private route: ActivatedRoute)
  {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'english');
    this.logged=(JSON.parse(localStorage.getItem("CU"))||'');
    if(this.logged)
    {
      this.router.navigate(['users']);
    }
  }

  ngOnInit() {
   // this.authService.logout();
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/users';
  }

  setLanguage(language: string)
  {
    this.translate.use(language);
    localStorage.setItem("CL", JSON.stringify(language));
  }

  login(username: string, password: string) 
  {
    this.authService.login(username, password).subscribe(user => {
      if(user)
      {
        this.router.navigate([this.returnUrl]);
      }
      else
      {
        if(this.translate.currentLang == 'francais')
        {
          this.myModal.showBasic('Erreur', 'Veuillez entrer un nom dutilisateur et un mot de passe valide');
        } else {
          this.myModal.showBasic('Error', 'Please provide a correct username and password');
        }
      }
    });
  }
}

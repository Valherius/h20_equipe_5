import { Component, OnInit, Output, ViewChild, EventEmitter } from '@angular/core';
import { Book } from 'src/app/models/book.model';
import { BooksService } from 'src/app/services/books.service';
import { TranslateService } from '@ngx-translate/core';
import { NgForm, FormGroup, FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})

export class BooksListComponent implements OnInit {

  @Output() readonly search: EventEmitter<any> = new EventEmitter();
  @ViewChild('form', {static: true}) formElement: NgForm;
  public books: Book[]=[];
  searchForm: FormGroup;
  searchControl: FormControl;

  constructor(private formBuilder:FormBuilder  , private booksService:BooksService, public translate: TranslateService) 
  { 
 
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    booksService.books.subscribe( books =>{
      this.books = books
      //console.log(this.books);
    });
    
    this.searchControl = new FormControl('');
    this.searchForm = formBuilder.group({
      search: this.searchControl,
    });
  }

  onSubmit(){

    const searchData = this.searchForm.value.search;
    //console.log(searchData);

    this.booksService.fullTextSearch(`books`,{searchData}).subscribe(Response => { 
      //console.log(Response);
      if (Response) {
        this.books = Response.books;
      }
    });
  }
  ngOnInit() {
  }

  bookCreated() {
    this.booksService.books.subscribe(books => this.books = books);
  }

  bookDeleted() {
    this.booksService.books.subscribe(books => this.books = books);
    //console.log(this.books);
  }
}

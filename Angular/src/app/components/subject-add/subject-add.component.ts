import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { SubjectService } from 'src/app/services/subjects.service';
import { Subject } from 'src/app/models/subject.model';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TagsService } from 'src/app/services/tags.service';
import { Tag } from 'src/app/models/tag.model';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-subject-add',
  templateUrl: './subject-add.component.html',
  styleUrls: ['./subject-add.component.css']
})
export class SubjectAddComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @ViewChild('form', { static: true }) formElement: NgForm;
  @Output() readonly tagCreate: EventEmitter<Tag> = new EventEmitter();
  tagForm: FormGroup;
  nameControl: FormControl;



  constructor(formBuilder: FormBuilder, private tagsService: TagsService,private router: Router, public translate: TranslateService)
  {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.nameControl = new FormControl('', Validators.required);

    this.tagForm = formBuilder.group(
      { 
        name: this.nameControl,
        description: new FormControl('')  
      }
    );

  }

  ngOnInit()
  { 
    
  }

  create() {
    const tagValue = this.tagForm.value;
    if(this.tagForm.valid)
    {
      this.tagsService.createsubjects(tagValue.name, tagValue.description)
      .subscribe(newTag => {
        //console.log(newTag);
        if(newTag)
        {
          //alert("Success.");
          this.myModal.showBasic('Success', 'Successfully created the subject');
          this.tagForm.reset();
          //this.router.navigate(['/subjects/']);
        }
        else
        { //alert("Unable to create the subject.");
          this.myModal.showBasic('Failed', 'Unable to create the subject');
        }
      });
      
    }
    else
    { //alert("Please fill out the form appropriately");
      this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
    }
  }


  return()
  {
    this.router.navigate(['/subjects/']);
  }

}
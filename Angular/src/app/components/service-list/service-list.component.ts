import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Tag } from 'src/app/models/tag.model';
import { TagsService } from 'src/app/services/tags.service';
import { TranslateService } from '@ngx-translate/core';
import { NgForm, FormGroup, FormControl, FormBuilder } from '@angular/forms';



@Component({
  selector: 'app-service-list',
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.css']
})
export class ServiceListComponent implements OnInit {
 
  @Output() readonly search: EventEmitter<any> = new EventEmitter();
  @ViewChild('form', {static: true}) formElement: NgForm;
  public services: Tag[]=[];
  serviceSearch:any;
  searchForm: FormGroup;
  searchControl: FormControl;
 
  constructor( private router: Router, private tagsService: TagsService, public translate: TranslateService, private formBuilder: FormBuilder ) {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');  
    tagsService.services.subscribe(services => this.services = services);
    this.searchControl = new FormControl('');
    this.searchForm = formBuilder.group({
      search: this.searchControl,
    });
   
  }

  onSubmit(){
    const searchData = this.searchForm.value.search;
    //console.log(searchData);

    this.tagsService.fullTextSearchService('services',{searchData}).subscribe(Response => { 
      //console.log(Response);
      if (Response) {
        //console.log(Response.services);
        this.services = Response;
      }
    });
  }

  ngOnInit() {
  }

  serviceCreated() {
    this.tagsService.services.subscribe(services => this.services = services);
  }

  serviceDeleted() {
    this.tagsService.services.subscribe(services => this.services = services);
    //console.log(this.services);
  }

}


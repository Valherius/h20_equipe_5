import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { Tag } from 'src/app/models/tag.model';
import { TagsService } from 'src/app/services/tags.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ModalComponent } from '../modal/modal.component';
import { RoomsService } from 'src/app/services/rooms.service';
import { Room } from 'src/app/models/room.model';
import { MatDialog } from '@angular/material';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

@Component({
  selector: '[app-service-row]',
  templateUrl: './service-row.component.html',
  styleUrls: ['./service-row.component.css']
})
export class ServiceRowComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @Input() tag: Tag;
  @Output() serviceDelete: EventEmitter<any> = new EventEmitter();
  public rooms: Room[]=[];
  msg:string;


  constructor(private tagsService: TagsService, private router: Router,  public translate: TranslateService, public roomsService: RoomsService, private dialog: MatDialog) { 
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    roomsService.rooms.subscribe(rooms => this.rooms = rooms)
  }

  view() {
    this.router.navigate(['/services/', this.tag.PK_id]);
  }

  delete()
  {
    /*if(this.translate.currentLang == 'francais')
    { 
      var response = confirm("Êtes-vous sûr de vouloir supprimer "+ this.tag.name+"?");
      if(!response)
      {
        return false;
      } else 
      {
        this.tagsService.deleteservice(this.tag.PK_id)
        .subscribe(success => this.serviceDelete.emit());
      }
    } else {
      var response = confirm("Are you sure you want to delete "+ this.tag.name+"?");
      if(!response)
      {
          return false;
      }
      else
      {
        this.tagsService.deleteservice(this.tag.PK_id)
        .subscribe(success => this.serviceDelete.emit());
    } }*/

      this.translate.get(['SERVICE_ROW.SUREDELETE'])
      .subscribe(translations => {
        this.msg =translations['SERVICE_ROW.SUREDELETE'] + this.tag.name + "?";
      });
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      data: this.msg
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.tagsService.deleteservice(this.tag.PK_id)
        .subscribe(success => this.serviceDelete.emit());
      }
    });
    
  }


  ngOnInit() {
  }

}

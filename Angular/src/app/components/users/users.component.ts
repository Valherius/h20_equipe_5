import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UsersService } from 'src/app/services/users.service';
import { TranslateService } from '@ngx-translate/core';
import { FnParam } from '@angular/compiler/src/output/output_ast';
import { FormGroup, NgForm, FormControl, FormBuilder } from '@angular/forms';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { ThemePalette } from '@angular/material/core';
import { MatDialog } from '@angular/material';
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
 
  @Output() readonly search: EventEmitter<any> = new EventEmitter();
  @ViewChild('form', {static: true}) formElement: NgForm;
  public users: User[]=[];
  searchForm: FormGroup;
  searchControl: FormControl;
  UserInSearch: boolean;
  msg: string;

  constructor(private formBuilder: FormBuilder, private usersService: UsersService,  public translate: TranslateService)
   {
     this.UserInSearch=true;
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');

      usersService.users.subscribe( users =>{
      this.users = users
      //console.log(this.users);
    });
    this.searchControl = new FormControl('');
    this.searchForm = formBuilder.group({
      search: this.searchControl,
    });
   }

   onSubmit(){
    const searchData = this.searchForm.value.search;
    //console.log(searchData);

    this.usersService.fullTextSearch(`users`,{searchData}).subscribe(Response => { 
     //console.log(Response);
      if (Response) {
        if(Response.users.lenght>0)
        {
          this.UserInSearch = true;
        } else {
          this.UserInSearch = false;
        }
        //console.log(Response.users);
        this.users = Response.users;
      }
    });
  }

  ngOnInit() {
  }

  userCreated() {
    this.usersService.users.subscribe(users => this.users = users);
  }

  userDeleted() {

    
    this.usersService.users.subscribe(users => this.users = users);
    //console.log(this.users);
  }

  
}

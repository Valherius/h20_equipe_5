import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { NgForm, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Room } from 'src/app/models/room.model';
import { RoomsService } from 'src/app/services/rooms.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { TagsService } from 'src/app/services/tags.service';
import { Tag } from 'src/app/models/tag.model';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-room-add',
  templateUrl: './room-add.component.html',
  styleUrls: ['./room-add.component.css']
})
export class RoomAddComponent implements OnInit {

  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @ViewChild('form', { static: true }) formElement: NgForm;
  @Output() readonly roomCreate: EventEmitter<Room> = new EventEmitter();
  roomForm: FormGroup;
  nameControl: FormControl;
  public room: Room;
  public services: Tag[]=[];
  public selectedservices: Tag[]=[];
  public highlightservice:any;
  public highlightselect:any;
  isImageSaved: boolean;
  imageError: string;
  public imagePath;
  imgURL: any;

  constructor(formBuilder: FormBuilder, private roomsService: RoomsService,private router:Router, public translate: TranslateService, public tagsService: TagsService)
  { 
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.roomForm = formBuilder.group(
      {
        roomIdentifier: new FormControl('', Validators.required),
        location: new FormControl('', Validators.required),
        size: new FormControl('', Validators.required),
        image: new FormControl('', Validators.required)
      }
      );
      tagsService.services.subscribe(services => this.services = services);
  }

  public highlightRow(service) {
    this.highlightservice = service.name;
  }
  public highlightRow2(service) {
    this.highlightselect = service.name;
  }

  ngOnInit() {
  }

  imageSelected(fileInput: any)
  {
    //console.log(fileInput.target.files[0]);
    this.roomForm.controls['image'].setValue(fileInput.target.files[0]);
    this.isImageSaved = true;
    
    const reader = new FileReader();
    
    this.imagePath = fileInput.target.files;
    reader.readAsDataURL(fileInput.target.files[0]); 
    reader.onload = (_fileInput) => { 
      this.imgURL = reader.result; 
    }
  }

  removeImage() {
    this.imagePath = null;
    this.imgURL=null;
    this.roomForm.controls['image'].setValue(null);
    this.isImageSaved = false;
  }

  onSelect(service: Tag)
  {
    if(this.selectedservices.includes(service))
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Service already in the list!');
      } else 
      {
        this.myModal.showBasic('Oops', 'Le service est déjà dans la liste!');
      }
    }
    else
    {
      this.selectedservices.push(service);
      const ind = this.services.indexOf(service);
      if (ind > -1) 
      {
        this.services.splice(ind, 1);
      }
    }
    //console.log(this.selectedservices);
  }

  secondSelect(service: Tag)
  {
    const index = this.selectedservices.indexOf(service);
    if (index > -1) 
    {
      this.services.push(service);
      this.selectedservices.splice(index, 1);
    }
    //console.log(this.selectedservices);
  }

  create() 
  {
    const roomValue = this.roomForm.value;
    //console.log(this.roomForm.value);
    if(this.roomForm.valid)
    {
      //console.log(this.roomForm.valid);
      const formData :FormData =new FormData();
      formData.append('roomIdentifier',roomValue.roomIdentifier);
      formData.append('location',roomValue.location);
      formData.append('size',roomValue.size);
      formData.append('image',roomValue.image);

      for(let index in this.selectedservices)
      {
        formData.append('services[_ids][]',this.selectedservices[index].PK_id.toString());
      }

      this.roomsService.create(formData)
      .subscribe(result => {
        //console.log(result);
        if(result)
        {
          if(this.translate.currentLang == 'english')
          {
            this.myModal.showBasic('Success', 'Successfully created the room');
          } else 
          {
            this.myModal.showBasic('Succès', 'La salle a été créé avec succès');
          }
          this.formElement.resetForm();
          this.selectedservices=[];
          this.roomForm.value.image = null;
          this.roomForm.controls['image'].setValue(null);
        }else{
          if(this.translate.currentLang == 'english')
          {
            this.myModal.showBasic('Error', 'Sorry, unable to create the room.');
          } else 
          {
            this.myModal.showBasic('Erreur', 'Désolé, la salle n a pas pu être créé');
          }
        }
      });
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
      } else 
      {
        this.myModal.showBasic('Oops', 'Veuillez remplir le formulaire correctement');
      }
    }
  }

  return()
  {
    this.router.navigate(['/rooms/']);
  }

}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Room } from 'src/app/models/room.model';
import { RoomsService } from 'src/app/services/rooms.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

@Component({
  selector: '[app-room-row]',
  templateUrl: './room-row.component.html',
  styleUrls: ['./room-row.component.css']
})
export class RoomRowComponent implements OnInit {

  @Input() room: Room;
  @Output() readonly roomDelete: EventEmitter<any> = new EventEmitter();
  msg:string;

  constructor(private roomsService:RoomsService, private router:Router, public translate: TranslateService, private dialog: MatDialog)
   {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
   }

   view() {
    //console.log(this.room);
    this.router.navigate(['/rooms/', this.room.PK_id]);
  }

  delete(){
    /*if(this.translate.currentLang == 'francais')
    {
        var response = confirm("Êtes-vous sûr de vouloir supprimer "+ this.room.roomIdentifier+"?");
        if(!response)
        {
          return false;
        }
        this.roomsService.delete(this.room)
        .subscribe(success => this.roomDelete.emit());
    } else {
        var response = confirm("Are you sure you want to delete "+ this.room.roomIdentifier+"?");
        if(!response)
        {
            return false;
        }
        this.roomsService.delete(this.room)
        .subscribe(success => this.roomDelete.emit());  
    }*/


    this.translate.get(['ROOM_ROW.DELETE_MSG'])
    .subscribe(translations => {
      this.msg =translations['ROOM_ROW.DELETE_MSG'] + this.room.roomIdentifier + "?";
    });
  const dialogRef = this.dialog.open(AlertDialogComponent, {
    data: this.msg
  });
  dialogRef.afterClosed().subscribe(result => {
    if (result === 'confirm') {
      this.roomsService.delete(this.room)
      .subscribe(success => this.roomDelete.emit());
    }
  });





  }

  ngOnInit() {
  }

}

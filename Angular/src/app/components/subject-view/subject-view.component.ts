import { Component, OnInit, Input, ViewChild} from '@angular/core';
import { Router, ActivatedRoute, RouteReuseStrategy } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Tag } from '../../models/tag.model';
import { TagsService } from '../../services/tags.service';
import { TranslateService } from '@ngx-translate/core';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-subject-view',
  templateUrl: './subject-view.component.html',
  styleUrls: ['./subject-view.component.css']
})
export class SubjectViewComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  private subject: Tag;

  tagForm: FormGroup;

  constructor(formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private tagsService: TagsService, public translate: TranslateService) { 
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.tagForm = formBuilder.group(
      {
        name: new FormControl('', Validators.required),
        description: new FormControl(''),
      });

    this.tagsService.getsubject(Number(this.route.snapshot.paramMap.get('id'))).subscribe(subject => {
      this.subject = subject;
      this.tagForm.controls['name'].setValue(this.subject.name);
      this.tagForm.controls['description'].setValue(this.subject.description);
      })
  }


  update()
  {
    const tagValue = this.tagForm.value;
    
    if(this.tagForm.valid)
    {
      if(tagValue.name !== this.subject.name || tagValue.description !== this.subject.description)
      {
        //console.log(tagValue.name);
        this.tagsService.updatesubject(this.subject.PK_id, tagValue.name, tagValue.description)
        .subscribe(success => {
          if(success)
          {
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Success', 'The subject has been modified successfully');
            } else 
            {
              this.myModal.showBasic('Succès', 'Le sujet a été modifié avec succès');
            }

          }
          else{
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Oops', 'Sorry, unable to modify the subject');
            } else {
              this.myModal.showBasic('Oops', 'Désolé, le sujet na pas pu être modifié');
            }
          }
        });
      }
      else
      {
        if(this.translate.currentLang == 'english')
        {
          this.myModal.showBasic('Error', 'You need to make changes to be able to save');
        } else 
        {
          this.myModal.showBasic('Erreur', 'Vous devez effectuer des changement pour sauvegarder');
        }

      }
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
      } else 
      {
        this.myModal.showBasic('Oops', 'Veuillez remplir le formulaire correctement');
      }
    }
  }

  return()
  {
    this.router.navigate(['/subjects/']);
  }

  ngOnInit() {
  }

}

import { Component, ViewChild, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Loan } from 'src/app/models/loan.model';
import { LoansService } from 'src/app/services/loans.service';

import { ModalComponent } from '../modal/modal.component';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { User } from 'src/app/models/user.model';
import { UsersService } from 'src/app/services/users.service';
@Component({
  selector: 'app-user-loans',
  templateUrl: './user-loans.component.html',
  styleUrls: ['./user-loans.component.css']
})
export class UserLoansComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  public loans: Loan[]=[];
  currentUser: User;
  isAnAdmin = false;
  public user: User;
  

  constructor( private usersService:UsersService, private authService: AuthService, private router: Router, private route: ActivatedRoute, public translate: TranslateService, private loansService: LoansService  ) {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.currentUser = JSON.parse(localStorage.getItem("CU"));
    route.paramMap.subscribe(params => {
      this.user = this.usersService.get(Number(params.get('PK_id')));
      //console.log(this.user);
       });
   
 


    if(!this.currentUser.isAdmin)
    {
      loansService.loans.subscribe( loans =>{
        this.loans = loans
        //console.log(loans);
      }); 
       
    }else{
        loansService.getloans(Number(this.route.snapshot.paramMap.get('id'))).subscribe( loans =>{
        this.loans = loans
        console.log(loans);
      });
    }
    //console.log(this.user.firstname);

   }

  ngOnInit() {

    if(this.currentUser.isAdmin)
    {
      this.isAnAdmin = true;
    } else {
      this.isAnAdmin = false;
    }

  }

  setLanguage(language: string)
  {
    this.translate.use(language);
    localStorage.setItem("CL", JSON.stringify(language));
  }
  userCreated() {
    this.loansService.loans.subscribe(loans => this.loans = loans);
  }

  userDeleted() {
    this.loansService.loans.subscribe(loans => this.loans = loans);
    //console.log(this.loans);
  }

  loanReturned() {
    this.loansService.getloans(Number(this.route.snapshot.paramMap.get('id'))).subscribe( loans =>{
      this.loans = loans
      //console.log(loans);
    });
  }

  logout()
  {
    this.authService.logout();
    this.router.navigate(['/']);
  }
}





import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectNavbarComponent } from './subject-navbar.component';

describe('SubjectNavbarComponent', () => {
  let component: SubjectNavbarComponent;
  let fixture: ComponentFixture<SubjectNavbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectNavbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectNavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-user-navbar',
  templateUrl: './user-navbar.component.html',
  styleUrls: ['./user-navbar.component.css']
})
export class UserNavbarComponent implements OnInit {

  constructor(private router: Router, private usersService: UsersService, public translate: TranslateService) { this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English'); }

  ngOnInit() {
  }

}

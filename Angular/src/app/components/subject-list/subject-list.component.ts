import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Tag } from 'src/app/models/tag.model';
import { TagsService } from 'src/app/services/tags.service';
import { TranslateService } from '@ngx-translate/core';
import { NgForm, FormGroup, FormControl, FormBuilder } from '@angular/forms';



@Component({
  selector: 'app-subject-list',
  templateUrl: './subject-list.component.html',
  styleUrls: ['./subject-list.component.css']
})
export class SubjectListComponent implements OnInit {
  
  @Output() readonly search: EventEmitter<any> = new EventEmitter();
  @ViewChild('form', {static: true}) formElement: NgForm;
  public subjects: Tag[]=[];
  subjectSearch:any;
  searchForm: FormGroup;
 searchControl: FormControl;

 
  constructor( private formBuilder:FormBuilder, private router: Router, private tagsService: TagsService, public translate: TranslateService ) {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');  
    tagsService.subjects.subscribe(subjects => this.subjects = subjects);

    this.searchControl = new FormControl('');
    this.searchForm = formBuilder.group({
      search: this.searchControl,
    });
   
   }

   onSubmit(){

    const searchData = this.searchForm.value.search;
    //console.log(searchData);

    this.tagsService.fullTextSearchSubject('subjects',{searchData}).subscribe(Response => { 
      //console.log(Response);
      if (Response) {
        this.subjects = Response;
      }
    });
  }

  ngOnInit() {
  }
  subjectCreated() {
    this.tagsService.subjects.subscribe(subjects => this.subjects = subjects);
  }

  subjectDeleted() {
    this.tagsService.subjects.subscribe(subjects => this.subjects = subjects);
    //console.log(this.subjects);
  }

}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MentorRowComponent } from './mentor-row.component';

describe('MentorRowComponent', () => {
  let component: MentorRowComponent;
  let fixture: ComponentFixture<MentorRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MentorRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MentorRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

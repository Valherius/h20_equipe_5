import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { MentorsService } from 'src/app/services/mentors.service';
import { Router } from '@angular/router';
import { Mentor } from 'src/app/models/mentor.model';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: '[app-mentor-row]',
  templateUrl: './mentor-row.component.html',
  styleUrls: ['./mentor-row.component.css']
})
export class MentorRowComponent implements OnInit {
  msg : string;
@Input() mentor: Mentor;
@ViewChild('myModal', { static: true }) myModal: ModalComponent;
@Output() readonly mentorDelete: EventEmitter<any> = new EventEmitter();

  constructor(private mentorsService: MentorsService, private dialog: MatDialog, private router:Router,public translate: TranslateService) { 

    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
  }

  ngOnInit() {
  }

  delete(){

    this.translate.get(['MENTOR_MSG.DELETE'])
    .subscribe(translations => {
      this.msg =translations['MENTOR_MSG.DELETE'] + (this.mentor.firstname+' '+this.mentor.lastname);
    });
    const dialogRef = this.dialog.open(AlertDialogComponent, {
    data: this.msg
    });
    dialogRef.afterClosed().subscribe(result => {
    if (result === 'confirm') {
        this.mentorsService.delete(this.mentor)
        .subscribe(success => this.mentorDelete.emit()
        );
    }
    });
    
  }

  view() {
    this.router.navigate(['/mentors/', this.mentor.PK_id]);
  }
}

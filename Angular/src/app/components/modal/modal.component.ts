import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormGroup, NgForm, FormBuilder, FormControl, Validators } from '@angular/forms';
declare const $: any;

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html'
  })

  export class ModalComponent
  {
    modaltitle: string;
    modalbody: string;
    result: boolean;
    showBasic(title:string, body:string)
    {
        this.modaltitle=title;
        this.modalbody=body;
        $('#basicModal').modal('show');
    }


}

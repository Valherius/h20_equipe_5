import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { Tag } from 'src/app/models/tag.model';
import { TagsService } from 'src/app/services/tags.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ModalComponent } from '../modal/modal.component';
import { MatDialog } from '@angular/material';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

@Component({
  selector: '[app-skill-row]',
  templateUrl: './skill-row.component.html',
  styleUrls: ['./skill-row.component.css']
})
export class SkillRowComponent implements OnInit {
  @Input() tag: Tag;
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @Output()  skillDelete: EventEmitter<any> = new EventEmitter();
  msg : string;
  constructor(private tagsService: TagsService,private dialog: MatDialog, private router: Router, public translate: TranslateService) { this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English'); }

  view() {
    this.router.navigate(['/skills/', this.tag.PK_id]);
  }

  delete(){
        
          this.translate.get(['SKILL_MSG.DELETE'])
          .subscribe(translations => {
            this.msg =translations['SKILL_MSG.DELETE'] + this.tag.name;
          });
          const dialogRef = this.dialog.open(AlertDialogComponent, {
          data: this.msg
          });
          dialogRef.afterClosed().subscribe(result => {
          if (result === 'confirm') {
            this.tagsService.deleteskill(this.tag.PK_id)
            .subscribe(success =>
              {this.skillDelete.emit()
              });
          }
          });
        }


  ngOnInit() {
  }

}

import { Component, OnInit, Input,EventEmitter, Output, ViewChild } from '@angular/core';
import { BooksService } from 'src/app/services/books.service';
import { Router } from '@angular/router';
import { Book } from 'src/app/models/book.model';
import { TranslateService } from '@ngx-translate/core';
import { ModalComponent } from '../modal/modal.component';
import { MatDialog } from '@angular/material';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

@Component({
  selector: '[app-book-row]',
  templateUrl: './book-row.component.html',
  styleUrls: ['./book-row.component.css']
})
export class BookRowComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @Input() book: Book;
  @Output() readonly bookDelete: EventEmitter<any> = new EventEmitter();
  msg:string;

  constructor(private booksService:BooksService, private router:Router,  private dialog: MatDialog,
    private translate: TranslateService){
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
  }


  view() {
    //console.log(this.book);
    this.router.navigate(['/books/', this.book.PK_id]);
  }

  delete(){

    this.translate.get(['BOOK_ROW.SUREDELETE'])
      .subscribe(translations => {
        this.msg =translations['BOOK_ROW.SUREDELETE'] + this.book.name + "?";
      });
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      data: this.msg
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.booksService.delete(this.book)
        .subscribe(success => this.bookDelete.emit());
      }
    });
   

  }
  

  ngOnInit() {
  }



}

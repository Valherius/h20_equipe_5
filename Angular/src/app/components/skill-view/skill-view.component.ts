import { Component, OnInit, Input, ViewChild} from '@angular/core';
import { Router, ActivatedRoute, RouteReuseStrategy } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Tag } from '../../models/tag.model';
import { TagsService } from '../../services/tags.service';
import { TranslateService } from '@ngx-translate/core';

import { ModalComponent } from '../modal/modal.component';
@Component({
  selector: 'app-skill-view',
  templateUrl: './skill-view.component.html',
  styleUrls: ['./skill-view.component.css']
})
export class SkillViewComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  private skill: Tag;

  tagForm: FormGroup;

  constructor(formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private tagsService: TagsService, public translate: TranslateService) { 
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.tagForm = formBuilder.group(
      {
        name: new FormControl('', Validators.required),
        description: new FormControl(''),
      });

    this.tagsService.getskill(Number(this.route.snapshot.paramMap.get('id'))).subscribe(skill => {
      this.skill = skill;
      this.tagForm.controls['name'].setValue(this.skill.name);
      this.tagForm.controls['description'].setValue(this.skill.description);
      })
  }


  update()
  {
    const tagValue = this.tagForm.value;
    
    if(this.tagForm.valid)
    {
      if(tagValue.name !== this.skill.name || tagValue.description !== this.skill.description)
      {
        //console.log(tagValue.name);
        this.tagsService.updateskill(this.skill.PK_id, tagValue.name, tagValue.description)
        .subscribe(success => {
          if(success)
          {
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Success', 'The skill has been modified successfully');
            } else 
            {
              this.myModal.showBasic('Succès', 'La compétence a été modifié avec succès');
            }
          }
          else{
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Oops', 'Sorry, unable to modify the skill');
            } else {
              this.myModal.showBasic('Oops', 'Désolé, la compétence na pas pu être modifié');
            }
          }
        });
      }
      else
      {
        if(this.translate.currentLang == 'english')
        {
          this.myModal.showBasic('Oops', 'You need to make changes to be able to save.');
        } else 
        {
          this.myModal.showBasic('Oups', 'Vous devez faire des changements avant de sauvegarder');
        }
        
      }
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
      } else 
      {
        this.myModal.showBasic('Oups', 'Veuillez completer le formulaire');
      }
      
    }
  }

  return()
  {
    this.router.navigate(['/skills/']);
  }

  ngOnInit() {
  }

}

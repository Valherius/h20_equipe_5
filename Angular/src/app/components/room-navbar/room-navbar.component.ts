import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-room-navbar',
  templateUrl: './room-navbar.component.html',
  styleUrls: ['./room-navbar.component.css']
})
export class RoomNavbarComponent implements OnInit {

  constructor(public translate: TranslateService) { this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English'); }

  ngOnInit() {
  }

}

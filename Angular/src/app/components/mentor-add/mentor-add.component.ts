import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { MentorsService } from 'src/app/services/mentors.service';
import { Mentor } from 'src/app/models/mentor.model';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Tag } from 'src/app/models/tag.model';

import { TagsService } from 'src/app/services/tags.service';
import { ModalComponent } from '../modal/modal.component';
import { skill } from 'src/app/models/skill.model';


@Component({
  selector: 'app-mentor-add',
  templateUrl: './mentor-add.component.html',
  styleUrls: ['./mentor-add.component.css']
})
export class MentorAddComponent implements OnInit {
  @ViewChild('form', { static: true }) formElement: NgForm;
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @Output() readonly userCreate: EventEmitter<Mentor> = new EventEmitter();
  mentorForm: FormGroup;
  nameControl: FormControl;
  public mentor: Mentor;
  public skills: Tag[]=[];
  public selectedskills: Tag[]=[];
  public highlightskill:any;
  public highlightselect:any;
  isImageSaved: boolean;
  imageError: string;
  public imagePath;
  imgURL: any;


  constructor(formBuilder: FormBuilder, private mentorService: MentorsService,private router:Router, public translate: TranslateService, private tagsService: TagsService)
  {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.mentorForm = formBuilder.group(
    {
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      contactNumber: new FormControl('',[Validators.maxLength(10), Validators.minLength(10)]),
      email: new FormControl('', [Validators.required, Validators.email ,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
      image: new FormControl('', Validators.required)
    }
    );

    tagsService.skills.subscribe(skills => this.skills = skills);


  }

  public highlightRow(skill) {
    this.highlightskill = skill.name;
  }
  public highlightRow2(skill) {
    this.highlightselect = skill.name;
  }

  ngOnInit() {
  }

  imageSelected(fileInput: any)
  {
    //console.log(fileInput.target.files[0]);
    this.mentorForm.controls['image'].setValue(fileInput.target.files[0]);
    this.isImageSaved = true;
    
    const reader = new FileReader();
    
    this.imagePath = fileInput.target.files;
    reader.readAsDataURL(fileInput.target.files[0]); 
    reader.onload = (_fileInput) => { 
      this.imgURL = reader.result; 
    }
  }

  removeImage() {
    this.imagePath = null;
    this.imgURL=null;
    this.mentorForm.controls['image'].setValue(null);
    this.isImageSaved = false;
}
  onSelect(skill: Tag)
  {
    if(this.selectedskills.includes(skill))
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'This skill is already in the list');
      } else 
      {
        this.myModal.showBasic('Oups', 'La compétence est déjà dans la liste');
      }
      
    }
    else
    {
      this.selectedskills.push(skill);
      const ind = this.skills.indexOf(skill);
      if (ind > -1) 
      {
        this.skills.splice(ind, 1);
      }
    }
  }

  secondSelect(skill: Tag)
  {   
    const index = this.selectedskills.indexOf(skill);
    
    if (index > -1) 
    {
      this.skills.push(skill);
      this.selectedskills.splice(index, 1);
    }
    //console.log(this.selectedskills);
  }

  create() 
  {
    //this.tmpskill=new skillsend(this._ids);
    const mentorValue = this.mentorForm.value;
    //console.log(this.mentorForm.value);
    if(this.mentorForm.valid)
    {
      const formData :FormData=new FormData();
      formData.append('firstname',mentorValue.firstname);
      formData.append('lastname',mentorValue.lastname);
      formData.append('contactNumber',mentorValue.contactNumber);
      formData.append('email',mentorValue.email);
      formData.append('image',mentorValue.image);
  
      for(let index in this.selectedskills)
      {
        formData.append('skills[_ids][]',this.selectedskills[index].PK_id.toString());
      }
      
      //this.mentorService.create(formData, this._ids)
      this.mentorService.create(formData)
      .subscribe(result => {
        //console.log(result);
        if(result)
        {
          if(this.translate.currentLang == 'english')
          {
            this.myModal.showBasic('Success', 'Mentor created successfully');
          } else 
          {
            this.myModal.showBasic('Reussite', 'Mentor créer avec succes');
          }
      
          this.formElement.resetForm();
          this.selectedskills=[];
          this.mentorForm.value.image = null;
          this.mentorForm.controls['image'].setValue(null);
        }else{
          if(this.translate.currentLang == 'english')
          {
            this.myModal.showBasic('Oops', 'Unable to create mentor');
          } else 
          {
            this.myModal.showBasic('Oups', 'Le mentor na pas pu être créer');
          }
          
        }
      });
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
      } else 
      {
        this.myModal.showBasic('Oups', 'Veuillez remplir le formulaire');
      }
      
    }
  }

  return()
  {
    this.router.navigate(['/mentors/']);
  }

}
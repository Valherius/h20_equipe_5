import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TranslateService } from '@ngx-translate/core';
import { NgForm, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Tag } from 'src/app/models/tag.model';
import { TagsService } from 'src/app/services/tags.service';

import { ModalComponent } from '../modal/modal.component';
@Component({
  selector: 'app-skill-list',
  templateUrl: './skill-list.component.html',
  styleUrls: ['./skill-list.component.css']
})
export class SkillListComponent implements OnInit {
  @Output() readonly search: EventEmitter<any> = new EventEmitter();
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  public skills: Tag[]=[];
  skillSearch:any;
  searchForm: FormGroup;
  searchControl: FormControl;

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private router: Router, private tagsService: TagsService, public translate: TranslateService ) {

    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');  
    tagsService.skills.subscribe(skills => this.skills = skills);

    this.searchControl = new FormControl('');
    this.searchForm = formBuilder.group({
      search: this.searchControl,
    });  
  }


  onSubmit(){

    const searchData = this.searchForm.value.search;
    //console.log(searchData);

    this.tagsService.fullTextSearchSkills('skills',{searchData}).subscribe(Response => { 
      //console.log(Response);
      if (Response) {
        this.skills = Response;
      }
    });
  }

  ngOnInit() {
  }

skillCreated() {
  this.tagsService.skills.subscribe(skills => this.skills = skills);
}

skillDeleted() {
  this.tagsService.skills.subscribe(skills => this.skills = skills);
  //console.log(this.skills);
}

}

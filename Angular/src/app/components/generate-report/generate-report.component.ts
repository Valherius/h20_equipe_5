import { Component, OnInit, ViewChild, Directive, EventEmitter, Input, Output, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NgForm, FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ReportsService } from 'src/app/services/reports.service';
import { Room } from 'src/app/models/room.model';
import { Book } from 'src/app/models/book.model';
import { RoomReportComponent } from '../room-report/room-report.component';
import { ModalComponent } from '../modal/modal.component';
import { DatePipe } from '@angular/common'
import { MentorReport } from 'src/app/models/mentorReport.model';
import { BookReport } from 'src/app/models/bookReport.model';
import { RoomReport } from 'src/app/models/roomReport.model';
import { RoomReportRowComponent } from '../room-report-row/room-report-row.component';

@Component({
  selector: 'app-generate-report',
  templateUrl: './generate-report.component.html',
  styleUrls: ['./generate-report.component.css']
})
export class GenerateReportComponent implements OnInit {
  @ViewChild('form', { static: true }) formElement: NgForm;
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @ViewChild('mentorReport', { static: true }) formMentorReport: MentorReport;
  @ViewChild('roomReport', { static: true }) formRoomReport: RoomReport;
  @ViewChild('bookReport', { static: true }) formBookReport: BookReport;
  public mentorData: MentorReport[]=[];
  public bookData: BookReport[]=[];
  public roomData: RoomReport[]=[];
  public roomTotal: RoomReport;
  public rooms: Room[]=[];
 
  reportForm: FormGroup;
  startDate: Date;
  endDate: Date;
  start:string;
  end:string;
  mentorSelected=false;
  roomSelected=false;
  bookSelected=false;
  submitted = false;
  customSelected=false;
  nbRent: Number[];
  total: Number;
  Gtotal:Number;
  LRent:Date[];
  LR:Date;
  test: number;
  isDirty: boolean;


  

  constructor(public datepipe: DatePipe, private router: Router, public translate: TranslateService, formBuilder: FormBuilder, private reportService: ReportsService) 
  {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'english');

    this.reportForm = formBuilder.group(
      { 
        type: new FormControl(''),
        datechoice: new FormControl(''),
        items: new FormControl(''),
        begin_date: new FormControl(''),
        end_date: new FormControl('')
      }
    );
    this.isDirty=false;
  }

  datechanged()
  {
    this.isDirty=true;
    if(this.reportForm.value.datechoice=='weekly')
    {
      this.endDate=new Date(this.reportForm.value.begin_date);
      this.test=0;
      this.test=this.endDate.getDate();
      this.endDate.setDate(this.test+7);
      this.end=this.datepipe.transform(this.endDate, 'yyyy-MM-dd');
      this.reportForm.controls['end_date'].setValue(this.end);

    }
    else if(this.reportForm.value.datechoice=='monthly')
    {
      this.endDate=new Date(this.reportForm.value.begin_date);
      this.endDate.setMonth(this.endDate.getMonth()+1);
      this.end=this.datepipe.transform(this.endDate, 'yyyy-MM-dd');
      this.reportForm.controls['end_date'].setValue(this.end);
    }
    else if(this.reportForm.value.datechoice=='annually')
    {
      this.endDate=new Date(this.reportForm.value.begin_date);
      this.endDate.setFullYear(this.endDate.getFullYear()+1);
      this.end=this.datepipe.transform(this.endDate, 'yyyy-MM-dd');
      this.reportForm.controls['end_date'].setValue(this.end);
    }
    
  }

  generate()
  {
    if(this.reportForm.value.items=='mentors')
    {
      this.submitted=true;
      this.mentorSelected=true;
      this.roomSelected=false;
      this.bookSelected=false;
    }
    else if(this.reportForm.value.items=='rooms')
    {
      this.submitted=true;
      this.mentorSelected=false;
      this.roomSelected=true;
      this.bookSelected=false;
    }
    else if(this.reportForm.value.items=='books')
    {
      this.submitted=true;
      this.mentorSelected=false;
      this.roomSelected=false;
      this.bookSelected=true; 
    }
    else
    {
      this.submitted=false;
    }

    this.startDate=new Date(this.reportForm.value.begin_date);
    this.endDate=new Date(this.reportForm.value.end_date);

    this.start=this.datepipe.transform(this.startDate, 'yyyy-MM-dd');
    this.end=this.datepipe.transform(this.endDate, 'yyyy-MM-dd');

 
    
    if(this.reportForm.value.items == 'books'){

      this.reportService.bookReport(this.start,this.end)
      .subscribe(result => {
        //console.log(result);
        if(result)
        {
          if(this.bookSelected)
          {
            //console.log(result);
            this.bookData=result;
            this.nbRent = this.bookData.map(yee => yee.Gtotal)
            this.Gtotal= this.nbRent[0];
            this.LRent = this.bookData.map(d => d.LastRentDate);
            this.LR = this.LRent[0];
          }
        }
      });

    }else if(this.reportForm.value.items == 'mentors'){

      this.reportService.mentorReport(this.start,this.end)
      .subscribe(result => {
        
        if(result)
        {
          if(this.mentorSelected)
          {
            this.mentorData=result;
          }
        }
      });
      
    }else if(this.reportForm.value.items == 'rooms') {
      this.reportService.roomReport(this.start,this.end)
      .subscribe(result => {
        if(result)
        {
          if(this.roomSelected)
          {
            this.roomData=result;
            this.nbRent = this.roomData.map(yee => yee.total)
            this.total = this.nbRent[0];
          }
        }
      });
    }
  }

  datechoiceChanged(event)
  {
    if(event.target.value=='custom')
    {
      this.customSelected=true;
    }
    else{
      this.customSelected=false;
      if(this.isDirty)
      { this.datechanged();}
     
    }
  }



  ngOnInit() {
  }

}

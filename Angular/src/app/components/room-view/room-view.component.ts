import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, NgForm, FormBuilder, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Room } from 'src/app/models/room.model';
import { Router, ActivatedRoute } from '@angular/router';
import { RoomsService } from 'src/app/services/rooms.service';
import { Tag } from 'src/app/models/tag.model';
import { TagsService } from 'src/app/services/tags.service';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-room-view',
  templateUrl: './room-view.component.html',
  styleUrls: ['./room-view.component.css']
})
export class RoomViewComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @ViewChild('form', { static: true }) formElement: NgForm;
  @Output() readonly roomUpdate: EventEmitter<Room> = new EventEmitter();
  roomForm: FormGroup;
  public room: Room;
  public services: Tag[]=[];
  public selectedservices: Tag[]=[];
  public ownedservices: Tag[]=[];
  public highlightservice:any;
  public highlightselect:any;
  isImageSaved: boolean;
  imageError: string;
  public imagePath;
  imgURL: any;

  constructor(formBuilder: FormBuilder , private router: Router, private route: ActivatedRoute, private roomsService: RoomsService, public translate: TranslateService, private tagsService: TagsService)
  {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.roomForm = formBuilder.group(
    {
        roomIdentifier: new FormControl('', Validators.required),
        location: new FormControl('', Validators.required),
        size: new FormControl('', Validators.required),
        image: new FormControl('',Validators.required)
    });

    tagsService.services.subscribe(services => this.services = services)

    this.roomsService.roomsg(Number(this.route.snapshot.paramMap.get('id'))).subscribe(room => {
      this.room = room;
      this.roomForm.controls['roomIdentifier'].setValue(this.room.roomIdentifier);
      this.roomForm.controls['location'].setValue(this.room.location);
      this.roomForm.controls['size'].setValue(this.room.size); 
      this.roomForm.controls['image'].setValue(this.room.image);

      for(let index in this.room.services)
      {
        for(let idx in this.services)
        {
          //console.log(this.room.services[index].PK_id+ " - "+ this.services[idx].PK_id)
          if(this.services[idx].PK_id==this.room.services[index].PK_id)
          {
            this.selectedservices.push(this.services[idx]);
            this.ownedservices.push(this.services[idx]);
            //console.log(this.selectedservices);
          }
        }
      }
      })
  }

  public highlightRow(service) {
    this.highlightservice = service.name;
  }
  public highlightRow2(service) {
    this.highlightselect = service.name;
  }


  imageSelected(fileInput: any)
  {
    //console.log(fileInput.target.files[0]);
    this.roomForm.controls['image'].setValue(fileInput.target.files[0]);
    this.isImageSaved = true;
    
    const reader = new FileReader();
    
    this.imagePath = fileInput.target.files;
    reader.readAsDataURL(fileInput.target.files[0]); 
    reader.onload = (_fileInput) => { 
      this.imgURL = reader.result; 
    }
  }

  removeImage() 
  {
    this.imagePath = null;
    this.imgURL = null;
    this.roomForm.controls['image'].setValue(null);
    this.isImageSaved = false;
  }

  onSelect(service: Tag)
  {
    if(this.selectedservices.includes(service))
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Service already in the list!');
      } else 
      {
        this.myModal.showBasic('Oops', 'Le service est déjà dans la liste!');
      }
    }
    else
    {
      this.selectedservices.push(service);
      const ind = this.services.indexOf(service);
      if (ind > -1) 
      {
        this.services.splice(ind, 1);
      }
    }
    //console.log(this.selectedservices);
  }

  secondSelect(service: Tag)
  {
    const index = this.selectedservices.indexOf(service);
    if (index > -1) 
    {
      this.services.push(service);
      this.selectedservices.splice(index, 1);
    }
    //console.log(this.selectedservices);
  }

  update()
  {
    const roomValue = this.roomForm.value;
    
    if(this.roomForm.valid)
    {
      
      if(this.ownedservices!== this.selectedservices || roomValue.roomIdentifier !== this.room.roomIdentifier || roomValue.location !== this.room.location || roomValue.size !== this.room.size || roomValue.image !== this.room.image)
      {
        //console.log(roomValue.roomIdentifier);
        //console.log(this.room.roomIdentifier);

        const formData :FormData =new FormData();
        formData.append('roomIdentifier',roomValue.roomIdentifier);
        formData.append('location',roomValue.location);
        formData.append('size',roomValue.size);
        formData.append('image',roomValue.image);

        for(let index in this.selectedservices)
        {
          formData.append('services[_ids][]',this.selectedservices[index].PK_id.toString());
        }

        this.roomsService.updateroom(this.room.PK_id, formData)
        .subscribe(success => { 
          if(success)
          {
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Success', 'Room has been modified successfully');
            } else 
            {
              this.myModal.showBasic('Succès', 'La salle a été modifié avec succès');
            }
          }else{
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Oops', 'Sorry, unable to modify the room');
            } else {
              this.myModal.showBasic('Oops', 'Désolé, la salle na pas pu être modifié');
            }
            
          }
        });
      }
      else
      {
        if(this.translate.currentLang == 'english')
        {
          this.myModal.showBasic('Oops', 'You need to make changes to be able to save.');
        } else 
        {
          this.myModal.showBasic('Oops', 'Vous devez effectuer des changement pour pouvoir sauvegarder.');
        }
        
      }
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form.');
      } else 
      {
        this.myModal.showBasic('Oops', 'Veuillez remplir le formulaire.');
      }
      
    }

  }


  return()
  {
    this.router.navigate(['/rooms/']);
  }

  ngOnInit() {
  }

}

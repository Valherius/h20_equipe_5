import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mentor-navbar',
  templateUrl: './mentor-navbar.component.html',
  styleUrls: ['./mentor-navbar.component.css']
})
export class MentorNavbarComponent implements OnInit {

  constructor(public translate: TranslateService) { this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English'); }

  ngOnInit() {
  }

}

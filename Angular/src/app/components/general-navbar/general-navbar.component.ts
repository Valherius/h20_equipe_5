import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-general-navbar',
  templateUrl: './general-navbar.component.html',
  styleUrls: ['./general-navbar.component.css']
})
export class GeneralNavbarComponent implements OnInit {

  currentUser: User;

  constructor(private authService: AuthService, private router: Router, public translate: TranslateService )
  {
    this.currentUser = JSON.parse(localStorage.getItem("CU"));
    this.translate.use(JSON.parse(localStorage.getItem("CL")));
  }

  ngOnInit() {
  }

  setLanguage(language: string)
  {
    this.translate.use(language);
    localStorage.setItem("CL", JSON.stringify(language));
  }

  redirectUser()
  {
    this.router.navigate(['/users']);
  }
  redirectSkill()
  {
    this.router.navigate(['/skills']);
  }

  redirectSubject()
  {
    this.router.navigate(['/subjects']);
  }

  redirectService()
  {
    this.router.navigate(['/services']);
  }

  redirectProfile()
  {
    this.router.navigate(['/users/', this.currentUser.PK_id])
  }

  logout()
  {
    this.authService.logout();
    this.router.navigate(['/']);
  }

}

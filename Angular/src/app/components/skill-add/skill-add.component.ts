import { Component, OnInit, ViewChild, Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { TagsService } from 'src/app/services/tags.service';
import { EventEmitter } from '@angular/core';
import { Tag } from 'src/app/models/tag.model';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ModalComponent } from '../modal/modal.component';
@Component({
  selector: 'app-skill-add',
  templateUrl: './skill-add.component.html',
  styleUrls: ['./skill-add.component.css']
})
export class SkillAddComponent implements OnInit {
  @ViewChild('form', {static: true}) formElement: NgForm;
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @Output() readonly tagCreate: EventEmitter<Tag> = new EventEmitter();
  tagForm: FormGroup;
  nameControl: FormControl;

  constructor(formBuilder: FormBuilder, private tagsService: TagsService,private router:Router, public translate: TranslateService) {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.nameControl = new FormControl('', Validators.required);

    this.tagForm = formBuilder.group(
      { 
        name: this.nameControl,
        description: new FormControl('')  
      }
    );
  }

  create() {
    const tagValue = this.tagForm.value;
    if(this.tagForm.valid)
    {
      this.tagsService.createskills(tagValue.name, tagValue.description)
      .subscribe(newTag => {
        //console.log(newTag);
        if(newTag)
        {
          //alert("Success.");
          this.router.navigate(['/skills']);
        }
        else
        {
          if(this.translate.currentLang == 'english')
          {
            this.myModal.showBasic('Failed', 'Unable to create the skill');
          } else 
          {
            this.myModal.showBasic('Echec', 'Echec lors de la création de la compétence');
          }
          
        }
      });
      this.tagForm.reset();
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
      } else 
      {
        this.myModal.showBasic('Oups', 'Veuillez completer le formulaire');
      }
      
    }
  }
  
  return()
  {
    this.router.navigate(['/skills/']);
  }
  
  ngOnInit() {
  }

}

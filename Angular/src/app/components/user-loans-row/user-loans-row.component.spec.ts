import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserLoansRowComponent } from './user-loans-row.component';

describe('UserLoansRowComponent', () => {
  let component: UserLoansRowComponent;
  let fixture: ComponentFixture<UserLoansRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserLoansRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserLoansRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, Output, EventEmitter, ElementRef, Renderer2 } from '@angular/core';
import { Loan } from 'src/app/models/loan.model';
import { LoansService } from 'src/app/services/loans.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { User } from 'src/app/models/user.model';
import { MatDialog } from '@angular/material';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

@Component({
  selector: '[app-user-loans-row]',
  templateUrl: './user-loans-row.component.html',
  styleUrls: ['./user-loans-row.component.css']
})
export class UserLoansRowComponent implements OnInit {
  @Output() readonly loanDelete: EventEmitter<any> = new EventEmitter();
  @Output() readonly loanReturn: EventEmitter<any> = new EventEmitter();
  @Input() loan:Loan;
  currentUser: User;
  isAdmini = true;
  thefine: number;
  isReturned = false;
  msg:string;

  constructor( private host: ElementRef, private renderer: Renderer2, private loansService:LoansService, private router: Router, public translate: TranslateService, private dialog: MatDialog) {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.currentUser = JSON.parse(localStorage.getItem("CU"));
    if(!this.currentUser.isAdmin)
    {
      this.isAdmini = false;
    }else{
      this.isAdmini = true;
    }


   }

  ngOnInit() {

    var date1= new Date;
    var date2 = new Date(this.loan.rent_endDate);
    this.thefine=Number(this.loan.fine);
    this.renderer.setStyle(this.host.nativeElement,'background-color',this.loan.rent_returnDate ? '':'' || date2 < date1 ? '#ff3300':'');
    if(this.loan.rent_returnDate)
    {
      this.isReturned = true;
    }
    else{
      this.isReturned = false;
    }
  } 


  return()
  {
      this.translate.get(['USER_LOAN.RETURN_MSG'])
      .subscribe(translations => {
        this.msg =translations['USER_LOAN.RETURN_MSG'];
      });
      const dialogRef = this.dialog.open(AlertDialogComponent, {
      data: this.msg
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result === 'confirm') {
          this.loansService.return(this.loan)
          .subscribe(success => this.loanReturn.emit());
        }
      });
  }


}

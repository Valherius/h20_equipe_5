import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { Mentor } from 'src/app/models/mentor.model';
import { FormGroup, NgForm, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MentorsService } from 'src/app/services/mentors.service';
import { TranslateService } from '@ngx-translate/core';
import { Tag } from 'src/app/models/tag.model';
import { TagsService } from 'src/app/services/tags.service';

import { ModalComponent } from '../modal/modal.component';


@Component({
  selector: 'app-mentor-view',
  templateUrl: './mentor-view.component.html',
  styleUrls: ['./mentor-view.component.css']
})
export class MentorViewComponent implements OnInit {

  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @ViewChild('form', { static: true }) formElement: NgForm;
  @Output() readonly mentorUpdate: EventEmitter<Mentor> = new EventEmitter();
  mentorForm: FormGroup;
  public mentor: Mentor;
  public skills: Tag[]=[];
  public selectedskills: Tag[]=[];
  public ownedskills: Tag[]=[];
  public highlightskill:any;
  public highlightselect:any;
  isImageSaved: boolean;
  imageError: string;
  public imagePath;
  imgURL: any;

  constructor(formBuilder: FormBuilder , private router: Router, private route: ActivatedRoute, private mentorsService:MentorsService, public translate: TranslateService, private tagsService: TagsService) 
  {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.mentorForm = formBuilder.group(
      {
          firstname: new FormControl('', Validators.required),
          lastname: new FormControl('', Validators.required),
          contactNumber: new FormControl('',[Validators.maxLength(10), Validators.minLength(10)]),
          email: new FormControl('', [Validators.required,Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]),
          image: new FormControl('')
      });

      tagsService.skills.subscribe(skills => this.skills = skills)

    this.mentorsService.getmentor(Number(this.route.snapshot.paramMap.get('id'))).subscribe(mentor => {
        //console.log(mentor.skills[0]);
        this.mentor = mentor;
        this.mentorForm.controls['firstname'].setValue(this.mentor.firstname);
        this.mentorForm.controls['lastname'].setValue(this.mentor.lastname);
        this.mentorForm.controls['contactNumber'].setValue(this.mentor.contactNumber);
        this.mentorForm.controls['email'].setValue(this.mentor.email);
        this.mentorForm.controls['image'].setValue(this.mentor.image);

        for(let index in this.mentor.skills)
        {
          for(let idx in this.skills)
          { //console.log(this.mentor.skills[index].PK_id+ " - "+ this.skills[idx].PK_id)
            if(this.skills[idx].PK_id==this.mentor.skills[index].PK_id)
            {
              this.selectedskills.push(this.skills[idx]);
              const ind = this.skills.indexOf(this.skills[idx]);
              if (ind > -1) 
              {
                this.skills.splice(ind, 1);
              }
            }
          }
        }
      })
    //console.log(this.mentor);
    }

  public highlightRow(skill) {
    this.highlightskill = skill.name;
  }
  public highlightRow2(skill) {
    this.highlightselect = skill.name;
  }


    imageSelected(fileInput: any)
  {
    //console.log(fileInput.target.files[0]);
    this.mentorForm.controls['image'].setValue(fileInput.target.files[0]);
    this.isImageSaved = true;
    
    const reader = new FileReader();
    
    this.imagePath = fileInput.target.files;
    reader.readAsDataURL(fileInput.target.files[0]); 
    reader.onload = (_fileInput) => { 
      this.imgURL = reader.result; 
    }
  }

  removeImage() {
    this.imagePath = null;
    this.imgURL=null;
    this.mentorForm.controls['image'].setValue(null);
    this.isImageSaved = false;
}

  onSelect(skill: Tag)
  {
    if(this.selectedskills.includes(skill))
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'This skill is already in the list');
      } else 
      {
        this.myModal.showBasic('Oups', 'La compétence est déjà dans la liste');
      }
      
    }
    else
    {
      this.selectedskills.push(skill);
      const ind = this.skills.indexOf(skill);
      if (ind > -1) 
      {
        this.skills.splice(ind, 1);
      }
    }
  }

  secondSelect(skill: Tag)
  {
    const index = this.selectedskills.indexOf(skill);
    if (index > -1) 
    {
      this.skills.push(skill);
      this.selectedskills.splice(index, 1);
    }
    //console.log(this.selectedskills);
  }
  

   update()
   {
     const mentorValue = this.mentorForm.value;
 
     if(this.mentorForm.valid)
     {
       if(this.ownedskills!== this.selectedskills || mentorValue.firstname !== this.mentor.firstname || mentorValue.lastname !== this.mentor.lastname || mentorValue.contactNumber !== this.mentor.contactNumber|| mentorValue.email !== this.mentor.email || mentorValue.image !== this.mentor.image)
       {
        const formData :FormData=new FormData();
        formData.append('firstname',mentorValue.firstname);
        formData.append('lastname',mentorValue.lastname);
        formData.append('contactNumber',mentorValue.contactNumber);
        formData.append('email',mentorValue.email);
        formData.append('image',mentorValue.image);
    
        for(let index in this.selectedskills)
        {
          formData.append('skills[_ids][]',this.selectedskills[index].PK_id.toString());
        }

         this.mentorsService.updatementor( this.mentor.PK_id, formData)
         .subscribe(success => { 
          if(success)
          {
            this.myModal.showBasic('Success', 'Mentor modified successfully');
          }else{
            this.myModal.showBasic('Oops', 'Unable to modify the mentor');
          }

           //this.router.navigate(['/mentors/']);
         });
       }
       else
       {
        this.myModal.showBasic('Oops', 'You need to make changes to be able to save.');
       }
     }
     else
     {
        this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
     }
 
   }
 
   return()
   {
     this.router.navigate(['/mentors/']);
   }
 
  ngOnInit() {
  }

}

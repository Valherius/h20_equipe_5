import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/models/user.model';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ModalComponent } from '../modal/modal.component';
@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})

export class UserAddComponent implements OnInit 
{
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @ViewChild('form', { static: true }) formElement: NgForm;
  @Output() readonly userCreate: EventEmitter<User> = new EventEmitter();
  userForm: FormGroup;
  nameControl: FormControl;
  public user: User;

  isImageSaved: boolean;
  imageError: string;
  public imagePath;
  imgURL: any;


  constructor(formBuilder: FormBuilder, private usersService: UsersService,private router:Router, public translate: TranslateService)
  {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.userForm = formBuilder.group(
    {
      username: new FormControl('', Validators.required),
      newPass: new FormControl('', Validators.required),
      confirmPass: new FormControl('', Validators.required),
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      image: new FormControl('', Validators.required),
      isAdmin: new FormControl(false)
    }
    );
  }

  ngOnInit()
  { 
    
  }

  imageSelected(fileInput: any)
  {
    //console.log(fileInput.target.files[0]);
    this.userForm.controls['image'].setValue(fileInput.target.files[0]);
    this.isImageSaved = true;
    
    const reader = new FileReader();
    
    this.imagePath = fileInput.target.files;
    reader.readAsDataURL(fileInput.target.files[0]); 
    reader.onload = (_fileInput) => { 
      this.imgURL = reader.result; 
    }
  }

  removeImage() {
    this.imagePath = null;
    this.imgURL=null;
    this.userForm.controls['image'].setValue(null);
    this.isImageSaved = false;
}

  create() 
  {
    const userValue = this.userForm.value;
    //console.log(this.userForm.value);
    if(this.userForm.valid)
    {
      const formData :FormData=new FormData();
      formData.append('username',userValue.username);
      formData.append('password',userValue.newPass);
      formData.append('firstname',userValue.firstname);
      formData.append('lastname',userValue.lastname);
      formData.append('address',userValue.address);
      
      formData.append('image',userValue.image);
      
      formData.append('isAdmin', `${Number(userValue.isAdmin)}`);

      //userValue.username, userValue.password, userValue.firstname, userValue.lastname, userValue.address, userValue.image, userValue.isAdmin
      this.usersService.create(formData)
      .subscribe(result => {
        //console.log(result);
        if(result)
        {
          if(this.translate.currentLang == 'english')
          {
            this.myModal.showBasic('Success', 'Successfully created the user');
          } else 
          {
            this.myModal.showBasic('Succès', 'Utilisateur créer avec succès');
          }

          this.formElement.resetForm();
          this.userForm.value.image = null;
          this.userForm.controls['image'].setValue(null);
          this.removeImage();
        }else{
          if(this.translate.currentLang == 'english')
          {
            this.myModal.showBasic('Error', 'Unable to create the user, the username is taken');
          } else 
          {
            this.myModal.showBasic('Erreur', 'Lusager na pas pu être créé, le nom dutilisateur est pris');
          }
        }
      });
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
      } else 
      {
        this.myModal.showBasic('Oups', 'Veuillez remplir le formulaire');
      }
    }
  }

  return()
  {
    this.router.navigate(['/users/']);
  }

}
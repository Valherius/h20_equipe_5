import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { Book } from 'src/app/models/book.model';
import { BooksService } from 'src/app/services/books.service';
import { Router, ActivatedRoute, RouteReuseStrategy } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Tag } from 'src/app/models/tag.model';
import { TagsService } from 'src/app/services/tags.service';
import { ModalComponent } from '../modal/modal.component';


@Component({
  selector: 'app-book-view',
  templateUrl: './book-view.component.html',
  styleUrls: ['./book-view.component.css']
})
export class BookViewComponent implements OnInit {

  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @ViewChild('form', { static: true }) formElement: NgForm;
  @Output() readonly bookUpdate: EventEmitter<Book> = new EventEmitter();
  bookForm: FormGroup;
  public book: Book;
  public subjects: Tag[]=[];
  public selectedsubjects: Tag[]=[];
  public ownedsubjects: Tag[]=[];
  public highlightsubject:any;
  public highlightselect:any;
  isImageSaved: boolean;
  imageError: string;
  public imagePath;
  imgURL: any;


  constructor(formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private booksService: BooksService,public translate: TranslateService, private tagsService: TagsService) 
  { 

    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    
    this.bookForm = formBuilder.group(
      {
        name: new FormControl('', Validators.required),
        author: new FormControl('', Validators.required),
        synopsis: new FormControl('', Validators.required),
        published_date: new FormControl('', Validators.required),
        image: new FormControl('')
      
      });
      tagsService.subjects.subscribe(subjects => this.subjects = subjects)
      
      this.booksService.getbook(Number(this.route.snapshot.paramMap.get('id'))).subscribe(book =>
      {
        this.book= book; 
        this.bookForm.controls['name'].setValue(this.book.name);
        this.bookForm.controls['author'].setValue(this.book.author);
        this.bookForm.controls['synopsis'].setValue(this.book.synopsis);
        this.bookForm.controls['published_date'].setValue(this.book.published_date.substr(0,10));
        this.bookForm.controls['image'].setValue(this.book.image);
       //console.log(1);
       //console.log(this.bookForm);

        for(let index in this.book.subjects)
        {
          for(let idx in this.subjects)
          { //console.log(this.book.subjects[index].PK_id+ " - "+ this.subjects[idx].PK_id)
            if(this.subjects[idx].PK_id==this.book.subjects[index].PK_id)
            {
              this.selectedsubjects.push(this.subjects[idx]);
              this.ownedsubjects.push(this.subjects[idx]);
              //console.log(this.selectedsubjects);
            }
          }
        }
       
      });
    //console.log(this.book);
  }

  public highlightRow(subject) {
    this.highlightsubject = subject.name;
  }
  public highlightRow2(subject) {
    this.highlightselect = subject.name;
  }

  imageSelected(fileInput: any)
  {
    //console.log(fileInput.target.files[0]);
    this.bookForm.controls['image'].setValue(fileInput.target.files[0]);
    this.isImageSaved = true;
    
    const reader = new FileReader();
    
    this.imagePath = fileInput.target.files;
    reader.readAsDataURL(fileInput.target.files[0]); 
    reader.onload = (_fileInput) => { 
      this.imgURL = reader.result; 
    }
  }

  removeImage() {
    this.imagePath = null;
    this.imgURL=null;
    this.bookForm.controls['image'].setValue(null);
    this.isImageSaved = false;
}

  onSelect(subject: Tag)
  {
    if(this.selectedsubjects.includes(subject))
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Service already in the list!');
      } else 
      {
        this.myModal.showBasic('Oops', 'Le service est déjà dans la liste!');
      }
    }
    else
    {
      this.selectedsubjects.push(subject);
      const ind = this.subjects.indexOf(subject);
      if (ind > -1) 
      {
        this.subjects.splice(ind, 1);
      }
    }
    //console.log(this.selectedsubjects);
  }

  secondSelect(subject: Tag)
  {
    const index = this.selectedsubjects.indexOf(subject);
    if (index > -1) 
    {
      this.subjects.push(subject);
      this.selectedsubjects.splice(index, 1);
    }
    //console.log(this.selectedsubjects);
  }
  

  update()
  {
    const bookValue = this.bookForm.value;
    //console.log(bookValue);
    if(this.bookForm.valid)
    {
      if(this.ownedsubjects!== this.selectedsubjects ||bookValue.name !== this.book.name || bookValue.author !== this.book.author || bookValue.synopsis !== this.book.synopsis || bookValue.image !== this.book.image || bookValue.published_date !== this.book.published_date)
      {
        const formData :FormData=new FormData();
        formData.append('name',bookValue.name);
        formData.append('author',bookValue.author);
        formData.append('synopsis',bookValue.synopsis);
        formData.append('image',bookValue.image);
        formData.append('published_date',bookValue.published_date);
   
        for(let index in this.selectedsubjects)
        {
          formData.append('subjects[_ids][]',this.selectedsubjects[index].PK_id.toString());
        }
       
        this.booksService.updatebook(this.book.PK_id, formData)
        .subscribe(success => { if(success)
          {
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Success', 'Book has been modified successfully');
            } else 
            {
              this.myModal.showBasic('Succès', 'Le livre a été modifié avec succès');
            }
          }else{
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Oops', 'Sorry, unable to modify the book');
            } else {
              this.myModal.showBasic('Oops', 'Désolé, le livre na pas pu être modifié');
            }      
          }
        });
      }
      else
      {
        if(this.translate.currentLang == 'english')
        {
          this.myModal.showBasic('Oops', 'You need to make changes to be able to save.');
        } else 
        {
          this.myModal.showBasic('Oops', 'Vous devez effectuer des changement pour pouvoir sauvegarder.');
        }
      }
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form.');
      } else 
      {
        this.myModal.showBasic('Oops', 'Veuillez remplir le formulaire.');
      }
    }
  }

  return()
  {
    this.router.navigate(['/books/']);
  }

  ngOnInit() {
  }

}







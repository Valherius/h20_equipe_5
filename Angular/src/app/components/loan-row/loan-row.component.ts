import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Loan } from 'src/app/models/loan.model';
import { DatePipe } from '@angular/common'
@Component({
  selector: '[app-loan-row]',
  templateUrl: './loan-row.component.html',
  styleUrls: ['./loan-row.component.css']
})
export class LoanRowComponent implements OnInit {
@Input() loan: Loan;
start:string;
end:string;


  constructor(public datepipe: DatePipe) { }

  ngOnInit() {
    this.start=this.datepipe.transform(this.loan.rent_beginDate, 'yyyy-MM-dd');
    this.end=this.datepipe.transform(this.loan.rent_endDate, 'yyyy-MM-dd');
  }

}

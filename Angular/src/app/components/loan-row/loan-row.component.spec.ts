import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanRowComponent } from './loan-row.component';

describe('LoanRowComponent', () => {
  let component: LoanRowComponent;
  let fixture: ComponentFixture<LoanRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoanRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

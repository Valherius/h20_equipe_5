import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, NgForm } from '@angular/forms';
import { LoansService } from 'src/app/services/loans.service';
import { MentorsService } from 'src/app/services/mentors.service';
import { UsersService } from 'src/app/services/users.service';
import { BooksService } from 'src/app/services/books.service';
import { RoomsService } from 'src/app/services/rooms.service';
import { Loan } from 'src/app/models/loan.model';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Item } from 'src/app/models/item.model';
import { User } from 'src/app/models/user.model';

import { ModalComponent } from '../modal/modal.component';


@Component({
  selector: 'app-loan-add',
  templateUrl: './loan-add.component.html',
  styleUrls: ['./loan-add.component.css']
})
export class LoanAddComponent implements OnInit {
  @ViewChild('form', { static: true }) formElement: NgForm;
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @Output() readonly loanCreate: EventEmitter<Loan> = new EventEmitter();
  rentForm: FormGroup;
  nameControl: FormControl;
  public item: Item;
  public selecteditem: Item=new Item(666,'None');
  public selecteduser: User=null;
  public mentors: Item[]=[];
  public rooms: Item[]=[];
  public books: Item[]=[];
  public highlightitem:any;

  public item_type:number;
  mentorSelected=false;
  roomSelected=false;
  bookSelected=false;
  startDate: Date;
  endDate: Date;
  start:string;
  end:string;
  selected:boolean;
  public itemloans:Loan[]=[];
  haveLoans:boolean;

  constructor(formBuilder: FormBuilder, private route: ActivatedRoute, private loansService: LoansService,private usersService: UsersService,private mentorsService: MentorsService,private roomsService: RoomsService,private booksService: BooksService,private router:Router, public translate: TranslateService) {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
   
    this.usersService.getuser(Number(this.route.snapshot.paramMap.get('id'))).subscribe(user => {
      this.selecteduser = user;
      //console.log(user);
    });
    this.selected=false;

    mentorsService.items.subscribe(items =>{
      this.mentors = items; });

    roomsService.items.subscribe(rooms =>{
      this.rooms = rooms; });

    booksService.items.subscribe( books =>{
      this.books = books; });



    this.rentForm = formBuilder.group(
      {
        item_type: new FormControl(''),
        begin_date: new FormControl(''),
        begin_hour: new FormControl('',[Validators.min(0),Validators.max(23)]),
        end_date: new FormControl(''),
        end_hour: new FormControl('',[Validators.min(0),Validators.max(23)])
      }
      );
      
  }

  ngOnInit() {
  }

  public highlightRow(item) {
    this.highlightitem = item.name;
  }

  onSelect(item: Item)
  {
    this.itemloans=[];

    //console.log(item);
    this.selecteditem=item; 
    this.selected=true;
    this.loansService.getselectedloans(this.selecteditem.PK_id,this.item_type).subscribe(itemloans => {
      this.itemloans = itemloans;
      if(this.itemloans.length>0)
      {this.haveLoans=true;}
      else
      {this.haveLoans=false;}
    });
  }

  typechanger(event)
  {
    this.haveLoans=false;
    this.selecteditem=new Item(666,'None');
    this.selected=false;

    if(this.rentForm.value.item_type=='mentor')
    {    
      //console.log(this.rentForm.value.end_date);
      this.item_type=0;

      this.mentorSelected=true;
      this.roomSelected=false;
      this.bookSelected=false;
      
    }
    else if(this.rentForm.value.item_type=='room')
    {    
      this.item_type=1;
      this.mentorSelected=false;
      this.roomSelected=true;
      this.bookSelected=false;
    }
    else if(this.rentForm.value.item_type=='book')
    {  
      this.item_type=2;
      this.mentorSelected=false;
      this.roomSelected=false;
      this.bookSelected=true;
    }
    else
    {
      this.mentorSelected=false;
      this.roomSelected=false;
      this.bookSelected=false;
    }
  }

  create() 
  {
    if(this.rentForm.value.end_date && this.rentForm.value.begin_date && this.selecteditem.PK_id!=666 &&  this.selecteditem.PK_id && this.rentForm.value.begin_hour<=24 && this.rentForm.value.begin_hour>=0 && this.rentForm.value.end_hour<=24 && this.rentForm.value.end_hour>=0)
    {
      var start:string;
      var end:string;
      start=this.rentForm.value.begin_date.toString()+ ' '+this.rentForm.value.begin_hour.toString()+':00:00';
      end=this.rentForm.value.end_date.toString()+ ' '+this.rentForm.value.end_hour.toString()+':00:00';
      var begin:Date;
      begin= new Date(this.rentForm.value.begin_date);
      var today:Date;
      today=new Date();
      if(begin<=today || this.rentForm.value.begin_date > this.rentForm.value.end_date)
      {
        if(this.translate.currentLang == 'english')
        {
          this.myModal.showBasic('Error', 'Going back in time? You cannot start a rent in the past or make it end before it started!');
        } else 
        {
          this.myModal.showBasic('Erreur', 'Vous retournez dans le passé? Un emprunt ne peut pas commencer dans le passé et ne peut pas finir avant son début!');
        }
        
      }
      else
      {
        this.loansService.create(this.item_type,this.selecteditem.PK_id,this.selecteduser.PK_id, start, end )
        .subscribe(result => {
          //console.log(result);
          if(result)
          {
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Success', 'Rent created successfully');
            } else 
            {
              this.myModal.showBasic('Succès', 'Emprunt créer avec succès');
            }
            
            this.formElement.resetForm();
            this.mentorSelected=false;
            this.roomSelected=false;
            this.bookSelected=false;
            this.selecteditem=new Item(666,'None');
          }else{
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Oops', 'Unable to create rent');
            } else 
            {
              this.myModal.showBasic('Oups', 'Echec lors de la création de lemprunt');
            }
            
          }
        });
      }
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
      } else 
      {
        this.myModal.showBasic('Oups', 'Veuillez completer le formulaire');
      }
      
    }
  }
  

  return()
  {this.router.navigate(['/users/']);}

}

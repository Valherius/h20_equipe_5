import { Component, OnInit, Input, ViewChild} from '@angular/core';
import { Router, ActivatedRoute, RouteReuseStrategy } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Tag } from '../../models/tag.model';
import { TagsService } from '../../services/tags.service';
import { TranslateService } from '@ngx-translate/core';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-service-view',
  templateUrl: './service-view.component.html',
  styleUrls: ['./service-view.component.css']
})
export class ServiceViewComponent implements OnInit {
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  private service: Tag;

  tagForm: FormGroup;

  constructor(formBuilder: FormBuilder, private router: Router, private route: ActivatedRoute, private tagsService: TagsService, public translate: TranslateService) { 
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
    this.tagForm = formBuilder.group(
      {
        name: new FormControl('', Validators.required),
        description: new FormControl(''),
      });

    this.tagsService.getservice(Number(this.route.snapshot.paramMap.get('id'))).subscribe(service => {
      this.service = service;
      this.tagForm.controls['name'].setValue(this.service.name);
      this.tagForm.controls['description'].setValue(this.service.description);
      })
  }


  update()
  {
    const tagValue = this.tagForm.value;
    
    if(this.tagForm.valid)
    {
      if(tagValue.name !== this.service.name || tagValue.description !== this.service.description)
      {
        //console.log(tagValue.name);
        this.tagsService.updateservice(this.service.PK_id, tagValue.name, tagValue.description)
        .subscribe(success => {
          if(success)
          {
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Success', 'The service has been modified successfully');
            } else 
            {
              this.myModal.showBasic('Succès', 'Le service a été modifié avec succès');
            }
          }else{
            if(this.translate.currentLang == 'english')
            {
              this.myModal.showBasic('Oops', 'Sorry, unable to modify the service');
            } else {
              this.myModal.showBasic('Oops', 'Désolé, le service na pas pu être modifié');
            }
            
          }
        });
      }
      else
      {
        if(this.translate.currentLang == 'english')
        {
          this.myModal.showBasic('Error', 'You need to make changes to be able to save');
        } else 
        {
          this.myModal.showBasic('Erreur', 'Vous devez effectuer des changement pour sauvegarder');
        }
      }
    }
    else
    {
      if(this.translate.currentLang == 'english')
      {
        this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
      } else 
      {
        this.myModal.showBasic('Oops', 'Veuillez remplir le formulaire correctement');
      }
    }
  }

  return()
  {
    this.router.navigate(['/services/']);
  }

  ngOnInit() {
  }

}

import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { BookReport } from 'src/app/models/bookReport.model';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: '[app-book-report-row]',
  templateUrl: './book-report-row.component.html',
  styleUrls: ['./book-report-row.component.css']
})
export class BookReportRowComponent implements OnInit {
  @Input() bookdata: BookReport;
  
  constructor( public translate: TranslateService) {
    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
   }

   

  ngOnInit() {
  }

}

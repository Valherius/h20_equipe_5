import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookReportRowComponent } from './book-report-row.component';

describe('BookReportRowComponent', () => {
  let component: BookReportRowComponent;
  let fixture: ComponentFixture<BookReportRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookReportRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookReportRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

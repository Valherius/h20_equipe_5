import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectRowComponent } from './subject-row.component';

describe('SubjectRowComponent', () => {
  let component: SubjectRowComponent;
  let fixture: ComponentFixture<SubjectRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubjectRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

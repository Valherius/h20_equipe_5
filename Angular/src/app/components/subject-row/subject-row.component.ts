import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Tag } from 'src/app/models/tag.model';
import { TagsService } from 'src/app/services/tags.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

@Component({
  selector: '[app-subject-row]',
  templateUrl: './subject-row.component.html',
  styleUrls: ['./subject-row.component.css']
})
export class SubjectRowComponent implements OnInit {
  @Input() tag: Tag;
  @Output() subjectDelete: EventEmitter<any> = new EventEmitter();
  msg:string;
  constructor(private tagsService: TagsService, private router: Router,  private dialog: MatDialog, public translate: TranslateService) { this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English'); }

  view() {
    this.router.navigate(['/subjects/', this.tag.PK_id]);
  }

  delete(){

    this.translate.get(['SUBJECT_MSG.DELETE'])
      .subscribe(translations => {
        this.msg =translations['SUBJECT_MSG.DELETE'] + this.tag.name + "?" ;
      });
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      data: this.msg
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'confirm') {
        this.tagsService.deletesubject(this.tag.PK_id)
        .subscribe(success => this.subjectDelete.emit());
        this.subjectDelete.emit();
      }
    });
  }
  

  ngOnInit() {
  }

}

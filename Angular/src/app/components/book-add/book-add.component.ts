import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup, NgForm } from '@angular/forms';
import { BooksService } from 'src/app/services/books.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Book } from 'src/app/models/book.model';
import { Observable } from 'rxjs';
import { Tag } from 'src/app/models/tag.model';
import { TagsService } from 'src/app/services/tags.service';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-book-add',
  templateUrl: './book-add.component.html',
  styleUrls: ['./book-add.component.css']
})
export class BookAddComponent implements OnInit {

  @ViewChild('form', { static: true }) formElement: NgForm;
  @ViewChild('myModal', { static: true }) myModal: ModalComponent;
  @Output() readonly tagCreate: EventEmitter<Book> = new EventEmitter();
  public book: Book;
  bookForm: FormGroup;
  subjectForm: FormGroup;
  nameControl: FormControl;
  public subjects: Tag[]=[];
  public selectedSubjects: Tag[]=[];
  public highlightsubject:any;
  public highlightselect:any;
  isImageSaved: boolean;
  imageError: string;
  public imagePath;
  imgURL: any;



  constructor(formBuilder: FormBuilder, private booksService: BooksService, private router: Router, public translate: TranslateService,public tagsService: TagsService)
   { 

    this.translate.use(JSON.parse(localStorage.getItem("CL"))||'English');
  
    this.bookForm = formBuilder.group(
      { 
        published_date: new FormControl('', Validators.required),
        name: new FormControl('',Validators.required),
        synopsis: new FormControl(''),  
        author: new FormControl('', Validators.required),
        image: new FormControl('', Validators.required)
      }
    );

    tagsService.subjects.subscribe(subjects => this.subjects = subjects);

  }

  public highlightRow(subject) {
    this.highlightsubject = subject.name;
  }
  public highlightRow2(subject) {
    this.highlightselect = subject.name;
  }

  ngOnInit() {
  }

  imageSelected(fileInput: any)
  {
    //console.log(fileInput.target.files[0]);
    this.bookForm.controls['image'].setValue(fileInput.target.files[0]);
    this.isImageSaved = true;
    
    const reader = new FileReader();
    
    this.imagePath = fileInput.target.files;
    reader.readAsDataURL(fileInput.target.files[0]); 
    reader.onload = (_fileInput) => { 
      this.imgURL = reader.result; 
    }
  }

  removeImage() {
    this.imagePath = null;
    this.imgURL=null;
    this.bookForm.controls['image'].setValue(null);
    this.isImageSaved = false;
}

  onSelect(subject: Tag)
  {
    if(this.selectedSubjects.includes(subject))
    {
      this.myModal.showBasic('Oops', 'This subject is already in the list');
      alert("This subject is already in the list");
    }
    else
    {
      this.selectedSubjects.push(subject);
    }
    //console.log(this.selectedSubjects);
  }

  secondSelect(subject: Tag)
  {
    const index = this.selectedSubjects.indexOf(subject);
    if (index > -1) 
    {
      this.selectedSubjects.splice(index, 1);
    }
    //console.log(this.selectedSubjects);
  }



  create() {
    const bookValue = this.bookForm.value;
    //console.log(this.bookForm.value);
    if(this.bookForm.valid)
    { //console.log(this.bookForm.value);


      const formData :FormData=new FormData();
      formData.append('published_date',bookValue.published_date);
      formData.append('name',bookValue.name);
      formData.append('author',bookValue.author);
      formData.append('synopsis',bookValue.synopsis);
      formData.append('image',bookValue.image);

      for(let index in this.selectedSubjects)
      {
        formData.append('subjects[_ids][]',this.selectedSubjects[index].PK_id.toString());
      }

      this.booksService.create(formData)
      .subscribe(result => {
        //console.log(result);
        if(result)
        {
          this.myModal.showBasic('Success', 'Book created successfully');
          this.formElement.resetForm();
          this.selectedSubjects=[];
          this.bookForm.value.image = null;
          this.bookForm.controls['image'].setValue(null);
        }else{
          alert("Unable to create book");
          this.myModal.showBasic('Oops', 'Unable to create book');
        }
      });   
    }
    else
    {
      alert("Please fill out the form appropriately");
      this.myModal.showBasic('Oops', 'Please fill out the form appropriately');
    }
  }


  return()
  {
    this.router.navigate(['/books/']);
  }


}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillListNacbarComponent } from './skill-list-nacbar.component';

describe('SkillListNacbarComponent', () => {
  let component: SkillListNacbarComponent;
  let fixture: ComponentFixture<SkillListNacbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillListNacbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillListNacbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input } from '@angular/core';
import { RoomReport } from 'src/app/models/roomReport.model';

@Component({
  selector: '[app-room-report-row]',
  templateUrl: './room-report-row.component.html',
  styleUrls: ['./room-report-row.component.css']
})
export class RoomReportRowComponent implements OnInit {
  @Input() rdata: RoomReport;
  constructor() { }

  ngOnInit() {
  }

}

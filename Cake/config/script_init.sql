SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';



-- -----------------------------------------------------
-- Table `USERS`
-- -----------------------------------------------------

DROP TABLE IF EXISTS `users` ;

CREATE TABLE `users` (
  `PK_id` INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(64) NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `address` VARCHAR(100) NOT NULL,
  `image` LONGTEXT NOT NULL,
  `balance` VARCHAR(45) NULL,
  `isAdmin` TINYINT NOT NULL,
  `createdAt` DATETIME NULL DEFAULT now(),
  `modifiedAt` DATETIME NULL ,
  `deletedAt` DATETIME NULL DEFAULT null,
  PRIMARY KEY (`PK_id`))
  
ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;

-- -----------------------------------------------------
-- Table `mentors`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mentors` ;

CREATE TABLE `mentors` (
  `PK_id` INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `contactNumber` VARCHAR(45) NULL,
  `email` VARCHAR(45) NOT NULL,
  `image` LONGTEXT NOT NULL,
  `createdAt` DATETIME NULL DEFAULT now(),
  `modifiedAt` DATETIME NULL ,
  `deletedAt` DATETIME NULL ,
  PRIMARY KEY (`PK_id`))
ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;


-- -----------------------------------------------------
-- Table `skills`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `skills` ;

CREATE TABLE`skills` (
  `PK_id` INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL UNIQUE,
  `description` VARCHAR(500) NULL,
  `createdAt` DATETIME NULL DEFAULT now(),
  `modifiedAt` DATETIME NULL,
  PRIMARY KEY (`PK_id`))
  
ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;


-- -----------------------------------------------------
-- Table `mentor_skills`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `mentor_skills` ;

CREATE TABLE `mentor_skills` (
  `FK_mentor_id` INT UNSIGNED NOT NULL,
  `FK_skill_id` INT UNSIGNED NOT NULL,
  CONSTRAINT `FK_mentor_id`
    FOREIGN KEY (`FK_mentor_id`)
    REFERENCES `mentors` (`PK_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_skill_id`
    FOREIGN KEY (`FK_skill_id`)
    REFERENCES `skills` (`PK_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)

ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;


-- -----------------------------------------------------
-- Table `rooms`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rooms` ;

CREATE TABLE `rooms` (
  `PK_id` INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
  `roomIdentifier` VARCHAR(45) NOT NULL,
  `location` VARCHAR(45) NOT NULL,
  `size` INT NULL,
  `image` LONGTEXT NOT NULL,
  `createdAt` DATETIME NULL DEFAULT NOW(),
  `modifiedAt` DATETIME NULL ,
  `deletedAt` DATETIME NULL ,
  PRIMARY KEY (`PK_id`))

ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;


-- -----------------------------------------------------
-- Table `services`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `services` ;

CREATE TABLE `services` (
  `PK_id` INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL UNIQUE,
  `description` VARCHAR(500) NULL,
  `createdAt` DATETIME NULL DEFAULT NOW(),
  `modifiedAt` DATETIME NULL,
  PRIMARY KEY (`PK_id`))

ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;


-- -----------------------------------------------------
-- Table `room_services`
-- -----------------------------------------------------
DROP TABLE IF EXISTS`room_services` ;

CREATE TABLE `room_services` (
  `FK_room_id` INT UNSIGNED NOT NULL,
  `FK_service_id` INT UNSIGNED NOT NULL,
  CONSTRAINT `FK_room_id`
    FOREIGN KEY (`FK_room_id`)
    REFERENCES `rooms` (`PK_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_service_id`
    FOREIGN KEY (`FK_service_id`)
    REFERENCES `services` (`PK_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)

ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;


-- -----------------------------------------------------
-- Table `books`
-- -----------------------------------------------------
DROP TABLE IF EXISTS`books` ;

CREATE TABLE `books` (
  `PK_id` INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `author` VARCHAR(45) NOT NULL,
  `synopsis` VARCHAR(500) NULL,
  `image` LONGTEXT NOT NULL,
  `createdAt` DATETIME  NULL DEFAULT NOW(),
  `modifiedAt` DATETIME NULL,
  `deletedAt` DATETIME NULL ,
  `published_date` DATETIME NOT NULL,
  PRIMARY KEY (`PK_id`))

ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;


-- -----------------------------------------------------
-- Table `subjects`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `subjects` ;

CREATE TABLE  `subjects` (
  `PK_id` INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
  `name` VARCHAR(45)NOT NULL UNIQUE,
  `description` VARCHAR(500) NULL,
  `createdAt` DATETIME NULL DEFAULT NOW(),
  `modifiedAt` DATETIME NULL,
  PRIMARY KEY (`PK_id`))

ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;


-- -----------------------------------------------------
-- Table `book_subjects`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `book_subjects` ;

CREATE TABLE `book_subjects` (
  `FK_book_id` INT UNSIGNED NOT NULL,
  `FK_subject_id` INT UNSIGNED NOT NULL,
  CONSTRAINT `FK_book_id`
    FOREIGN KEY (`FK_book_id`)
    REFERENCES `books` (`PK_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_subject_id`
    FOREIGN KEY (`FK_subject_id`)
    REFERENCES `subjects` (`PK_id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)

ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;


-- -----------------------------------------------------
-- Table `RENTING`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `renting` ;

CREATE TABLE `renting` (
  `PK_rent_id` INT UNSIGNED NOT NULL UNIQUE AUTO_INCREMENT,
  `item_type` INT NOT NULL,
  `FK_item_id` INT UNSIGNED NOT NULL,
  `FK_user_id` INT UNSIGNED NOT NULL,
  `rent_beginDate` VARCHAR(45) NOT NULL,
  `rent_endDate` VARCHAR(45) NOT NULL,
  `rent_returnDate` VARCHAR(45) NULL,
  `fine` DECIMAL NULL,
  PRIMARY KEY (`PK_rent_id`),
  CONSTRAINT `renting_FK_user_id`
    FOREIGN KEY (`FK_user_id`)
    REFERENCES `users` (`PK_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `renting_FK_item_id_Book`
    FOREIGN KEY (`FK_item_id`)
    REFERENCES `books` (`PK_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `renting_FK_item_id_Room`
    FOREIGN KEY (`FK_item_id`)
    REFERENCES `rooms` (`PK_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `renting_FK_item_id_Mentor`
    FOREIGN KEY (`FK_item_id`)
    REFERENCES `mentors` (`PK_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)

ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE UNIQUE INDEX PK_rent_id_UNIQUE ON renting(PK_rent_id);
CREATE INDEX renting_FK_user_id_idx ON renting(FK_user_id);
CREATE INDEX renting_FK_item_id_Book_idx ON renting(FK_item_id);
CREATE UNIQUE INDEX service_name_UNIQUE ON services(name);
CREATE UNIQUE INDEX service_PK_id_UNIQUE ON service(PK_id);
CREATE UNIQUE INDEX room_roomIdentifier_UNIQUE ON rooms(roomIdentifier);
CREATE UNIQUE INDEX room_PK_id_UNIQUE ON rooms(PK_id);
CREATE INDEX mentorskill_FK_skill_id_idx ON mentor_skills(FK_skill_id);
CREATE INDEX mentorskill_FK_mentor_id_idx ON mentor_skills(FK_mentor_id);
CREATE UNIQUE INDEX skill_name_UNIQUE ON skills(name);
CREATE UNIQUE INDEX skill_PK_id_UNIQUE ON skills(PK_id);
CREATE UNIQUE INDEX mentor_email_UNIQUE ON mentors(email);
CREATE UNIQUE INDEX mentor_PK_id_UNIQUE ON mentors(PK_id);
CREATE UNIQUE INDEX user_username_UNIQUE ON users(username);
CREATE UNIQUE INDEX user_PK_id_UNIQUE ON users(PK_id);
CREATE INDEX roomservice_FK_service_id_idx ON room_services(FK_room_id);
CREATE INDEX roomservice_FK_room_id_idx ON room_services(FK_service_id);
CREATE UNIQUE INDEX book_PK_id_UNIQUE ON books(PK_id);
CREATE INDEX booksubject_FK_book_id_idx ON book_subjects(FK_book_id);
CREATE INDEX booksubject_FK_subject_id_idx ON book_subjects(FK_subject_id);

-- -----------------------------------------------------
-- CONSTRAINT
-- -----------------------------------------------------
ALTER TABLE users
ADD  CONSTRAINT user_check_username CHECK (TRIM(username) != '' ),
ADD CONSTRAINT user_check_password_min_7_char CHECK ((TRIM(password) != ''));

ALTER TABLE mentors
ADD CONSTRAINT mentor_check_email CHECK (REGEXP_LIKE(email, '^(.+)@(.+).(.+)$'));

ALTER TABLE skills
ADD CONSTRAINT skill_check_name_not_empty CHECK (TRIM(name) != '' );

ALTER TABLE services
ADD CONSTRAINT service_check_name_not_empty CHECK (TRIM(name) != '' AND name != null);


ALTER TABLE books
ADD CONSTRAINT book_check_name_not_empty CHECK (TRIM(name) != ''),
ADD CONSTRAINT book_check_author_not_empty CHECK (TRIM(author) != '');


ALTER TABLE subjects
ADD CONSTRAINT subject_check_name_not_empty CHECK (TRIM(name) != '');


-- -----------------------------------------------------
-- TRIGGERS
-- -----------------------------------------------------



-- -----------------------------------------------------
-- INSERT users
-- -----------------------------------------------------
insert into users (PK_id, username, password, firstname, lastname, address, image, balance, isAdmin, createdAt, modifiedAt, deletedAt) values (1, 'maasaf0', 'Bw4LoJ', 'Myrilla', 'Aasaf', '1 Arkansas Way', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHcSURBVDjLhZPbahpRFIbnJXLb4lsIQx+sF6G0kMsmpZQ8hEeUGWcUTbQqnlDUUTwgIkaj4kUI0j3ozObvXjs4jXjoxbpZe//f/689a5Rut4tOp4N2u41Wq4Vms4lGo4F6vY5arXYFQLlUimVZ4Jwf1Ww2k5ByuXwRopAzCabTqXSeTCYehHoiBQqFwlmIQpHpMrlRo1qt1jebDRzHkX0ClkolZLPZkxCFXPcXhXgrIk9t24bz8gyna8qz8XiMfD6PTCZzBFHIeR/ZdV2QmL+u4Bpf4cY/C4ghz0ajEaVAMpk8gChiRrZer+Wl3W4nnd3EF/CH7+C5n+ACtIcMh0NKAV3XPYhSqVQ+iRnZarV6gzw1pTN/vAPP3x9BBoMBpUAsFpMQSSkWi6qYkS2XyzfI3IKjixSPP/5BRCrH0uR5r9ejFIhEIlfeLLlcThUzssVicQz5/Qs8eYM/+g2468gUhmEgFAp9PHhRMZ+aTqfZfD73IDvtGtz8Bjtzhy3bvBf7vBHeVyqVUk3TZLSJEjJrw3m4Bd/anjgYDPq8Rzy1HIlEQtU0jdEm7j8xiUX/QHwWQBWPx/3ipRktWL/fPym+CKCKRqP+cDjMSBwIBHwnV/l/v6tw9Qvxh3PnfwF+wjbwD++YrQAAAABJRU5ErkJggg==', 61.52, false, '2019-03-28 00:07:12', '2019-03-27 08:02:35', '2019-04-28 08:50:30');
insert into users (PK_id, username, password, firstname, lastname, address, image, balance, isAdmin, createdAt, modifiedAt, deletedAt) values (2, 'gstenhouse1', 'm6XLnIcLHiqf', 'Gilbertine', 'Stenhouse', '55829 Hermina Terrace', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKnSURBVDjLdVJNSFRRFP7e+H7MZ06TyjCSDpZCaS93BpX9EUEEhUJkBS0KJDBoES6ibRujIqJcBUW0Khft2qQZtWigEYLBUgxz0BnHqWbUeTPv/3Xue/5k2eXdex73nO873zn3cK7r4tS1J61kX7pADi72uXTQNdjhG3d1O7YJU1+Yevfs5nZygYcf0EOBO5WWBm4+u+CBfQDgkzmgD/kFFSWdSFy+EcvLJwAOKi313JE99WiIKB6YoR0GZNbxs9duqUBFOY/9p69gHQF5Q0prFK8/TSKXK1I2x7t2VlQwSyT5xSI03cCfa7kEcLzIo3l3FE3hrbBteLJZZtumTYQW/Zc0Ey8GRzYiYIEOpucXIQYEaJpGjTI8ELu3UAZeEFEjS56qfwmYXBZIHdZ1HZ3tTb58X52X4OnbrzDLBfyFX1NgEoFtWkQSwPORMejePymwmAIOgijCsPymbkhgkdMwTRjUPzcggRMEIEAvwLFekM/maP+XADAtG5ZmoVgsQZ0ZQ9DIoDFcg2/JNNTqNkCSsTT1Eef2hjEdaUVvb29fIpEYWFNA7EZJQ3J0CG11m3D2wkWoqoodMzMYeh+D5pbh/JkuiAKPdDqNeDx+O5lM1q4rIZccR7O8hI6jJ9F36z5+fU+gLhKBoige6N7dO0il0oBYiRvXr4IIeviVgTGoBHU+iSqaRrbKW06g+CWO7u5uhEIhFAoFZLNZPHg4gB8F2YsJBoPSag/YwHC8jFQ64znzo6+8B+7v74cs+wBWUoADtm22NJZjdnZWWy0BgQDaDx/D5IdBDL8ZRtfxA5D4DmQyGUxMTECSJESjUYTDYRRLGheLxfKpVGqOY+BDlx49JnOZjS+MPCoLn1EXriaQSBMAjOeqYLkidoV+kgLXMXStROAsKer8DdsBr2sFe8jtAAAAAElFTkSuQmCC', 65.16, true, '2019-09-17 05:44:07', '2019-09-18 07:15:13', '2020-02-05 08:14:24');
insert into users (PK_id, username, password, firstname, lastname, address, image, balance, isAdmin, createdAt, modifiedAt, deletedAt) values (3, 'rlindell2', 'NeVijf2', 'Randi', 'Lindell', '42273 Stoughton Center', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKbSURBVDjLpZNLbIxRGIaf/9Lfpa1rZ0hNtYpEVRBiNkUiEZFoKjSxQmLBglizIQiJILEgWHQlgkpLGg1BU6E3imorLkElLmOmM1XV6mXm/8/5LH5GI7EQJ/lyztm85/ne872GiPA/ywa40NC3X4TtSnRIa43S4GmNUhqlBU/pUaVQnqC0fnRk89ywDaBFdpeHs3P+5eUDlW8XpgmU1jkAmdU7QQSUB1qB6/rnVBLcFCRdcF0G99bjumrMKIFfPgjkBkB7fon3UyQJbhK8FLyIAuB66rcHSumfAgZ8ToBon0Rrn0T5+6AzyKd5eVi3j7HDuUfnmchWRITTN6PyfdiViw3dIiJS2RgTEZErzf69qiUmg59rJFq/R/o6a0UGIvK1s0paTqyN2QCu0mgRbAuqW+JYlsnVB3FsC2pa4yQSuxjKLmbK3BJ6u17iGCmyJ0wna+rMiekWBLAtgw3hADWtCdaHg9Q+jrO64BVDmUVMKlxDMnoZZ7zB+/ZX9A+ZGMmeRWkC0WCbUPskQWvPcR7eEEI6xvDYPCbNKWMkcg7T8cjIysfxPnC+dwun95a/Nn1HNSJChgWlSwMAFNtDbJw+g4lzyhiJnMXMcEn1F9B9vwNZto/vTggAE/ypA7BMg7qOHnL6PrBYxhEsXkcqVoHlCMn+fLob2mDVSQIFC9M/ZwN4nud7YMKMgTpyJ8/GkyDRZ6eYHHQY6c2jp/Ul5qqTBHLz0VqwTH4TuMpvYcHMLDoaK5i/fAdjPjbyuqaJ9lu9PL/zFKPkEMFQoU9qGWgtetQgSdPBS28Wp5TOzO1KkHpey6xFK/iW+EJX2xvuhg7z5do34GE6C30DI9cBjD/jvGnllOElhdPscNEsJCPZPBxPbCs92vnub6H6AWmNdrgLt0FkAAAAAElFTkSuQmCC', 47.07, false, '2019-08-26 21:28:22', '2019-09-29 10:22:24', '2019-10-22 03:25:51');
insert into users (PK_id, username, password, firstname, lastname, address, image, balance, isAdmin, createdAt, modifiedAt, deletedAt) values (4, 'rflexman3', 'V12JMj', 'Raimund', 'Flexman', '28094 Scoville Way', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIMSURBVBgZpcHNi05xGMfhz/07hzTDiKZmEmLYeM3iKTKUiFhY2EhZ2NjIBgsWYoUoSWr+B7NhY6GkJBRhYSMvJYRSFDPPi3N+9/01Z2Jvcl0mif9h+46PH92yrXXpe0f9EhCBIvBwFCIUyJ2QkDsewcDsuv3y5adTN67sHytbo61rs+b0p6E5zER/u+PXgLGyUyt1vk8yU91aiSmlXJw/uJKZOnzxPY1SChpVdgQohAcEIkJ4BJ6FZ+EKKhfLh+fh4TRKJBqWDJNQMmTCwkjJMEuYOVaIIhJlFo3ITiN5OI0EmBmWjCIZqTAsQZFgVlFw/tZuTt/cjIqaRnjQSAoxzYxGApIZKRlFYRQGKcGvXLF4cBXHxjdS5R4RTqOMcP4yM6ZJnLy+DSlTRabKmUULVrJqeCMTvTZ7x0ZYoKs0ylzXTDPDAEmYGTkqdq45hCvwcALx+cdH1i0eZbLq8qx7iPXnDswv5UGjAMQUM5Do5QpX8P7bG+rI5Kipvebnrwk2LNnKZN3h8bsH38qI4C8DjClm9HKP7JmhgaXkcFzBlx8fWDh3mOcfH/L47Qs6Tsv2HR8fH1qyaH+4Ex64OxHBz8Ej9KqKKip6uWLF4Go2jezi6YdH3H/1hGXdE7fvXD6zxyTxL9aeS+3W0u19917f/VQFOz5f0CummCT+xchZa3sUfd3wka8X9I4/fgON+TR7PCxMcAAAAABJRU5ErkJggg==', 28.29, false, '2019-02-25 09:22:20', '2019-11-03 08:25:27', '2020-01-02 07:43:09');
insert into users (PK_id, username, password, firstname, lastname, address, image, balance, isAdmin, createdAt, modifiedAt, deletedAt) values (5, 'bmelling4', '85GQCWcBy3', 'Bethena', 'Melling', '84136 Lawn Drive', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADPSURBVDjL3VIxCsJAEJyTaCmIYCFWfsMuviNPsAzkB/7E3gdob2+V1kKwsUmOmLu9c+8wASVocalcWGY5dmdmlxPWWoTEAIERNUWWZa0VIkJd16iqClJKn2VZ+iyKwmOe58L1itAVWgdpmtpfqs37q6dnB0mS2C+qxKonxtUnQT8OtvvbYTEdxl0NRBbaGChGpcBIeGjgfJHH3Wa59gRueDZut4ExFmTcIKcWjG4Q0JHhOvKk88kofrvB9a46lRRPKybQ2nii3m8Q/JX/gOAJ1Om5dnjvy8QAAAAASUVORK5CYII=', 90.9, false, '2019-03-01 07:57:19', '2020-01-29 00:30:26', '2019-12-28 07:13:57');
insert into users (PK_id, username, password, firstname, lastname, address, image, balance, isAdmin, createdAt, modifiedAt, deletedAt) values (6, 'jrosso5', 'PXvPWNZj6', 'Janith', 'Rosso', '0151 Boyd Road', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAALoSURBVDjLhZP9S1NRHIf3D+gfURRhL2gFFQYFiSkEIkZFI8kfyjIUNNFCrOnMFxKJMTfCKNEl5AxUVCx1bm6NdMMkx97ci5vTjTn3otvdy9326dxbiinRhQ/n3MN9nnO+557DAcDZH7VanSuXy4VTU1OL4+Pjm8PDw4HBwUGTRCIRf+wXXz34/V5Ho9FkEFhE4ITT6YTf78f29jYikQhCoRAMBj3E3a/otyJ+v1DQnvmX4A88abVakU6nEY1GwUgcDgd8Ph+SySTSSQo0ZYJ8egCvO+qV7W2NmXsCZmYGTiQSYB6apsG8WywWBINBVhqnNhAL65GKreDrRC+aX1b2swICXyTLToXDYRbY2dlhJevr6zAajWDGk0kakZAR8bCBXUWCpKb6Uar26ZNcDoFFa2trYGIymViIqZkRud1uth+PhYhAR0An6W+RFcahVCpRXl4u4mRnZ+N/qbh/BZMfShDZ9rLiQCAAm82GsrKyJVag0+lgNpuhUqnQ19fHQkzrcrlgNi5DN/EAWytS2Ba6Ybfbsbq6Co/HAy6X62MFDLwLMRImzBiTlppr2DRIQIct0I/chVY7i3mdBUbHBopLbm0dEjDZhc3LKmgGihDbHENsowt+6zgWx+qh0jvwRWtEQdFN/aESdkU5OUQ8y4fPNITYWjPm2s8hsTWEH+/zMK8exTvpBApuFPX8cxNLiy/APtOAuLcPlKUMc205iDrqEbRNYKH3NvILC1N5+dcvsQdJIBCIFAoFPEHyK9d/Qm/XYklaigDZuOhqLSLmO7+zco+U8gYOhQDC6lzt3kns7OzM4Lc2T38alcDmNUD3TQjHXBfiHjE7e2SFS0o4y7aUrQKUewQK/mmvoulk1t5l4vF4Gc8a6noeVz2k1d15oHxWxP0ziHnJHnil+/IZ9I4Oru8SyBqOSzkHr2dVVeVlRcf5qKI1JyVvyU7Lms6kZbxTKdmLLFrWeCJGIGrm+TFqpv4oNV13RPkLngD4bMIOcuMAAAAASUVORK5CYII=', 58.43, false, '2019-07-24 13:21:10', '2019-12-07 07:59:11', '2019-03-02 17:33:41');
insert into users (PK_id, username, password, firstname, lastname, address, image, balance, isAdmin, createdAt, modifiedAt, deletedAt) values (7, 'badicot6', 'YJ7iFhPL4', 'Bobby', 'Adicot', '6 Summer Ridge Place', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAALVSURBVDjLfZJNaFxlGIWf7853597JTJzQzDi9U6chPxCaOmi78idIlglEsEJJqFmIaDddlEK7EF3pKmJcdiUU3bmwXQiiGxsQQdBaF9HYUFucZP6SNHWS3uncO9+PiyDNGPDs3s3DOec94rOvflkyRrzRjo0fKQ7L9p6uBN/l2tuvnb4EII0VC7Mvl/K+7wshEvyfOga06nL9u7tvAvuAdmQ8z/PF58sP6fCAPXGH4cImKbeFsoad3RS/3c3SZ08gGeTy2WG0EeJfqIwVOE4CR0DsNBjObeIl/6IV7aG0QnoeI6WA+/fSDCRzGNPrSgIYwHEEkd0i6T7k78ctIt0hVgplQzzPI4xSDKYcLLanmCcAIbDKEOkuHRXxuBsRG4U2gOhijUY6Amt7W3UArN0H+OTZC9NYXCKjiZRGiCThXoZMskgi4YAVhyNYux8hkzhGtdEk69fpSzt4QvFop49mIyDIjiAdcegz0h4EuE9TTJ1i4/ZNBjI1hNa4nTzHRxfI+AHamP/O4omDOw/Oo42hG2uOtCTSfYVc4Rk27q9S2/wAIyMGt07xze5JdpvbT1248OmVlZWVq9JaiwCmT19CK833X/9A//EB5ubmCMOQjdFRfvw5R8JxmD33Kq50qdfr4tYt76NKpZKX4sBeV3/9g/ZWm9lzr/PJ1SV+v71KcDSgXC5Tr9dZ+niJRqPBsZGTauHsrKxUKucd3xUtTJfnCi/waD2ivz8LwOWL79GNFfPz80xOTjIzM8P09DRxHPPsS2dCgGw268mkFF98efPei7G25T83VTLRrvuAePf9D5VSSi4uLpJOpwEIwxCtDT99e631/FsL2Wq12hEHhzExMXFibGzs+tTU1NFSqWSklEeazSZra2t4nsfQ0BCFQgGl1M76+rqzvLzc6AGMj48ngiAop9PpG8ViMe95XgJwtre33wGquVzuBuBGUaRrtdpWGIZn/gGotkJJF2DBHwAAAABJRU5ErkJggg==', 65.37, false, '2020-01-09 09:59:57', '2019-04-21 06:41:30', '2019-11-11 23:06:42');
insert into users (PK_id, username, password, firstname, lastname, address, image, balance, isAdmin, createdAt, modifiedAt, deletedAt) values (8, 'seagleton7', '17ufP2A5F0lx', 'Suzanne', 'Eagleton', '986 Spenser Circle', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAALRSURBVDjLfVNbSJNhGP6li24C6UIiDHWah83UdJv7d7R5oiiPWGp5yIUHNNF0acNjrmWe5gx1Ujmd+pubW8upaSWZRilZdhN00WVBEAgalSK5p18hczm7eOF7eb/neZ/n/d6PAEDsFWETAoSN8aWbZ/EDXqLIFLr67x27RPpU6LUzl1hJiC08a7461lNo4GYLBjnf/ktwYrPjOF/+JxeZeWtCY+hSTk00FX9TsCroZttSrggb9iSQjJLL4hFybUuyiVwRDHHBpzjg6zmoni3B7CcLrjVnIiY75KpDAtqnVDQcComBXD5tioL5vQ6THwagfqFCQKsfMifiYJzXIrjlGI43+CNQxaK7jpArtM9t77RPWxjFx9CiBjcey1BhTUGZ9SIkWhIBDUzIBpIRGe/zcVsB7XOd9gnBfW6fhCK/njPHg3rTgtrxDJSa45DeH45UYwIMC3fQMa1GoJIJYaq3xc4Cf4AzR+rZIHVsmN61o3osDSWmWKTrpUgejEXXjAoF/SSye4IRVMMCS+HznVnmXWg3A/Ieey3VkIjmqUIUG2OQ1hOOpL4z6JxWorA/AZd6QnB99DySu/zAlB+1+RZ7weey598B8jpCfuSZZWh7Vo703kgUUWIaSCKXVpbVHYRKy1kYF9ogH45DVOuhDXHTwQNeOR6V2wSC29z6kOYA1I2XI0kXQ0vm4eFiJ8xv27eAQwsaaKbk0M81Io+KBrd+/0aA0snZ7hnZ9UEzXnIPdD9vpyUHb4HVT4rQMFkA1aMc1I5m0Q1y0TWrRFqvCN41xK9d+x9YwZoJVLBw4S4ThtcaUPMt0L9qhO7lLZokH9rZOuRQJ8GoIpZcFcRxh5/Iv9RHn6Bl4FSbKyJaXSBsckZGjwCdM7WQ9UfDvYr4clhBsHZtoqPwzHKfY2S4wV/p9DNTHwH3CuIzDfZ1uMp7hXvqkXVGpZPNrWIfXBQEY2ftN8xTb5GsXWfEAAAAAElFTkSuQmCC', 48.14, true, '2019-05-02 06:06:58', '2019-10-05 23:39:00', '2019-12-25 15:46:36');
insert into users (PK_id, username, password, firstname, lastname, address, image, balance, isAdmin, createdAt, modifiedAt, deletedAt) values (9, 'jradenhurst8', 'DdMACn', 'Jimmy', 'Radenhurst', '77655 Alpine Hill', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAD0SURBVCjPfdExSwJxHMbx/yTc1NrUy+h1+AZ6GUJBaYdiKVwopjmYOASiINJgEVFwUFHo4BIiDtql/SPU5BDUQb8Nomh3J8/we4bP8MBPIOYpexdtPcvyyrO6ETxR5zGwAeiMeOfmxBE8MOKXKsWwA7hjSJceZbJhW1DC5BvJDy+kNRtwzYA2BgYSnUTEAgr0+aBJC0mbe85i/0AOkw4Gn8SH0Yo2CRGMrYEralyOq/SJzrRtBEJVvMoKyJCSyd3zZh2dUMZmZOotuYOIuAuYBKbqlgVcKPN7KhvccnRsAYv49/I0ODA9Lgfgcx1+7Vc8y8/+AURAMO9/VDEvAAAAAElFTkSuQmCC', 54.05, true, '2019-07-08 14:51:44', '2020-01-04 08:08:01', '2019-09-22 17:46:22');
insert into users (PK_id, username, password, firstname, lastname, address, image, balance, isAdmin, createdAt, modifiedAt, deletedAt) values (10, 'fsecombe9', 'me56jCrIB4', 'Freemon', 'Secombe', '2 Bartelt Parkway', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJ+SURBVBgZBcExbFRlAADg7//fu7teC3elQEoMgeDkYDQ6oMQQTYyGxMHZuDA6Ypw0cWI20cHJUdl0cJLIiomR6OACGhUCpqGWtlzbu/b97/3v9/tCKQVc/e7RRXz+7OrSpUXbW7S9tu8ddv0M+3iCjF1s42v8WAP0XffKi2eOXfro9dMAYJ766SL1092jfDa17DfZgycHfvh7/hau1QB9161PhgE8epoNQlAHqprRIDo3iqoYDSpeOjv2zHRl7atfNj6LALltJys1Xc9+CmYtTxtmR8yO2D7kv4MMPr7x0KULK54/NThdA+S2XTs+jOYN86MsxqBGVRErKkEV6BHynp//2fXbw9lGDZBTWp+OK7PDzqIpYiyqSMxBFakUVYVS2dxrfHHrrz1crQG6lM6vTwZmR0UHhSoHsSBTKeoS9YU8yLrUXfj+w9d2IkBOzfkz05F5KkKkCkFERACEQil0TSOnJkMNV67fHNdVHI4GUcpZVFAUZAEExEibs4P5osMeROiadHoUiIEeCgFREAoRBOMB2weNrkmbNz+9UiBCTs1yrVdHqhgIkRL0EOj7QGG5jrZ2D+XUbADEy9dunOpSun7xuXMe7xUPNrOd/WyeyKUIoRgOGS8xWWZ7b6FLaROgzim9iXd+vXvf7mHtoCnaXDRtkLpel3t9KdamUx+8fcbj7YWc0hZAndv25XffeGH8yfuvAoBcaHOROhS+vLlhecD+wUJu222AOrft/cdPZr65ddfqsbHVyZLVlZHpysjx5aHRMBrV0XuX141qtnb25bb9F6Duu+7b23funb195955nMRJnMAJTJeGg8HS0sBkZWx1suz3Px79iZ8A/gd7ijssEaZF9QAAAABJRU5ErkJggg==', 7.47, false, '2019-07-10 04:38:27', '2019-05-21 12:04:56', '2019-12-06 02:20:08');
-- -----------------------------------------------------
-- INSERT mentors 
-- -----------------------------------------------------
insert into mentors (PK_id, firstname, lastname, contactNumber, email, image, createdAt, modifiedAt, deletedAt) values (1, 'Lanny', 'Drinnan', 1, 'ldrinnan0@fastcompany.com', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAEgSURBVDjLrZMxSkQxEIa/J2IhVmKlF/EWVl7FW3iNrS28wna29iKIKNgIvrfZzPy/Rd7uIu5jFzQQApnMN18mpLPNX8bhrgO396+bCoaUubk67/YGAFycHiEbCZ7fFz9iB/sAVslKU9Pbr3D30NuAEgzYcH153EWaDJBMpqhlApCCs5MDBFjw9ikAIkSkUJqQKDW2A2xIt1WGaPnUKiJa9UxPG0SCtAFktv1l9doi05TI7QCNBtJPg2VNaogcLWrdYdAARtkItSRRW/WQqXXCINQaKbsZjOdKNXWsLkFMPWOmSHWbHnjVxGzJ2cCSJgwMLx9Jji+y+iKxNI9PX78SV6P7l880m81cSmEYBhaLBX3f/5rDMKzjpRTm83n3LwbfdX8jZ1EmeqAAAAAASUVORK5CYII=', '2019-06-20 10:13:31', '2019-12-15 11:31:16', '2019-12-03 07:27:15');
insert into mentors (PK_id, firstname, lastname, contactNumber, email, image, createdAt, modifiedAt, deletedAt) values (2, 'Godfree', 'McKimmey', 2, 'gmckimmey1@nyu.edu', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHTSURBVDjLY/j//z8DJRhMxE8+VwjEE4D4DBD/j5989l/8pLP/4yec+Z8IpJMnn/2fOPH0//i+0+9TJp063b3uxv/uNdf/Ixsw+z+JoHPVNRQDboB4c468hePZeDAIJPYc+4EwYOLZDzADDt76AsaHbmPHMAMiWg7+gxsQN+HML5DgbAI2I7sgvHEfwguxfaf/kRoGoXV7EAbE9JwCC4bU7/nvU7vrv1fNLjD/3tMPWDFYbc1OhAHRXSfAgg5l2/83L7v436J4G9yA1++/oWCYAcEV2xAGRHUcBwsa5235X7f0wn+9nE0oBjx78/X/moMr/5fMTPif1uf/P7HL679veRGSC9qOgjXopK//X7rg7H/tlLUoBizfu/h/4+rE/1uuTP1/4dmu//27s/6H9+v+N8+W7IG4oO0I3ICS+ZgGgGzceHnC/43XJoHFe/ak/u/fkw4y4DvYgIimQ/9gBoA0IxsAwj7Vhv+3XZmDEgubLk0FGQDJEKENB+7hizL7QqX/3buS/rfvSgDz23cmoLogtHZff0jN3q3B1XveBVfu+h9Yvu1/YNmW/wElG//7F63975wX/z+4R+N/764UsM0gGsSHhwExGKi4A4g/gZwNpTtA4gDRWxxGCQ/RXQAAAABJRU5ErkJggg==', '2019-12-28 22:46:25', '2019-04-07 12:51:57', '2019-08-20 22:44:27');
insert into mentors (PK_id, firstname, lastname, contactNumber, email, image, createdAt, modifiedAt, deletedAt) values (3, 'Lazare', 'Killner', 3, 'lkillner2@t-online.de', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKZSURBVDjLjY/Na1R3GIWf+zV3JolJ40iMptZMGrGtVLoQpK66qagLV0VwI3XThS4EQQQ31Y34F7gQFOqqdCMtdGGCRXAhQmskgVSNmYAOmkySmczHvTP33t/vfbsUkyl44OwOD+dxVJXNSWfHJ1X0oSqjKuKpkKjoPMK1/m8rv38wVtUt7cx81sre3VWN51Tbj1Ub05qt3NXmo92vNm99ekStDPifHILuWzB1yOq44TbEyOebtz0BYkGSCk62AdrFdpYgcbFmq+7/PFBs8x+ylftI0kbiBkHxMGLgIx8IbjhGMPglmiwjQR+igjWyFZDM7B1U0XkVGVXBU6uJow6m9S+mNod229i4RWHiYG8FsXLFH7k0Fuw8CdoFG4VZtEj84hqFHUfQ/DJeWAc12IxeAL3sjxwH0wTbBNvGL4yQRet47jzaaWGjFoEzgs16KFgDSISaNmiKJKuQdjBGyA1NovkqNqyxOrtB5S/D4u1ArKcV4ObRKXPDFyPYaAG78RRJV9DkDd7gBDZVktpzNI5Ye9Ygqo1x6MzPhKUDTmd2as/8o+nrT84WJlybKU5QxCuU8Pu/wB/4BtRiMiUc3kdu+y7e/F1l8rtT5Bcf4vxymr7yPcb3Fp24Zn70rREc1yWLF9DuOzRdIRw7gUnvkUVr2HoVUxfyoyU4cfG9+9VdSJvAtxm/ddZmTuW3fYUEw6DjxOtlvHA7tm83+Z0H8IZeEj/7k/4/zpF0lomBVtNDC07Hu/BD4VM3N3jMzQ/g+A5ZWqO1+pJWZeFB4/Xz+vqLpzt8vy+qvqqGbuCSeRGNdaW87OEPuVNO+ddiSQw/iZXvreVrMcyJ1Wmx3Dp4vr4EsHR7uFSby9/ZKK8dISKnBdKg6D0e2J87+x98zpgrhVsXPQAAAABJRU5ErkJggg==', '2020-01-28 08:36:24', '2019-07-01 23:01:00', '2019-09-30 08:25:50');
insert into mentors (PK_id, firstname, lastname, contactNumber, email, image, createdAt, modifiedAt, deletedAt) values (4, 'Colene', 'Rouby', 4, 'crouby3@mlb.com', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIMSURBVDjLpVNLaxNRFP6STmaKdFqrgYKWlGLSgoiKCwsKVnFRtBsVUSTNyj/gxv4Bl678AyKCoCulgmtd+W7romgzKT4QMW1G+5hMpnPnnuuZm6ZNawoVBw7n3pn5vvP4zkkopfA/j9F8cafO3FekCjGpIgKIvayftTXOkr71jkz2/UXA4HxXfz72gIx/lBsWSfiVtwiWHK8B3kRQeX/6lmnnkuDAwn0MJSKQEFChQCp9CcHixxgsGWw3B01uRKfx9t1HIP1POpoSdUulLyD0vqO26IAkDW7tgSZYeHPqcmpXxkTChKzOaAKSEdo6jnEWVY5ehFxdHs2cn55rScDR73H6DKyyRWs1R0haGdR+z8YZ3MyMTj9rpUKi/PLkUJuZfmX3nkNYmQBxzYprpyCA2XMRrvNAcdfDhgKkm6ttKTdW6jH4w4RpD/ALAaNzhH2kSwALoSJCd9+VhIqEVVeD4C1MclaOT0Ke0Cowq+X9eLHapLH23f1XreDzI27LfqT2HIfvzsRAyLB2N1coXV8vodUkfn16+HnnvrPDhrmXsxBY+fmOwcVlJh/IFebK207iuqSShg0rjer8B9TcWY7q38nmnRstm7g1gy9PDk2129mjinjy3OIvJjvI4PJ2u7CJgMEdUMmVuA9ShLez14rj/7RMDHzNAzTP/gCDvR2to968NSs9HBxqvu/E/gBCSoxk53STJQAAAABJRU5ErkJggg==', '2019-12-21 17:34:41', '2019-05-22 19:11:58', '2019-12-17 11:49:54');
insert into mentors (PK_id, firstname, lastname, contactNumber, email, image, createdAt, modifiedAt, deletedAt) values (5, 'Amandy', 'Cheevers', 5, 'acheevers4@princeton.edu', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJ3SURBVDjLpVJdSJNhFD7bp7Js6qaUDqmxEFfRQFmZ00wjXYK2kgX+h114Ed5siZf9aIWFUSLRReVFgUElGVkURDCkKI3NlKIaRYmWmJmSue3b+9f5DGuBBNoHLwfe7zw/57yPSggB//NFLQcUKHG4BCEuESbt6uUQCDncqNm3x4gEbtVSR5jbuStGEPoaHSRibV7yCDxMWhH8HsHpCd6n7J8E9mPDLsGZmzN27tHJze2z23aUIbAcCTITfM+Y0qMiDQd7gNJSQdnd6MudZZEEhYd9Y5VbpFRZ9kJmlG/OdOGNdC0+58wNg03ijFZxTnGJhJZKjt1RuBCHXFmV9QfszccmbXf/9Lfc2MeTZkvBytFiw/h1Q/Z6xkhTuS3eyCh1qeQDdT0Kya/FUC3am7yjt769aCjMp4Lv7yzoyNeZHM26Ndnw7mHTjODcXnO/NpdzdggFzv71CkVHBmNKxp/cy5sY3Jo2MxKiejY7VZGwzlhUD0D8EAia4VP/+V7BuNNS84AoGHXEvCmMUc/tJOsXt7kuGdddPJsZbUqy1gKEfDBwtQu0uiDQULgUj2MBp7YfHXLhvONo5yWnpMdzylbd15YXHG3QrobtWao4fQC4/AHTw4H9eA6mgkYVleXjAx22uHkCVHXtzYhGdcI8p3PalMuhK/YYVDmhW5sBPDCM2CBYnWY09Rk0Gj8kWyo2UDnsnifgjLTf7P8+guqtC7aYHK5PTCuxxsZ9BUGn8LEl8N7yKzECHvLDqnQj9pCGvpZNGxeNMtobs1R3pUrqj0gwraQ/4q8apBVmmHj1Avy9Ld2LJhHtaXyXnEHBBdrnEUf8rqBUIVJ+AugPahHelS39AAAAAElFTkSuQmCC', '2020-02-06 00:43:36', '2019-09-03 08:28:38', '2019-05-24 02:34:30');
insert into mentors (PK_id, firstname, lastname, contactNumber, email, image, createdAt, modifiedAt, deletedAt) values (6, 'Wyndham', 'Aaronson', 6, 'waaronson5@freewebs.com', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIpSURBVDjLpZP7T1JhGMfPn9RaznVZa7Zhl1WoOI1ZtNlmq5Wrma1jMTSG5li1ahWSFJKmjuhEYzVJCDGQUNJI7WYX7ALnhFwiKFvn2zkHKw6d33y27y/v830+++5535cAQCxHhN7+AR23I9Ba30EzMIeTva9BWl4+ljJbRhLqHk9i/trDOLpdDLoeMCAyuZ8oVtP1WVYKYPYsfCv2Eqd9bdB61dB4SJxwNQuHjcZnkAKY3F+Efu/0VZjDV9A9eVFoiIo37L88JQkwDjNCv7CIPm8MheINey+ERIC6/kpFtXkbdhjKUdtVIfITVn9URGRSOajOBv8ClH1yRZVpK9s63IL2kVbIz20RBvkaGI3mAVQgBmosCsd4FG8+p7Gzc0wA1Fi2KyqMm1nyfhNqjHKsP1WKct1GDPpisPLy0/8nePUxhWqdD1xkJReZbXY0oqxjLbtOU7JJf2ceqewibAFa8FKBJYCQgktg49Rg3QMuMupv1uGw/QA26Faza9SrZHyidtt7JDOLsAdp3B3Pixh6QiOd/bdZVY8SGjeJg1QDH5ktbVkp+7OPtsG3SHz9gXuhfALnJPeQHBM0ClVrqOIjg4uMkuMrZIW3oe6fEwBD3KBzScQtPy3awfNIEiq9T/IdkDdeYIEDuJ4ygtcd5gD8QLF2dT76JQU4ap5FPP0ddDKHT/EsInQGRKXWi2KVHXNSUoAjppnRQ4ZwZt+lKdSfD2H3meDyvjKv3+cfGcwF4FggAAAAAElFTkSuQmCC', '2019-08-29 00:57:49', '2019-11-05 13:04:21', '2019-11-12 21:21:51');
insert into mentors (PK_id, firstname, lastname, contactNumber, email, image, createdAt, modifiedAt, deletedAt) values (7, 'Selby', 'Jansie', 7, 'sjansie6@discovery.com', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIdSURBVDjLpZNPSFRxEMc/b9/P9b9uu5qWVG6soERCIringi6J5wKhTl2CvFQnQQi9dO3QoUudOkSHLkUQniL/ENWyrSRbUqC9dc1dXUXxubvvzft1sF1d6iA4MMwMzHznO8OMobXmKOLjiHJkALWysjIKXAXwPA+tNZ7nVaiI/GP/6jMjnU67LS0tJsDBfZT8/9mSn0gkRGmtzaqqKh7GhKK9zcb392hnB19rD8fD5xlq+EEkEmF0chM7X8TeLWLnHV7cjCAipjrYte/bI9z0J5Ysi8WsjR6bA8BxHCYu1eJ51eWxAEQEVQoAnLYLXOzt4LJfMfP5KwnTLCdOTU1VLG9wcHAPQET2il2Pj5mTKLVBQ1MTseIAuVweGvYAotFouXuJdQWDTWuB1vBZXqo+cr+K1AT9tO8slxPjTybwxV8jqxYSCNJ17S7S3LvPoKN+i/n1AF4wwLE2P/mtPI1eFqgl+/YprWsxum6MUB0+x+7cJMmZ5xhtSxjJZFJ3d3dXzGfbNoVCoUx5/k6Ugesj1P58B8vT0BxgTZ0h9mFhn4FlWWQyGUKhEJ2dndTV1ZUBZdWipj0MQ/f2L3D8BGpzHSUiyXg83iMi9Pf3G7OzszqVSlVeX3MQ+8sb6l/dprD7GxvY3jJxGwOucZhvnB7uGPc31o+dCrlK+VJsZ10WV01x8vq+cdh3nhk+PbqTS98yxTgjpl7W8PjKpPvgD7bjUD5Jjh8/AAAAAElFTkSuQmCC', '2019-11-13 13:14:33', '2019-12-07 03:50:35', '2019-10-16 12:07:03');
insert into mentors (PK_id, firstname, lastname, contactNumber, email, image, createdAt, modifiedAt, deletedAt) values (8, 'Adrianne', 'Wingham', 8, 'awingham7@uiuc.edu', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIySURBVDjLpdNdTFJhGAdw120X3XXR5kU33fQxS0+5Yl24lFnQKsvl2nJLM0fmXLNASceKgAv8yBGgJPEhkcIShEEYKuKU1IxcTm0WUDiJ1Fpbm1tZ/855186oLS/k4r/34n2e355z9rwZADLSyX8vCm+WU6fqT38+21S4ztPy1rmK4lXF5Ry//Hwm6LjoHN8QOGOgUOe9iGByCJ7FJ5BMX0ORiosfa1/wTHqQIAQ4VCHbwpXL53iWHPAe7QefJAvq4G2MLY9gcnUcQ0kf/AkvAm4DPvhl6Lq+jwEuESD7inLrCWXJ10BygC56SgpHlofxfGUMjvhjDH7sR1e0Hfq3VmiqKSwOt6CldCcD7CDA3qrOXfRo37tjRojC5SRt81KYIxp4lxx0+mCOaqEON8NeR2Ght5ppBvsTT9Yqai60F/y0vTehPlyBW+FKAliiOvQnPGQKY+Q+TOOdCCjzEPU2/A1wxIaH3a8N0C20ouGVAI3TVVC9kcEa0yO0MgrfkptM0mprwqypGKG2AgaYYYEsqfGFI94D4csy1E6VonlWgt64Fb6EG7aYGTdGK1ETEv6yu+wEcDQeZoA7LHBEJfxkiejQQxczccZtEE8JwHNRKLMK1rRzng6R3xU8kLkdM/oidAh2M8BRFsi7W/Iu38wBty8bXCcdSy6OyfjfUneCbjj34OoeMkHq92+4SP8A95wSTlrA/ISGnxZAmgeV+ewKbwqwi3MZQLQZQP3nFTLnttS73y9CuFIqo/imAAAAAElFTkSuQmCC', '2019-08-12 22:12:49', '2019-08-04 14:57:27', '2020-01-08 12:19:29');
insert into mentors (PK_id, firstname, lastname, contactNumber, email, image, createdAt, modifiedAt, deletedAt) values (9, 'Archie', 'Hayhow', 9, 'ahayhow8@tuttocitta.it', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJMSURBVDjLpVPPaxNBGH3ZJCQkVSEkJJja0MSCabHoSUEKerAgggRy0ZP9F4QeDAhepKeeBA8GPFQCEutdCMRSSYkimouIVqUkNW1iyJZN0mR/zvrNwtbUGjw48JiZb773vvfN7jhM08T/DNfwplgsekjwBuEWY+wMzVMEWrKPNH+j+QmhmEqlDJvjsB0QeZrWz0Kh0GwkEoHf74fP5wM/lyQJ3W4XtVoNrVarRLGb6XS6bhF5AkehUFirVqu8nDlqaJpmVioVM5/Pr9g8wbZCm5lwOPzPnqPRKKjItSN3QEFLsdlswuv1wuPxwO12W7F+vw9RFFGv15FIJKzckQIulwt7e3uQZdna67qOTqcDRVGsMx77q4Ddk9PptBzwZA7q2xJZrWYw0PaRmXwBwzj4CL/vwHbAkzmJgydy8JiiqxgPJpF5eR0aUxwjW7AJD98swGQaVKZDpf3JwBSSkQvoyvtY/XE/+HT57tjrRbF3RMCurjMVV2duwzAZDGaATrEjbePs+CX01AHe19al2QdC4JAAB6/OIZNlTq62v5JlipEbzdDQUbo4d2oOPa0vvN0qtQ8EuHX+qehPRKPRIAEZuqEjfHyCyIYltivVEBiL4MP2Bja+l1qqjvlhBwvlcvl5Mpn0x2IxDHQFK+VlugPVchMPTuNifB7vqiWsbRbbso7LO0vmJ8fwa8zlcpMkdI+QFgThBH8LvB3u7LF4xzw/MedY33y1qzDzCpG/HHpMf45sNnuMyKcJjC718yNpUTSY0zdgRvznkrll5/0CZpfQA8IRXj8AAAAASUVORK5CYII=', '2020-01-31 01:54:09', '2019-07-08 02:46:33', '2019-05-22 02:36:19');
insert into mentors (PK_id, firstname, lastname, contactNumber, email, image, createdAt, modifiedAt, deletedAt) values (10, 'Allsun', 'Mountford', 10, 'amountford9@intel.com', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHtSURBVDjLpZNfSFNhGMYXZ110E1QYFRR6Yd0kIhgE3ozlIII1oQvzwjAUtD8jSFILoigISqrVCAy2u6y5aAQpuJH9cUabK06GSEOnHCcrzAzsrJ3Z3K9zDjGMhhzog+fm4f093/e97/eZANP/yHDhXKhyw6TfMT/qrluJuA7z0Vs5M/1g33bDAXFfHZ96T5KRoijJGKP3rAxfqQoY3R15/BGK9IZs8p2uzOQQUZdjxRCsfAujzPpIBE4ji726tICY+0jGEJyTn5GOt/FDPMMHbwOJx+1E7zhYs4l/wVNNyBO1LC/eItXfSPx2DWtOwShcNOCla49h+J+A4PXdLKVeFIXFcxW8sm+UnlvN+aBNmFXVVQjoaS4t1WAp9pRIX2tReKx9fzYzcJP8xCBp31lipypyoQNmpx7gdu5iSYqw/HmMgYvVhO9XMTXiRPQc4u3VaoK1QuKnCnPXDp2b4FoZX29YNH+6EJBVX9eC/zhfeiwMt5bgb9pKf8de/c7asfPvA6xe3y9tQ/P1gAbrZsY9jaTFhyyIfbzuPliA9d7YhKTsbQYVUjpMLKqS2gRUf04vOGrZwjFrCZ4T5ToY6q55srq54fodl6Mt5b9SnWXMn19PomUdQ3ZzbtAmXDD8mUbqd3apO878mUJSgzX/N4hT3VGIyxnZAAAAAElFTkSuQmCC', '2019-11-27 11:25:57', '2019-03-28 15:34:57', '2019-05-07 21:02:27');
-- -----------------------------------------------------
-- INSERT books 
-- -----------------------------------------------------
insert into books (PK_id, name, author, synopsis, image, createdAt, modifiedAt, deletedAt, published_date) values (1, 'Rigney', 'Hamlen Sames', 'Nondisplaced comminuted fracture of shaft of ulna, unspecified arm, subsequent encounter for open fracture type I or II with delayed healing', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAI4SURBVDjLpVPNaxNBFP9tNx9N0zW7ENRFE6ygpYaCNXrQkwWtFUUEc6hgD/0TxKsS1KOX4LlQEAUPfoAKpScDAcHUhI1VCr2kF4VWsRBCk93szPhmYqJt0pMLj8fMzu/jvTejCSHwP19g90a5XF7wff9Wq9UKUAZlFZ7nyfw8k8nc7EtQqVRiBJg3TTMTDg/BJRA4h/JHLi3LxNLS4gytegkcx5mW4Hg8figajaK6/g1SnXMBLjgRCTAic123fwl0eDGZTKqNRqOJFoFXKiUi4N24OHVZldGXQNYYCoVQq9XayozhROqkUhd/1GWz93QgmRmB5CE5FGm94ixTZkrdjv3CSNCHxs29HbR7JRSRJEiNn1LqEE0cFq9h2ZM4auZ7CAY6DjoEikQqs3btgUAe8cQ57LMnkDA2sPrq+pm+BF0w+9s4nf2AsfUFRtwEq71BYmIOzPXurTydDu4gkM3p3EjZODU+cmE0PuJIahZollF88gzDZgN+07tKca3HgZyCruuqbmk/7FWxPxaCYW2Du1Wqj4PVlzFy/o7mu+7D4uOzxr8OioVCAfV6HYZhYHAwgOHae5hJGuX2Z8I2kL4xCu59p39rODA+M+a73m2J1TrWc7ncFZrGfdu208fMTRqdhoPHI6RapPI8lF6uEskYND0GRC7h0/zdTcH5pLb7NWaz2akLVv7d6dmFoD6wDuFvyfns6LseGcXGVwdrbx+80Po95w+P0j8F40OyH0Lewm6Ibkb7dpZ+A2HofmbUgVesAAAAAElFTkSuQmCC',  '2020-01-13 19:51:25', '2019-10-02 10:25:59', '2019-12-21 09:56:49', '2019-07-18 17:07:52');
insert into books (PK_id, name, author, synopsis, image, createdAt, modifiedAt, deletedAt, published_date) values (2, 'Heffernan', 'Joceline Dach', 'Unspecified fracture of fifth metacarpal bone, right hand, subsequent encounter for fracture with routine healing', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHJSURBVDjLY/j//z8DJZiBZgY4tN9wcO6+0erZd2uKc+fNfoeWGxMcW27Msiq+3GWUdIZXL/okI14D7JqvB+csf3Rv4p6X//t3Pf/fvf35/8Ilj3471V3bph9zmougC6xrr8mETbu7q3jl40/FKx5+LVzy8Ltd+eUZBvGnOYjygk3llfKCZY++u3fcWutcd21B07on/61yz88kKgwsCi8qJc++9yhu2p37ppnnQ4C4oWblo/9WOReXEjTANOsCs1PD9VVZ8+9/N0k7m6Yfe5LLOPFMR+Wyh/9dqq5eUvc6xIbXALOs8zEZc+9/C+q+ddEw/rSfXuRxLfP0swuqgAYEt934pOq2nxenAUbJZ0TjJt9+Vbn80X+v5huXrbLOb7LMOLfVterqjcYVj/+Htd38qey4TxqrAQaxpxntSy7PBvnVPO0MSmCZJ5/ZWL7g/v+ozlv/lex2K2EYoB9zigsYPS6lSx7+j+i59UYn6JgtTIGK635hdY/D9dnT7vxP6L/9X9F+b4icxTYmFAMsMs6ti+2/9S9hwu3/Ac3X32oHHOlVdtoroGS/R0vb9/Aip8ILrwLrrv33rbn63zD02F5Zy22GtM8LdDMAACVPr6ZjGHxnAAAAAElFTkSuQmCC', '2019-07-14 20:36:53', '2019-08-27 02:12:35', '2019-08-31 21:40:38', '2019-12-15 18:47:52');
insert into books (PK_id, name, author, synopsis, image, createdAt, modifiedAt, deletedAt, published_date) values (3, 'Doe Crossing', 'Rina Probbin', 'Other specified congenital malformations of upper alimentary tract', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJySURBVDjLjVJNa1NBFD3vvXxhWprShrSxbTbGYEs2hUoXBaELaTaCICEQJd2Im+JfcF2EUgT/gH8gKymIttkIIlRS3dgs4kfSpMFUmqRJXvI+xnsnJiZ2oZc3794Zzpx7zswoQggcHx8L27bBw7IsOUzTHOThYRjGIMfjccUBCgYGg3M4r9cBIkTvk7WQSQxqzpOTPuztveQZBgTntRoeP3uL/4mnj1bQ7Xb/ELCkHj0wN7+AsSsuzPu98Lgd0A0Bw7RhWNSIMO/eZyFsgU6nI/Eq/9iPlEmhKApauonTcx2tjiUdmbSzSyQds4fhtREFTGD39REBkzRaJmzRgc/rhMOhQaWu4jeBLezLBHQNvf1DJO2uha5hw0uWNE2FqvR6MPYSAYYs8JAkDGZLZEVRRW9dKvjLAk9UTR4Hvn398s9b0MhSn0Dhw0un02Jl5SbqjQbabZ0e1mcUC9/h9/tRLp/iWvg6Qgsh7O+/xsxMANXqGYrFArLZ7FhfwU4mc3BL13Xl6OjoajgcDqRSKTSbTQIWkclkGp8+Zp3JZNLtcrmUcrmMw0MX8vn8E6V/fRzLy8t31tfXXyQ3Nyee7+6eVSqVqdnZWUSjUfCmarWKUqlkjY+Pv9ra2oqR8po67M3tdt+eoOD6/oPUFD+WRCKBtbU1xGIxbGxs4OLiok1xjzEEdTuGCXw+H6irrDMHb2Te3t6G1+uVNVtSVdWjaVqa5ycnJ/qIhcXFxRtLS0sfVldXPcFgEB6PRxLmcjlWh1AohEAgwE//Z6FQUOlsTkcIIpGIRp6j1DFNBH7apPFzJ+8PueH09DR3dpI1i87iBym6+wuQ3GjC/VvaWwAAAABJRU5ErkJggg==',  '2019-12-07 13:19:12', '2019-06-12 02:35:17', '2019-05-02 08:31:32', '2019-12-19 07:56:48');
insert into books (PK_id, name, author, synopsis, image, createdAt, modifiedAt, deletedAt, published_date) values (4, 'Manitowish', 'Beverley Bathurst', 'Puncture wound without foreign body of right shoulder, sequela', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAALNSURBVDjLbZJtSFNxFIdvzKSBIfWlKCqMygiMoj5UhItR9CKaHxSNIqhMtIhsZSsrtbISZTE0nbP0w2Yh03xhmK2WKd3EmGV+KIJKiCTIoBesmXXd07mIlS8XDhf+/J/fec65VwGUcdVRGS1lo83RTUeZht+u4SvupvWSjYbc6In3J8IWAYM8qYXeZikvPG+Cx25oL4P67CCeLMvkgI7KcNqdrXTVwLN6ueyAO4VIV2g6Bw1WaM6FtqvQmAPXd7fiSA7/F9DmsPNEYD3AV6wJaBZd499ONw8bcR00U71Xo17C3Jlg32EbDWgri+FhiUbXLel4RaMpfxGNZyJEt0B0O6nJ7MS1v4CKXRE4kxZRGq/hkSmKTBqFG1Yo3LfZUatFz67rmmm0zqfO0kfLRajNgluH5H0MKhL7KNk+n2KzWULgxh4oWG1TRPcVnTWjs9WfMIpuHd7ziK6HylQT1xJNouvBnS5dY+soWm/kwhrEDHKiXyiiO0ynbNmTpR8aRDeIOwPRNQUCgQRVVVeIronSOLi4Nkj+SgNnlsPNIxKw+KsiusOoVbKYg/p2jVSk6LoD1B7dKPBvv9//i/KkbaL7jrwYsC41krNkdLSzUYOKbPc1t7MR3aDohoturL5dgd0898Gbe3g8HpeauvBUR0LkB785LNQeF671ZSzQA2SEil1HRTddFhM59tkEni2dB71eb0jgkPfcvm+9x9cND7XYCL28y49aC4HDMSM9yTOrlUm/spTA5QJrfOqBjwFads4hKDAlsn3rLLgcxaeiTfg2G/qmglcJPKJ3drlcqtPpVP3m6YSeNvD/8yVvLvo4kwIE9gs8JPCNsbOWHTN/fK86AAL9PKnwWepdhgHfFkP/RDhO4H6Bz/5//ihlXn5X2rKRD9YoBk5P523aNB7Eh2l3txhyxgUIXClwwlR7UVMWnLqzdcZHXVs6v9dh/fwPHS3E3E4GyQ4AAAAASUVORK5CYII=', '2019-06-21 21:39:39', '2019-10-13 03:51:05', '2019-03-02 07:20:33', '2019-08-25 19:31:27');
insert into books (PK_id, name, author, synopsis, image, createdAt, modifiedAt, deletedAt, published_date) values (5, 'Warner', 'Denna Allston', 'Nondisplaced fracture of medial malleolus of left tibia, subsequent encounter for open fracture type I or II with routine healing', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKzSURBVBgZBcFNbJNlAADg53v7tdR1o4vup/yk20IQxqZsLIskCsaTGi7sYDzIzYtctDEkXkwWEhOvix4kRG8eOKkh/iUmDgJiREVh6PidcRnb2Ngcg3Xr1671eaLKm1ozRaNiJQABAQABAFDDrLFk2sk4UzRq90slnUOk0oSYKCJERIEQESKkiBChscnsr6XMt2fFYiWdQ1RSFIZJYeFvHlwhBFIBgRSiQKNBcxsdB+g4W4pBKk3IMjgCGKG6xu0fuPMNm48R0Wgg4r95sk9RJ6gjiinPMfElUxdZWySdY99RXv2I7QcBEdUqG1VqCQmxgBBRnuP654SIONA2wMAx8kWee4crp61NfWWmmpfKD1ibOq+4pVUsIKAwzOESK9PMXmF6nB/fY+g4xcPK2woePXxWoe2QfM+glX/2uPugJlaHFBHSOdp7ae9l3wgXP+D3U8orl1XSZU/uft7y3UmZKNGytSDb1iMWEGH5Kt+9TUc/e0fItfPC+zbG31JJPZTvfkVl7oxMU+TfP2+oV6p2/fSFWEAINALrC0yPM3eZZ44pZ6ls3ym/64iNe6eETE26uUvu0TVtTbtlVx+L1SEi1eDgCdLNTJ83f+5D+gZ19B+1ce8TIV2TrPZYunTVtnKrbC4mIYDQoL2f7cO095qvNax09uvoOyKZ/1Qq01BZ7bJ44Tc7Xz8t2zVErUFCACLWF6mW3bp0xvLyjETR3MTH6jasLe+wNP6z4o4XpbduI/MEm5vUiCVo1LDJ9++6OnnHa8c/c/PMCbcuXLKluF9YmjKwUpNePMeNa9Q2ackRCGaMmf2F5jbynW7fvS/562s9+w/J5fa4/8ekvs6nZfsO0N1DazMtOaYmyBiLKi9rzRSMKiip88blVvu7Ow3v7bE1WdB787qm9YQENSSIkTG2Mevk/++B+Jm41JzeAAAAAElFTkSuQmCC', '2019-12-31 15:09:04', '2019-12-18 15:36:45', '2019-05-16 20:00:47', '2019-03-12 00:40:23');
insert into books (PK_id, name, author, synopsis, image, createdAt, modifiedAt, deletedAt, published_date) values (6, 'Pearson', 'Agata Pele', 'Person boarding or alighting a car injured in collision with two- or three-wheeled motor vehicle, subsequent encounter', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAALRSURBVDjLfVNbSJNhGP6li24C6UIiDHWah83UdJv7d7R5oiiPWGp5yIUHNNF0acNjrmWe5gx1Ujmd+pubW8upaSWZRilZdhN00WVBEAgalSK5p18hczm7eOF7eb/neZ/n/d6PAEDsFWETAoSN8aWbZ/EDXqLIFLr67x27RPpU6LUzl1hJiC08a7461lNo4GYLBjnf/ktwYrPjOF/+JxeZeWtCY+hSTk00FX9TsCroZttSrggb9iSQjJLL4hFybUuyiVwRDHHBpzjg6zmoni3B7CcLrjVnIiY75KpDAtqnVDQcComBXD5tioL5vQ6THwagfqFCQKsfMifiYJzXIrjlGI43+CNQxaK7jpArtM9t77RPWxjFx9CiBjcey1BhTUGZ9SIkWhIBDUzIBpIRGe/zcVsB7XOd9gnBfW6fhCK/njPHg3rTgtrxDJSa45DeH45UYwIMC3fQMa1GoJIJYaq3xc4Cf4AzR+rZIHVsmN61o3osDSWmWKTrpUgejEXXjAoF/SSye4IRVMMCS+HznVnmXWg3A/Ieey3VkIjmqUIUG2OQ1hOOpL4z6JxWorA/AZd6QnB99DySu/zAlB+1+RZ7weey598B8jpCfuSZZWh7Vo703kgUUWIaSCKXVpbVHYRKy1kYF9ogH45DVOuhDXHTwQNeOR6V2wSC29z6kOYA1I2XI0kXQ0vm4eFiJ8xv27eAQwsaaKbk0M81Io+KBrd+/0aA0snZ7hnZ9UEzXnIPdD9vpyUHb4HVT4rQMFkA1aMc1I5m0Q1y0TWrRFqvCN41xK9d+x9YwZoJVLBw4S4ThtcaUPMt0L9qhO7lLZokH9rZOuRQJ8GoIpZcFcRxh5/Iv9RHn6Bl4FSbKyJaXSBsckZGjwCdM7WQ9UfDvYr4clhBsHZtoqPwzHKfY2S4wV/p9DNTHwH3CuIzDfZ1uMp7hXvqkXVGpZPNrWIfXBQEY2ftN8xTb5GsXWfEAAAAAElFTkSuQmCC',  '2019-12-24 05:05:19', '2019-06-09 20:09:00', '2019-02-12 01:45:47', '2019-03-14 07:39:15');
insert into books (PK_id, name, author, synopsis, image, createdAt, modifiedAt, deletedAt, published_date) values (7, 'Lindbergh', 'Agnese Duer', 'Atherosclerosis of other type of bypass graft(s) of the extremities with intermittent claudication, other extremity', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADrSURBVDjLY/z//z8DJYCJgUIwyAwoPZHJBsS7STGABY1/9e+fvzKkGMAIiwWgzRfF2ST0/vz5w/Dw/UOGXz9/M/z6AcK/GH4CMZj+jmCD5C70X2VkgWo+KcYqrqfArcTw598fBhluOTD9++9fIP7N8PsfEP/9AxUD0b8ZVq9ci/AC0Nm//zD+Yfj19xdY0R+got9gxb8RNNQAkNyf/0CxX39QvZC5M+68MJuIAQczJ8PDlw8ZXr9/g9XZIK+BNP/5/Yfh/sJHjIzIKTF2VchNoEI5oAbHDWk7TpAcjUDNukDNB4nVjOKFEZwXAOOhu7x6WtPJAAAAAElFTkSuQmCC','2019-08-03 10:49:12', '2019-07-24 19:03:37', '2019-09-22 12:19:32', '2019-03-13 13:54:32');
insert into books (PK_id, name, author, synopsis, image, createdAt, modifiedAt, deletedAt, published_date) values (8, 'Tennyson', 'Aleksandr Wyeld', 'Burn of third degree of unspecified wrist', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAHaSURBVDjLlZO7a1NRHMfzfzhIKQ5OHR1ddRRBLA6lg4iTd5PSas37YR56Y2JiHgg21uoFxSatCVFjbl5iNBBiMmUJgWwZhCB4pR9/V4QKfSQdDufF5/v7nu85xwJYprV0Oq0kk8luIpEw4vG48f/eVDiVSikCTobDIePxmGg0yokEBO4OBgNGoxH5fJ5wOHwygVgsZpjVW60WqqqWzbVgMIjf78fn8xlTBcTy736/T7VaJRQKfQoEArqmafR6Pdxu9/ECkUjkglje63Q6NBoNisUihUKBcrlMpVLB6XR2D4df3VQnmRstsWzU63WazSZmX6vV0HWdUqmEw+GY2Gw25SC8dV1l1wrZNX5s3qLdbpPL5fB6vXumZalq2O32rtVqVQ6GuGnCd+HbFnx9AZrC+MkSHo/np8vlmj/M7f4ks6yysyawgB8fwPv70HgKG8v8cp/7fFRO/+AllewqNJ/DhyBsi9A7J1QTkF4E69mXRws8u6ayvSJwRqoG4K2Md+ygxyF5FdbPaMfdlIXUZfiyAUWx/OY25O4JHBP4CtyZ16a9EwuRi1CXs+5K1ew6lB9DXERX517P8tEsPDzfNIP6C5YeQewSrJyeCd4P0bnwXYISy3MCn5oZNtsf3pH46e7XBJcAAAAASUVORK5CYII=','2019-07-16 03:24:36', '2019-07-22 15:16:54', '2019-12-11 20:29:25', '2019-04-05 12:57:23');
insert into books (PK_id, name, author, synopsis, image, createdAt, modifiedAt, deletedAt, published_date) values (9, 'Stone Corner', 'Lawry Mangan', 'Injury of oculomotor nerve, left side', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJGSURBVDjLpZNLTxpRFMdd+EX8BixZ6VfxQ9imq3YzSdNVE2MNyEMYAeUNofIKEBjA8CoiM4BCgiQgaJqWtjySAc2/585iogG76eI/czN3/r9z7jnnbgDY+B8pj3w+v5nNZncEQdhLp9N8KpUqJhKJYTwel2OxmByJRIbn5+fFUCjEB4PBPZ/Pt+PxeDZVAJm5SqUCURTRarVUNZtNdd1oNFCtVkHBEA6H4XK5OBWQyWQwnU4xHA7RbrdRr9eVn8vlsiK2ZnC2NxqNMB6PcXZ2BhVAacu3t7eYTCYQbr4jIP2ErzWHt/0I780jnOIUjsoDYlcDjH//UYAOh0NWAXTmbTrzUmpew3bRA196gONqAndrARfJevkLXzJ9fI5dwxkvwG63L09OTrZVABMVTBuNRpfVegPWlIRPvhI+nF7gHZ/FG4sAzl2AP1V8YX4BYKJKa6nSy8srEZakiPeneby1CvjoKeJrurRiXgEwUZu0fr9/+a16iVStC9/FNSLCevNaAJPX69W63e6nxWKhdIfMT+vMrwKYnE6nl7WtVCqB53nbPyfxuSjyFvV4l9pU6Xa7yOVysFgsebPZvGs0GrdeBdBoami6ioFAADQXoPHFYDBQ3lQXUHFxfHwMnU5XPDw81KwAyPxDkiTIsozZbIa7uztFDHJ/f698Y3vJZBIHBwejFQClzbFIhUIBnU4H/X4f8/lcUa/XQ61WU+7A0dER9vf3ubU1sNlsGqvVylGqWZPJ1DEYDNDr9SztLqWdpcgcmTXPPX8BpLUNr3FYePgAAAAASUVORK5CYII=', '2019-09-22 03:03:38', '2020-02-01 05:52:21', '2019-11-01 04:51:46', '2019-12-07 08:59:53');
insert into books (PK_id, name, author, synopsis, image, createdAt, modifiedAt, deletedAt, published_date) values (10, 'Sutteridge', 'Gothart Cleiment', 'Toxic effect of tin and its compounds, assault, subsequent encounter', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAK/SURBVDjLbZNdSFNhGMf/52xunTldrM2JaeomlZnTalJQOSmyLithSRBFH0ReJX3QlVfhTeWFUJFQCHVTV9WFVCCUaZaYpIb4kc1mmihOp5vO97znnJ4jFqI78Lt5n/P/ned9n/MK1dXV31VVzSYm6+vrPUjwVFRU9Mmy7OGc/2xqaspfXTNSME9RlKfExcrKSi2RgMI6dcSxtTUjBc3ESbLLzgt1fINoBuMamApcct5HlhU4c7XdSGE/sWOtQKRgD/GQGOxIapBiUkQSLQ5JkxxSEF7JJk1IjLEfRCPRva4DCqYSfoLbkr7hY6wTPsM9iMo2vA2fQPdsIRg7x+nrpwhbIoFMBKlY6DIDJZusmGVViC54EY6UY3Deqe9fJfR3XOsEtMhWBAVHXR6YBAs4chGzTQEZz6AKMh4zhhXB7rUCIRAIhKgQdrlcRTMzM4mGgN7e3kX/DauZQxaZyi2N53sWV3cwR7wKBoN8e83rPZLBBKbQ6GgKAecbbLYaUF5W1b+kyjuz0/LFvrGuheMPciwvq0aWJUY6WRMJcglDR+wRiqUKmLUMiKOleDFC89c4DtxK3pVhz0N++l7Mx2Po/v05WnrXbmu5Ho7qgqQVgZBiHEZ7+A6Kkq/BqjIcKTgLRVOhqApUaBiPjKIwcz+ibFH8GvoQ8d4W7foWIsRz4orDbEKxfSNm4g2Ic7Yc/jU9RNvh4Cp1o8iYW5pHcdZBROUF8UuwdVrvoIjCp4kcf1qmloQUyBYDWnhc4AqHK3ULhZVl2Z9ICHZrOrpG29A23DrFOMoFn8/HKHzZZDI9cbvd/0+elXUgTuNjtBW9G4+jAPvc5egMfULz4LsJCh8er9X6BK/X208CN9EzNDTkSzRGSZIG3DeXtpZ4DuH9YPMY3RM9PLD8H2hawgu47nHXCDFFNVgWVcU9WasF/63/Ban+u4K8LTKZAAAAAElFTkSuQmCC', '2019-09-12 15:16:01', '2019-11-17 23:58:28', '2019-04-27 19:51:27', '2019-03-21 19:40:01');
-- -----------------------------------------------------
-- INSERT subjects 
-- -----------------------------------------------------
insert into subjects (PK_id, name, description, createdAt, modifiedAt) values (1, 'Theobald Dyzart', 'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem.', '2019-10-20 03:02:35', '2020-01-20 01:02:51');
insert into subjects (PK_id, name, description, createdAt, modifiedAt) values (2, 'Jerry Ledeker', 'Fusce posuere felis sed lacus.', '2020-01-22 07:48:53', '2020-01-23 22:28:55');
insert into subjects (PK_id, name, description, createdAt, modifiedAt) values (3, 'Tades Woolliams', 'Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti. Cras in purus eu magna vulputate luctus.', '2019-12-12 06:20:19', '2019-02-16 04:31:20');
insert into subjects (PK_id, name, description, createdAt, modifiedAt) values (4, 'Reena Gudyer', 'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis.', '2020-01-25 13:14:32', '2019-08-03 20:57:32');
insert into subjects (PK_id, name, description, createdAt, modifiedAt) values (5, 'Elfrieda Hiley', 'Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla.', '2019-09-01 05:00:32', '2019-03-11 22:14:43');
insert into subjects (PK_id, name, description, createdAt, modifiedAt) values (6, 'Cassie Steade', 'Aenean auctor gravida sem. Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio.', '2019-03-22 16:06:20', '2019-10-24 12:00:08');
insert into subjects (PK_id, name, description, createdAt, modifiedAt) values (7, 'Mariel Litherborough', 'Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis.', '2019-12-21 09:01:49', '2019-11-07 04:48:54');
insert into subjects (PK_id, name, description, createdAt, modifiedAt) values (8, 'Jayme Hubbucks', 'Cras non velit nec nisi vulputate nonummy. Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat. Quisque erat eros, viverra eget, congue eget, semper rutrum, nulla. Nunc purus. Phasellus in felis. Donec semper sapien a libero. Nam dui.', '2019-05-17 18:50:58', '2019-09-05 01:51:55');
insert into subjects (PK_id, name, description, createdAt, modifiedAt) values (9, 'Lea Rudall', 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Mauris viverra diam vitae quam. Suspendisse potenti. Nullam porttitor lacus at turpis. Donec posuere metus vitae ipsum. Aliquam non mauris. Morbi non lectus.', '2019-08-23 19:16:47', '2019-04-14 13:41:44');
insert into subjects (PK_id, name, description, createdAt, modifiedAt) values (10, 'Emelyne Smeal', 'Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique. Fusce congue, diam id ornare imperdiet, sapien urna pretium nisl, ut volutpat sapien arcu sed augue. Aliquam erat volutpat. In congue.', '2019-06-11 22:30:31', '2019-06-08 19:33:21');
-- -----------------------------------------------------
-- INSERT services 
-- -----------------------------------------------------
insert into services (PK_id, name, description, createdAt, modifiedAt) values (1, 1, 'Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.', '2019-05-14 06:26:48', '2019-08-31 16:58:57');
insert into services (PK_id, name, description, createdAt, modifiedAt) values (2, 2, 'Duis bibendum.', '2019-11-21 16:45:02', '2019-02-23 01:06:56');
insert into services (PK_id, name, description, createdAt, modifiedAt) values (3, 3, 'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros.', '2019-03-06 21:38:23', '2019-04-13 22:14:31');
insert into services (PK_id, name, description, createdAt, modifiedAt) values (4, 4, 'Praesent id massa id nisl venenatis lacinia. Aenean sit amet justo. Morbi ut odio. Cras mi pede, malesuada in, imperdiet et, commodo vulputate, justo.', '2019-07-19 01:18:19', '2019-09-17 06:18:26');
insert into services (PK_id, name, description, createdAt, modifiedAt) values (5, 5, 'Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum.', '2019-05-28 12:34:48', '2019-12-10 12:45:30');
insert into services (PK_id, name, description, createdAt, modifiedAt) values (6, 6, 'Nunc nisl.', '2019-12-15 05:13:06', '2019-08-25 05:51:29');
insert into services (PK_id, name, description, createdAt, modifiedAt) values (7, 7, 'Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus.', '2019-04-24 08:36:50', '2019-08-24 00:41:05');
insert into services (PK_id, name, description, createdAt, modifiedAt) values (8, 8, 'In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna. Ut tellus. Nulla ut erat id mauris vulputate elementum. Nullam varius. Nulla facilisi. Cras non velit nec nisi vulputate nonummy.', '2019-09-26 20:24:45', '2019-10-17 07:20:29');
insert into services (PK_id, name, description, createdAt, modifiedAt) values (9, 9, 'Vestibulum quam sapien, varius ut, blandit non, interdum in, ante. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.', '2019-11-01 18:40:25', '2019-09-11 01:49:55');
insert into services (PK_id, name, description, createdAt, modifiedAt) values (10, 10, 'Integer ac neque. Duis bibendum. Morbi non quam nec dui luctus rutrum. Nulla tellus. In sagittis dui vel nisl. Duis ac nibh. Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti.', '2019-06-02 11:51:03', '2019-02-15 23:12:14');
-- -----------------------------------------------------
-- INSERT rooms
-- -----------------------------------------------------
insert into rooms (PK_id, roomIdentifier, location, size, image, createdAt, modifiedAt, deletedAt) values (1, 1, 1, '120', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKnSURBVDjLjY9NbFRlGIWf+zfToTMFWjKUQrTlJyk1FBYYlI0KkuimceHKsGGhG2JcGKNxRySBmBj3JG5YGMJKF5poNRAbfxJDCy1UsJZGbaCddmY6c+femXvv970va9ox8SRnd/LkPI6qsjnp7PBBFb2pyqCKeCokKjqPcKH3xeWvnxqr6pa2Z54Js8dXVeM51davqo1JzVavanNq6K/NW58uUStFf8dx6DwCU4esjpsvIUYObN52BYgFSZZxsg3QDra9BImLNVt1/+OBYpu3yFa/Q5IWEjcIBk4gBv7nA8HN7yXoO4wmK0iwDVHBGtkKSGae7VPReRUZVMFTq4mjDib8A1ObQzstbBxS2D/eXUGsfOyXP9gb7J4A7YCN8lm0SPzgAoVdJ9GeFbx8HdRgM7oB9EO//DqYJtgm2BZ+oUwWVfHcebQdYqOQwCljsy4K1gASoaYFmiLJGqRtjBFy2w+iPRVsvsbNapEbG6dofv6GhnEj3GjVPv320p2LvhjBRgvYjWkkXUWTf/H69mNTJandR+OIH1Z7udvr8/JLr7Cv/xA37n1V+u3u1CdnPxo87tpMcYIBvMIIfu8ofvEYqMVkSn7nIXL9e/hmpcSx0aNY13J0zxmsk/HCkZP8HbkTrjWC47qYeAHTnMU0b+MXn8OkShatk1YrVMIagVNk4vC7ALx/+goHyuNkiuPajOvt9RmC0hh+aYyg/wRx9SFevh+7bYie0VcJwzXuPfqFy5PnALj8/TkWK7N4DtZ7783CPjfX95rbU8TxHbK0Rrj2J+Hywo+Nf+7Xqw+md7Uhmq5X88O7h1mPl2lEFX6+M8VQO/7JeXhtYEQM74iVM9ZyRAxzYnVSLFfGz9eXAJa+2Dny2WLuy9mW93wqeDkHO1Ywv7+9o/PWEyzemnF7ZfggAAAAAElFTkSuQmCC', '2020-02-05 00:33:05', '2019-05-25 19:15:15', '2019-11-26 13:14:55');
insert into rooms (PK_id, roomIdentifier, location, size, image, createdAt, modifiedAt, deletedAt) values (2, 2, 2, '50', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJ+SURBVDjLfZPNTxNRFMXnX3DhxkRN48aEf8CkYaFGAy7cKImRCAv8iInGnSDGhSFEsJqoUWxABGNQ0zZFGrWRfqS1rf2ype1MC5FaytBmirWK1M7YTofjm0GaUAqLt7lzz++ce98bKh6Pg2EYxGIxRKNRRCIRhMNhhEIhBIPB3QConQ5F0zQkSdpyMpmMAvH5fDtCKNlZFrAsqzin0+kaRK6RFHC73dtCKDmy3Cy7yYVAIBAqFosQRVGpy0Cv1wuHw9EQQsmuG41EXCaRWUEQkCuKsC2tJ0mlUnC5XLDZbFsglOy8EblarUIWfy9JuDuzhr4vgJVd/5ZMJhGwPMLcxBnMT/UjMnoc0SdNHRSZkc/n80pTpVIBR5wHw2sYYoDhOKCZWYf8yUyCs3djJfYOKGbxK2aA915LjvL7/c1kRn55eVmB0HlRcdYSwPNZYCQBvPn8HoX4bZSXTOAcGvC0EdW0G/TYRUGZw+PxqMmMfC6XUyCJHyIGQhK0JIEp7ESBuQWJd4P/dgWFwFXMvuwiI5yHc+TaodoynE6nmmya5ziuBhl32/GTvgFJ8KKU7ITAtmM10YvU2ElYJl/AYDDs37RRsmW11Wrls9ksssxbcMHu/+IOCItn8Zu5iaT2BOzGUVmsUm6h/lqmp6fV5ld95cTH6yT2JwgLXfjLnsMKSTL/tAUW02vo9XpV7RrrAc8u7+2Kf+hF1PwAGX8bSgvtKER68HWYxJ6a2CRuCNANHgVW05gbPg177x54tK1waY7BYhzfIm4I6LvQJJa9j1H2P4S//zB0lw7ArB+FTqdTNXzK9YW25l3Cnc6Dom2gVbTeP+I0DvWcIuJ92/1M/wCZISaoLgB85AAAAABJRU5ErkJggg==', '2019-09-06 13:13:59', '2019-09-10 13:28:36', '2019-12-24 06:18:35');
insert into rooms (PK_id, roomIdentifier, location, size, image, createdAt, modifiedAt, deletedAt) values (3, 3, 3, '250', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIwSURBVDjLhZHLaxNRGMUjaRDBjQtBxAdZFEQE/wEFUaurLm1FfEGzENwpturG6qIFrYUKXbUudOODNqIiTWqvFEwXKo1UUVRqS2NM0kmaZPKYPKbJ8XzTiUQxceDH3HvnO+e73xnH8X7fLjJInjbgEekiOwA4/sbBD0Ov5sIqY5SVXiO/Rpospw01HphXrOttZPBMxCkWJ3NltZItq3i2pOKZklrWi9Z5SMuKwf2GBtJVxJotiqWLKpIqqHCyYO3/Z/A8UyirBDtLcZTi6Y+RdxdHAsnTAy/NM0TerCuRlE2Y9El+YjCWoLBkViyxdL40OpNmLuBo0Gvk12AuYC5gLqB2XAw8A2NBFZzXVHm1YnHq1qQpYs4PjgbmAuYC5gLe0jrnWGLwzZqDi33ksSTunw3JvKZ0FbFmi5gLeDswF2v/h4Ftcm8yaIl9JMtcwFys4midOJQwEOX6ZyInBos18QYJk0yQVhJjLiiald/iTw+GMHN2N6YOuTB9YieCozfE4EvNYDO5Ttz2vn/Q+x5zC3EwEyw9GcaH7v0ovLiN6mcf8g8v4O35vRg+edTr+Ne/tU2OEV03SvB3uGFQjDvtQM8moM+N+M0D8B92LjQ0sE2+MhdMHXShOutF/ZO6toXnLdVm4o1yA1KYOLI+lrvbBVBU7HYgSZbOOeFvc4abGWwjXrLndefW3jeeVjPS44Z2xYXvnnVQ7S2rvjbn1aYj1BPo3H6ZHRfl2nz/ELGc/wJRo/MQHUFwBgAAAABJRU5ErkJggg==', '2019-09-11 15:21:52', '2019-10-28 21:25:52', '2019-09-05 23:42:29');
insert into rooms (PK_id, roomIdentifier, location, size, image, createdAt, modifiedAt, deletedAt) values (4, 4, 4, '300', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAI3SURBVDjLpZNNSJRxEMaf1XVVyA/8iH1fKdEKIzwpSIeCLmkRUaaXSsE6tB3qYLIQeTUMCwqEKNI6hJHRNamwEDSXCENKoaKNNjLCd9PDRuX7f2b+HUSzcLs4t2Hm+Q0zPBOw1mItkbEy6RiP9LSPnuhK19x6r6nn8MDBrlUBHc8iveFsN+rkOJ2nnhy78q+4ZbCp180ri7qFbmfjjb3L9UDH+MlMq9oXznHaNuVvgYjg3dwbJOYT/UKJkAIx0ucUlLVVOVWgJabiU4jPxPtpJBJU1bMqWpuFEFQVFhakgEbqhBKlkUyStUJCrEBVEQxmwfisEyPRgLUWp58e3xgKhBJ1znaoKh6/fYRbjYOBlSscuN5g62vrQSsYjg0j9T1VPnIu9ikDAISyOFUEtITS4sidxtCy+FpDSI2CSlAJYwj68ueIQoEYQixBEYTzwqBh9xLA+Ox2SlwYXazTX4QAQBAASJlZMP6cl/KK8nMLUF5agR+/fp7Zf3V3tVDgFLv1lRsqQCVmkx5SqdQcKTMAEFgyUsvdQ82kDFSUVIbcYhcKC2/egwhRWFAEqkH84wdMTU/7Qjk6cfHV/b8AANB8c99mGukk2bq1fFvm+uJSqAo+f/2CickJEcptGjn/8tLr98s+WM3Key7v2rEuN2+0proGYhWx5zF4yeTOFxcmx/5r5aV42D4y5n1LLvhi4NNg1vMWVhOnBQAAfWarVSgUNJKdri+YFmCIB0NDECMQStpvDKz1nX8D4+Fd1+gIFK0AAAAASUVORK5CYII=', '2019-02-18 04:59:10', '2019-03-04 22:30:16', '2019-10-17 10:55:33');
insert into rooms (PK_id, roomIdentifier, location, size, image, createdAt, modifiedAt, deletedAt) values (5, 5, 5, '450', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAADVSURBVDjLY/z//z8DJYCJgUIw8AawgIj58+c7A6lWIDYnUt89IC5MTEzcxAIVmKyvr6kpLi4C5jAygkkoG0FD2IwMr1+/VTp9+uJUIAdugIiQED/Do0cvGX7//gvxGxMTXBMIw/gsLCwM0tLCYD1wL0AAIwMzMzPD37//4YqRDUEYwAxkM6OGAcxGZmYWoAIGFA3oNDMziGbCNAAkCJL8/58Fp+0QS1ANYJw3b95/BQVZBj09bXjgIQIMxkelQeD8+UsM9+49gLjgwYPHYEwOYBzNCwwAGT0uf+Tb34kAAAAASUVORK5CYII=', '2019-03-08 08:27:03', '2019-04-24 14:22:14', '2019-04-28 22:03:21');
insert into rooms (PK_id, roomIdentifier, location, size, image, createdAt, modifiedAt, deletedAt) values (6, 6, 6, '1200', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJGSURBVBgZpcFLaFVXFIDh/5yc5EZJhN6E+koziC/UtqgIUYOIbZWGgkNLUVAcOHToRBCU6qygExGHQmfViSCCD0RQELVQCoW2UDW2iimlJrnn7rP3WnstMxA66UDI9xXuzkKULFDJAlWnfvjb+R9qhogRVRGBoE6IShQlJKcWIwSlYt74mkX8x1FzcnbUHM2OqCNmiDiihmQjqXHpzmsq3pntKtlAzdFspOyIGqJOUiNqJokT1UhitAcqQkcpmWfu7Pl0kCiZrzYPsn/7BxycaHN41xAT6wboqwq+2TFMf1/Bkd0f0omZbsxYEMqoiqjRqgqiGK3ekos3pykK+O7aK0aGelm7vMWFu18yNbuPlW2om0wQw2qlrKPSqNHqLTiwc4iqLKij4u50YsYMxtcMEDUxMryeo5e30E2BKBmvM1UnODEZIRnnr7/m2ORSuo1x4upuDOHM9UwSZUV7NeuXjTPX1PwUJunKDSwmqpmQaNIi3KGOijt0okJfYs/GQ2Q3smUM5+XMCz4ZmaCTAk+mtlPYZao3XSGkzOkrf5LEOP79M6I4PWUiu/H8n98RU9QEycJsnGPTRzvpSJc3X3xNNTunhJQZ7O8h9Rb0S4G0nOnUoFlZumQUtUx249XMFO2BZfz44j4P//iFf++do1h17Ik3QXFxLBqeFBdjYvIITUokSzSaWDW8gW1je3k89YBbPz+ifngG74xSuDvv4+Nvy3rr6GeL7/52+69kfP7yrP/KvMLdeR9jJ4s6W8/iYHls+qw/5Z23QlWChZhP1DoAAAAASUVORK5CYII=', '2019-04-13 22:32:51', '2019-02-13 00:55:04', '2019-05-13 03:03:37');
insert into rooms (PK_id, roomIdentifier, location, size, image, createdAt, modifiedAt, deletedAt) values (7, 7, 7, '80', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAJrSURBVDjLjZNdSFNxHIZPnfS+usiborU2JaKIPvAmig0kkYSCCpHKsTnDolLXnBFJBQV9YC2VtDRThKiYzA9coW6abWSTwRixYqPtIqHTWGM7+3Bfb+d/ZEeHXjh4Lt/395x3/CkAlMPhODHzSIUpXTk+KUpAUdSRdbKThPfZ7XZ2zjaLeds0TBeLkU6n1wVfotPpCo1GY83HB3X40l0H04VixOPxPKLRKFiWFQiHw0ilUksF+QYWjFdLVxXkSnJEIhEkk8l8g/F7dZjtUmO8SopEIsFzc9AP3YAfzRzafh+0fT5oOIiFUCAYWJcMxs5Ksbi4yEPCAxYG/RxvzAz6phg09P7iC4hVnsHYXTU+d9Zi9IyEbyesvNrEBRtfeXDtpYffIBaLZdY0GD4t4QciBblPIZCL5NtJOBQKIRgMslzBwWWD1lrMtKswfGq3EG567UNDjxdXuz243PUT9S9+oP65CwzDIBAIrLGB1QJjpZgvIBvkLnO68Bv0sCn2YEJeAHPVDljbroOm6VLBYOSWCtN6JYYqxMKIBBL2vnsKp/Yo4mNPkP1uQvRtI75d2Y9nh+jHeQZ2zsBQvksYkRiQxS3VYsS4MPQngebNwH0R/j48jhEZ/XvZoEUJc5sChrLlAnKdDEe0s/MGrPz9ay3ChGxTdpXBB7kImUyG/ydyBuZz28H2KAEulNBSCHL4L9EYldMMb6DRaESDFXsxdP4w3stE8Hg8cLvdcLlccDqdmGu/ga/qEiw0i8C0FMCr2oDJykJ0lG7spMhzJtRIthy7faDojlK6VbXW0+0t29YxIqcXiDZ3+Q8f5p7zf7M8wtRUBE6BAAAAAElFTkSuQmCC', '2019-04-06 13:47:06', '2019-02-21 18:37:14', '2019-12-12 12:20:10');
insert into rooms (PK_id, roomIdentifier, location, size, image, createdAt, modifiedAt, deletedAt) values (8, 8, 8, '111', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAKLSURBVBgZBcFJaFxlAADg7/3zZkjbYBfTZIg1SNVCFjW2hgbUQwWXHkQDHkUPgkcdiuBFiEVvXoJ4kHiqiHhQLAUXqCjGiNGaSG1Ra9pqp5JI4tY0y5u3+n1R5xm7GgMmxVoAAgIAAgAgx5KptO143Bgw6fYjLX2HqNUJMVFEiIgCISJEqBEhQlWwdKbV+PiUWKyl7xCdGs0xalj5ib8WCIFaQKCGKFBVdPfQe5DeU60Y1OqELu6eAEyQbbB4mosfUawjoqoQ8e+fdN1ISVAiitlc5tyHXJ5lY5X6DoYe5+jr9I8DIrKMJCNPSYkFhIjNZc6/Q4iIAz2jjD7JzgEOP8/CNBdP08lJO2QFCUFAQHOMJ97mgVe47THWLvP5i7RngIPP0jdKp0OSk+WkxEqoEaG+g72D7B1kaILZV5l/k7J0bO5l68maN6oBjWSLrCAhCIjwz1k+eY75aTZWqW/nvpfovomZKUnWsa9n0FPlBWknIytJiQWEQBXYWqH9Bcvfaa39qqpyaZlJ80z/ngMGm4ddTzY8ujXnZGfEtpRYCRG1ivEXqHfT/lL+33kPDj+tqEpFWShVlq5ddce+e62nW460T/isIAahYu8I/WNAlUvOTSuq0pW/F2VlLi8zWZFZ61w3evP91rNNY3fOioGIrRWyTerbWb0kyRN5keu7YUBeFoqqtHytbU9308LVr/382/fe/4FYiipHwafHqO+kfdZWmTjxzWvSMpXkqf09w8b3P+TMlVk/Ls54d/cjDiQnxf4wZenblt4xulBk3HKXD7IRsoK0ICvc8/tborDNL5fmvLf7qFvbbRqmos7DdjWaJjW1lMiRIEWKBDnDQ6yr+Wq+MFCgYSpZcvx/t+Akg61CC8wAAAAASUVORK5CYII=', '2019-12-26 12:25:21', '2019-07-06 03:54:02', '2019-12-25 02:16:52');
insert into rooms (PK_id, roomIdentifier, location, size, image, createdAt, modifiedAt, deletedAt) values (9, 9, 9, '125', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAAK/INwWK6QAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAIiSURBVBgZpcE7SJZhFMDx/3neV/u8ZlhoVxAkESoyJYoa3BojDCFc25psaS8CWxoEhxAagiCpHCpqaa3AyiIISwjTtHIou3wX/b73nFOPIEG0SL+fuDv/Q04Mjp052ttz6WvR69wBM9wMNcXNMTdcFXPHVVEzGqsrhamphXPjl/tH0p4jPcNVubrQkmM96gpFHQZG0mLFQ/FrnvUqVTzwW+rqXBxoZ71OD80Spe5GVM4UB9wcNTAcM0fN0MzRzFE3yuq0tTagpkQBdyIJQhAIQQgJJCKkIZAmKf7zBeV3Q1RJidqqlMgyJQpqShQAEUGCkAQhJIIECF5ieW6c+uZ9VD7dJ60ORKZGFNycVSJEAQgihCAkiVD88IDa5i4at3ZRmHsI+RkiMyUKZsoaEQERogBofoFv7+7RsLkJ/XGHLZ2n+P72Bm4ZZkYUskqFVSKICJGIEH15c5Pm9uOwPMnEtevUN5X4MfOI77OPySoZUXA1ogQQQEQQoPB5Ei0s0bCpiK3MgBuaf0pb71nmn1yhimWiYGasESAA4sris6s07dqPFV/hVqK7rwMrfySXm6ZxxyG6aiaI5MTg2FjLzm39poqpoars2fCUkwdztO6uQfMTuJd5fnuK7r5OJNkINcd4NHphpdpLB8Td+dvE8OH5vQPXtyfhPZ4tAc4fgaSmg8XXL5m+e/5Wyj9kK+Xc5Ghfyc1xM9wMN8PNcTPcHMxw99ZfSC4lgw+6sSMAAAAASUVORK5CYII=', '2019-04-04 17:06:02', '2019-04-10 22:09:31', '2019-10-22 13:56:57');
-- -----------------------------------------------------
-- INSERT skills
-- -----------------------------------------------------
insert into skills (PK_id, name, description, createdAt, modifiedAt) values (1, 'Talyah Dottridge', 'Sed accumsan felis. Ut at dolor quis odio consequat varius. Integer ac leo. Pellentesque ultrices mattis odio. Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla.', '2019-12-17 15:18:27', '2019-03-31 21:00:17');
insert into skills (PK_id, name, description, createdAt, modifiedAt) values (2, 'Dede Campa', 'Suspendisse potenti. Nullam porttitor lacus at turpis.', '2019-04-18 00:01:23', '2019-08-24 02:22:24');
insert into skills (PK_id, name, description, createdAt, modifiedAt) values (3, 'Rees Demangeot', 'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam vel augue.', '2019-09-09 03:46:37', '2019-02-28 08:56:34');
insert into skills (PK_id, name, description, createdAt, modifiedAt) values (4, 'Truda Dwyer', 'Aliquam non mauris. Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet.', '2019-08-24 09:13:49', '2019-04-01 07:47:06');
insert into skills (PK_id, name, description, createdAt, modifiedAt) values (5, 'Obediah Overy', 'In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat. Nulla nisl.', '2019-02-26 05:56:06', '2019-07-18 08:22:27');
insert into skills (PK_id, name, description, createdAt, modifiedAt) values (6, 'Lurette Curness', 'Nulla ac enim. In tempor, nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.', '2019-09-15 14:06:45', '2019-11-23 23:46:38');
insert into skills (PK_id, name, description, createdAt, modifiedAt) values (7, 'Laurena Malham', 'Mauris lacinia sapien quis libero. Nullam sit amet turpis elementum ligula vehicula consequat. Morbi a ipsum.', '2019-05-18 05:48:40', '2019-05-30 08:13:38');
insert into skills (PK_id, name, description, createdAt, modifiedAt) values (8, 'Sharlene Lummus', 'Proin interdum mauris non ligula pellentesque ultrices. Phasellus id sapien in sapien iaculis congue. Vivamus metus arcu, adipiscing molestie, hendrerit at, vulputate vitae, nisl. Aenean lectus. Pellentesque eget nunc. Donec quis orci eget orci vehicula condimentum. Curabitur in libero ut massa volutpat convallis.', '2020-01-26 22:34:09', '2019-10-17 06:47:56');
insert into skills (PK_id, name, description, createdAt, modifiedAt) values (9, 'York Semor', 'Morbi non lectus. Aliquam sit amet diam in magna bibendum imperdiet. Nullam orci pede, venenatis non, sodales sed, tincidunt eu, felis. Fusce posuere felis sed lacus. Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl. Nunc rhoncus dui vel sem. Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla.', '2019-11-04 06:33:47', '2019-11-28 22:11:27');
insert into skills (PK_id, name, description, createdAt, modifiedAt) values (10, 'Koren Briston', 'Curabitur at ipsum ac tellus semper interdum.', '2019-09-12 13:56:48', '2019-02-22 00:22:47');
-- -----------------------------------------------------
-- INSERT mentor_skills
-- -----------------------------------------------------
insert into mentor_skills (FK_mentor_id, FK_skill_id) values (1, 1 );
insert into mentor_skills (FK_mentor_id, FK_skill_id) values (2, 2 );
insert into mentor_skills (FK_mentor_id, FK_skill_id) values (3, 3 );
insert into mentor_skills (FK_mentor_id, FK_skill_id) values (4, 4 );
insert into mentor_skills (FK_mentor_id, FK_skill_id) values (5, 5 );
insert into mentor_skills (FK_mentor_id, FK_skill_id) values (6, 6 );
insert into mentor_skills (FK_mentor_id, FK_skill_id) values (7, 7 );
insert into mentor_skills (FK_mentor_id, FK_skill_id) values (8, 8 );
insert into mentor_skills (FK_mentor_id, FK_skill_id) values (9, 9 );
insert into mentor_skills (FK_mentor_id, FK_skill_id) values (10, 10 );
-- -----------------------------------------------------
-- INSERT book_subjects
-- -----------------------------------------------------
insert into book_subjects (FK_book_id, FK_subject_id) values (1, 1 );
insert into book_subjects (FK_book_id, FK_subject_id) values (2, 2 );
insert into book_subjects (FK_book_id, FK_subject_id) values (3, 3 );
insert into book_subjects (FK_book_id, FK_subject_id) values (4, 4 );
insert into book_subjects (FK_book_id, FK_subject_id) values (5, 5 );
insert into book_subjects (FK_book_id, FK_subject_id) values (6, 6 );
insert into book_subjects (FK_book_id, FK_subject_id) values (7, 7 );
insert into book_subjects (FK_book_id, FK_subject_id) values (8, 8 );
insert into book_subjects (FK_book_id, FK_subject_id) values (9, 9 );
insert into book_subjects (FK_book_id, FK_subject_id) values (10, 10 );
-- -----------------------------------------------------
-- INSERT room_services
-- -----------------------------------------------------
insert into room_services (FK_room_id, FK_service_id) values (1, 1 );
insert into room_services (FK_room_id, FK_service_id) values (2, 2 );
insert into room_services (FK_room_id, FK_service_id) values (3, 3 );
insert into room_services (FK_room_id, FK_service_id) values (4, 4 );
insert into room_services (FK_room_id, FK_service_id) values (5, 5 );
-- ---------------------------------------------------
-- TRIGGER
-- ---------------------------------------------------

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
//* API PARTUH*/

Router::scope('/api', function (RouteBuilder $routes) {
    //Permettre au javascript de récupérer les cookies et reformer le CSRF token dans la demande
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => false
    ]));
    
    //Toutes les actions de notre API seront de type JSON (Données seulement)
    $routes->setExtensions(['json']);

    //Gestion des routes de l'authentification
    $routes->connect('/profile', ['controller' => 'Users', 'action' => 'view']);
    $routes->connect('/sign-up', ['controller' => 'Users', 'action' => 'add']);
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/allusers', ['controller' => 'Users', 'action' => 'indexAll']);
    $routes->connect('/logout', ['controller' => 'Users', 'action' => 'logout']);
    $routes->connect('/mentorskill', ['controller' => 'MentorSkills', 'action' => 'add']);
    $routes->connect('/booksubject', ['controller' => 'BookSubjects', 'action' => 'add']);
    $routes->connect('/mentorReport', ['controller' => 'Reports', 'action' => 'ReportMentor']);
    $routes->connect('/bookReport', ['controller' => 'Reports', 'action' => 'ReportBook']);
    $routes->connect('/roomReport', ['controller' => 'Reports', 'action' => 'ReportRoom']);
    $routes->connect('/irenting', ['controller' => 'Renting', 'action' => 'SelectedLoans']);
    $routes->connect('/renting', ['controller' => 'Renting', 'action' => 'index']);
    $routes->connect('/renting/return', ['controller' => 'Renting', 'action' => 'return']);
    $routes->connect('/renting/setIsPaid', ['controller' => 'Renting', 'action' => 'setIsPaid']);
    $routes->connect('/urenting/:id', ['controller' => 'Renting', 'action' => 'userIndex'])
    ->setPatterns(['id' => '\d+'])
    ->setPass(['id']);

    
    $routes->connect('/listM', ['controller' => 'Mentors', 'action' => 'rentList']);
    $routes->connect('/listR', ['controller' => 'Rooms', 'action' => 'rentList']);
    $routes->connect('/listB', ['controller' => 'Books', 'action' => 'rentList']);

    $routes->fallbacks(DashedRoute::class);
});

Router::scope('/', function (RouteBuilder $routes) {
    //Toutes les requêtes doivent être reporté dans la page html Angular
    $routes->connect('/*', ['controller' => 'Angular', 'action' => 'show']);

    $routes->fallbacks(DashedRoute::class);
});





Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => false
    ]));


    $routes->applyMiddleware('csrf');

    $routes->connect('/allusers', ['controller' => 'Users', 'action' => 'indexAll']);
    
    $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    $routes->fallbacks(DashedRoute::class);
});
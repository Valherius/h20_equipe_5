<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Mentor $mentor
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Mentor'), ['controller' => 'Mentors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Mentors'), ['controller' => 'Mentors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Skill'), ['controller' => 'Skills', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List skill'), ['controller' => 'Skills', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New users'), ['controller' => 'Users', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List active users'), ['controller' => 'Users', 'action' => 'index']) ?></li>         
        <li><?= $this->Html->link(__('List all users'), ['controller' => 'Users', 'action' => 'indexAll']) ?></li> 
        <li><?= $this->Html->link(__('New service'), ['controller' => 'Services', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List services'), ['controller' => 'Services', 'action' => 'index']) ?></li> 
        <li><?= $this->Html->link(__('New subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li> 
        <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout'], ['confirm' => __('Are you sure you want to Logout?')]) ?></li>
    
        <li><?= $this->Html->link(__('Edit Mentor'), ['action' => 'edit', $mentor->PK_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Mentor'), ['action' => 'delete', $mentor->PK_id], ['confirm' => __('Are you sure you want to delete # {0}?', $mentor->PK_id)]) ?> </li>
    </ul>
</nav>
<div class="mentors view large-9 medium-8 columns content">
    <h3><?= h($mentor->PK_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Firstname') ?></th>
            <td><?= h($mentor->firstname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lastname') ?></th>
            <td><?= h($mentor->lastname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ContactNumber') ?></th>
            <td><?= h($mentor->contactNumber) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($mentor->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PK Id') ?></th>
            <td><?= $this->Number->format($mentor->PK_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CreatedAt') ?></th>
            <td><?= h($mentor->createdAt) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ModifiedAt') ?></th>
            <td><?= h($mentor->modifiedAt) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('DeletedAt') ?></th>
            <td><?= h($mentor->deletedAt) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Image') ?></h4>
        <?= $this->Text->autoParagraph(h($mentor->image)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Skills') ?></h4>
        <?php if (!empty($mentor->skills)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('PK_ID') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($mentor->skills as $skills): ?>
            <tr>
                <td><?= h($skills->PK_id) ?></td>
                <td><?= h($skills->name) ?></td>
                <td><?= h($skills->description) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View Skill'), ['controller' => 'Skills', 'action' => 'view', $skills->PK_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MentorSkills', 'action' => 'delete',$mentor->PK_id, $skills->PK_id], ['confirm' => __('Are you sure you want to delete the link?')]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Subject $subject
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Mentor'), ['controller' => 'Mentors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Mentors'), ['controller' => 'Mentors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Skill'), ['controller' => 'Skills', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List skill'), ['controller' => 'Skills', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New users'), ['controller' => 'Users', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List active users'), ['controller' => 'Users', 'action' => 'index']) ?></li>         
        <li><?= $this->Html->link(__('List all users'), ['controller' => 'Users', 'action' => 'indexAll']) ?></li> 
        <li><?= $this->Html->link(__('New service'), ['controller' => 'Services', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List services'), ['controller' => 'Services', 'action' => 'index']) ?></li> 
        <li><?= $this->Html->link(__('New subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li> 
        <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout'], ['confirm' => __('Are you sure you want to Logout?')]) ?></li>

        <li><?= $this->Html->link(__('Edit Subject'), ['action' => 'edit', $subject->PK_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Subject'), ['action' => 'delete', $subject->PK_id], ['confirm' => __('Are you sure you want to delete # {0}?', $subject->PK_id)]) ?> </li>
    </ul>
</nav>
<div class="subjects view large-9 medium-8 columns content">
    <h3><?= h($subject->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($subject->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($subject->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PK Id') ?></th>
            <td><?= $this->Number->format($subject->PK_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CreatedAt') ?></th>
            <td><?= h($subject->createdAt) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ModifiedAt') ?></th>
            <td><?= h($subject->modifiedAt) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Books') ?></h4>
        <?php if (!empty($subject->books)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('PK_ID') ?></th>
                <th scope="col"><?= __('name') ?></th>
                <th scope="col"><?= __('author') ?></th>
                <th scope="col"><?= __('synopsis') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($subject->books as $books): ?>
            <tr>
                <td><?= h($books->PK_id) ?></td>
                <td><?= h($books->name) ?></td>
                <td><?= h($books->author) ?></td>
                <td><?= h($books->synopsis) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View Book'), ['controller' => 'Books', 'action' => 'view', $books->PK_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BookSubjects', 'action' => 'delete',$books->PK_id, $subject->PK_id], ['confirm' => __('Are you sure you want to delete the link?')]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Renting $renting
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $renting->PK_rent_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $renting->PK_rent_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Renting'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Books'), ['controller' => 'Books', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Book'), ['controller' => 'Books', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="renting form large-9 medium-8 columns content">
    <?= $this->Form->create($renting) ?>
    <fieldset>
        <legend><?= __('Edit Renting') ?></legend>
        <?php
            echo $this->Form->control('item_type');
            echo $this->Form->control('FK_item_id', ['options' => $books]);
            echo $this->Form->control('FK_user_id', ['options' => $users]);
            echo $this->Form->control('rent_beginDate');
            echo $this->Form->control('rent_endDate');
            echo $this->Form->control('rent_returnDate');
            echo $this->Form->control('fine');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Renting[]|\Cake\Collection\CollectionInterface $renting
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Renting'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Books'), ['controller' => 'Books', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Book'), ['controller' => 'Books', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="renting index large-9 medium-8 columns content">
    <h3><?= __('Renting') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('PK_rent_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('item_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('FK_item_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('FK_user_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rent_beginDate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rent_endDate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rent_returnDate') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($renting as $renting): ?>
            <tr>
                <td><?= $this->Number->format($renting->PK_rent_id) ?></td>
                <td><?= $this->Number->format($renting->item_type) ?></td>
                <td><?= $renting->has('mentor') ? $this->Html->link($renting->mentor->PK_id, ['controller' => 'Mentors', 'action' => 'view', $renting->mentor->PK_id]) : '' ?></td>
                <td><?= $renting->has('room') ? $this->Html->link($renting->room->PK_id, ['controller' => 'Rooms', 'action' => 'view', $renting->room->PK_id]) : '' ?></td>
                <td><?= $renting->has('book') ? $this->Html->link($renting->book->PK_id, ['controller' => 'Books', 'action' => 'view', $renting->book->PK_id]) : '' ?></td>
                <td><?= $renting->has('user') ? $this->Html->link($renting->user->PK_id, ['controller' => 'Users', 'action' => 'view', $renting->user->PK_id]) : '' ?></td>
                <td><?= h($renting->rent_beginDate) ?></td>
                <td><?= h($renting->rent_endDate) ?></td>
                <td><?= h($renting->rent_returnDate) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $renting->PK_rent_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $renting->PK_rent_id]) ?>
                    <?= $this->Html->link(__('Return'), ['action' => 'return', $renting->PK_rent_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $renting->PK_rent_id], ['confirm' => __('Are you sure you want to delete # {0}?', $renting->PK_rent_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Renting $renting
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Renting'), ['action' => 'edit', $renting->PK_rent_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Renting'), ['action' => 'delete', $renting->PK_rent_id], ['confirm' => __('Are you sure you want to delete # {0}?', $renting->PK_rent_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Renting'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Renting'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Books'), ['controller' => 'Books', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Book'), ['controller' => 'Books', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="renting view large-9 medium-8 columns content">
    <h3><?= h($renting->PK_rent_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Book') ?></th>
            <td><?= $renting->has('book') ? $this->Html->link($renting->book->PK_id, ['controller' => 'Books', 'action' => 'view', $renting->book->PK_id]) : '' ?></td>
            <th scope="row"><?= __('Room') ?></th>
            <td><?= $renting->has('room') ? $this->Html->link($renting->room->PK_id, ['controller' => 'Rooms', 'action' => 'view', $renting->room->PK_id]) : '' ?></td>
            <th scope="row"><?= __('Mentor') ?></th>
            <td><?= $renting->has('mentor') ? $this->Html->link($renting->mentor->PK_id, ['controller' => 'Mentors', 'action' => 'view', $renting->mentor->PK_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $renting->has('user') ? $this->Html->link($renting->user->PK_id, ['controller' => 'Users', 'action' => 'view', $renting->user->PK_id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rent BeginDate') ?></th>
            <td><?= h($renting->rent_beginDate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rent EndDate') ?></th>
            <td><?= h($renting->rent_endDate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rent ReturnDate') ?></th>
            <td><?= h($renting->rent_returnDate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PK Rent Id') ?></th>
            <td><?= $this->Number->format($renting->PK_rent_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Item Type') ?></th>
            <td><?= $this->Number->format($renting->item_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fine') ?></th>
            <td><?= $this->Number->format($renting->fine) ?></td>
        </tr>
    </table>
</div>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Service $service
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Mentor'), ['controller' => 'Mentors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Mentors'), ['controller' => 'Mentors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Skill'), ['controller' => 'Skills', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List skill'), ['controller' => 'Skills', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New users'), ['controller' => 'Users', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List active users'), ['controller' => 'Users', 'action' => 'index']) ?></li>         
        <li><?= $this->Html->link(__('List all users'), ['controller' => 'Users', 'action' => 'indexAll']) ?></li> 
        <li><?= $this->Html->link(__('New service'), ['controller' => 'Services', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List services'), ['controller' => 'Services', 'action' => 'index']) ?></li> 
        <li><?= $this->Html->link(__('New subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li> 
        <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout'], ['confirm' => __('Are you sure you want to Logout?')]) ?></li>
        
        <li><?= $this->Html->link(__('Edit Service'), ['action' => 'edit', $service->PK_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Service'), ['action' => 'delete', $service->PK_id], ['confirm' => __('Are you sure you want to delete # {0}?', $service->PK_id)]) ?> </li>

    </ul>
</nav>
<div class="services view large-9 medium-8 columns content">
    <h3><?= h($service->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($service->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($service->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PK Id') ?></th>
            <td><?= $this->Number->format($service->PK_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CreatedAt') ?></th>
            <td><?= h($service->createdAt) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ModifiedAt') ?></th>
            <td><?= h($service->modifiedAt) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Rooms') ?></h4>
        <?php if (!empty($service->rooms)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('PK_ID') ?></th>
                <th scope="col"><?= __('roomIdentifier') ?></th>
                <th scope="col"><?= __('location') ?></th>
                <th scope="col"><?= __('size') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($service->rooms as $rooms): ?>
            <tr>
                <td><?= h($rooms->PK_id) ?></td>
                <td><?= h($rooms->roomIdentifier) ?></td>
                <td><?= h($rooms->location) ?></td>
                <td><?= h($rooms->size) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View Rooms'), ['controller' => 'Rooms', 'action' => 'view', $rooms->PK_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'RoomServices', 'action' => 'delete',$rooms->PK_id, $service->PK_id], ['confirm' => __('Are you sure you want to delete the link?')]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

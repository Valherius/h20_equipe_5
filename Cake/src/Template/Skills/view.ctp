<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Skill $skill
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Mentor'), ['controller' => 'Mentors', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Mentors'), ['controller' => 'Mentors', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Skill'), ['controller' => 'Skills', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List skill'), ['controller' => 'Skills', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New users'), ['controller' => 'Users', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List active users'), ['controller' => 'Users', 'action' => 'index']) ?></li>         
        <li><?= $this->Html->link(__('List all users'), ['controller' => 'Users', 'action' => 'indexAll']) ?></li> 
        <li><?= $this->Html->link(__('New service'), ['controller' => 'Services', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List services'), ['controller' => 'Services', 'action' => 'index']) ?></li> 
        <li><?= $this->Html->link(__('New subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li> 
        <li><?= $this->Html->link(__('Logout'), ['controller' => 'Users', 'action' => 'logout'], ['confirm' => __('Are you sure you want to Logout?')]) ?></li>
        
        <li><?= $this->Html->link(__('Edit Skill'), ['action' => 'edit', $skill->PK_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Skill'), ['action' => 'delete', $skill->PK_id], ['confirm' => __('Are you sure you want to delete # {0}?', $skill->PK_id)]) ?> </li>

    </ul>
</nav>
<div class="skills view large-9 medium-8 columns content">
    <h3><?= h($skill->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($skill->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($skill->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PK Id') ?></th>
            <td><?= $this->Number->format($skill->PK_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CreatedAt') ?></th>
            <td><?= h($skill->createdAt) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ModifiedAt') ?></th>
            <td><?= h($skill->modifiedAt) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Mentors') ?></h4>
        <?php if (!empty($skill->mentors)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('PK_ID') ?></th>
                <th scope="col"><?= __('firstname') ?></th>
                <th scope="col"><?= __('lastname') ?></th>
                <th scope="col"><?= __('contactNumber') ?></th>
                <th scope="col"><?= __('email') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($skill->mentors as $mentors): ?>
            <tr>
                <td><?= h($mentors->PK_id) ?></td>
                <td><?= h($mentors->firstname) ?></td>
                <td><?= h($mentors->lastname) ?></td>
                <td><?= h($mentors->contactNumber) ?></td>
                <td><?= h($mentors->email) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View Mentor'), ['controller' => 'Mentors', 'action' => 'view', $mentors->PK_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'MentorSkills', 'action' => 'delete',$mentors->PK_id, $skill->PK_id], ['confirm' => __('Are you sure you want to delete the link?')]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>

</div>

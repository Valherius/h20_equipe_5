<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Book $book
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Book'), ['action' => 'edit', $book->PK_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Book'), ['action' => 'delete', $book->PK_id], ['confirm' => __('Are you sure you want to delete # {0}?', $book->PK_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Books'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Book'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('New subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li> 
    </ul>
</nav>
<div class="books view large-9 medium-8 columns content">
    <h3><?= h($book->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($book->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Author') ?></th>
            <td><?= h($book->author) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Synopsis') ?></th>
            <td><?= h($book->synopsis) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('PK Id') ?></th>
            <td><?= $this->Number->format($book->PK_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('CreatedAt') ?></th>
            <td><?= h($book->createdAt) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ModifiedAt') ?></th>
            <td><?= h($book->modifiedAt) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('DeletedAt') ?></th>
            <td><?= h($book->deletedAt) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Published Date') ?></th>
            <td><?= h($book->published_date) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Image') ?></h4>
        <?= $this->Text->autoParagraph(h($book->image)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Subjects') ?></h4>
        <?php if (!empty($book->subjects)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('PK_ID') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($book->subjects as $subjects): ?>
            <tr>
                <td><?= h($subjects->PK_id) ?></td>
                <td><?= h($subjects->name) ?></td>
                <td><?= h($subjects->description) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View Subject'), ['controller' => 'Subjects', 'action' => 'view', $subjects->PK_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BookSubjects', 'action' => 'delete',$book->PK_id, $subjects->PK_id], ['confirm' => __('Are you sure you want to delete the link?')]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>

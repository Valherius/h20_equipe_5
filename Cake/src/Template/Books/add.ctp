<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Book $book
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Books'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New subject'), ['controller' => 'Subjects', 'action' => 'add']) ?></li> 
        <li><?= $this->Html->link(__('List subjects'), ['controller' => 'Subjects', 'action' => 'index']) ?></li> 
    </ul>
</nav>
<div class="books form large-9 medium-8 columns content">
    <?= $this->Form->create($book) ?>
    <fieldset>
        <legend><?= __('Add Book') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('author');
            echo $this->Form->control('synopsis');
            echo $this->Form->control('image');
            echo $this->Form->control('createdAt', ['empty' => true]);
            echo $this->Form->control('modifiedAt', ['empty' => true]);
            echo $this->Form->control('deletedAt', ['empty' => true]);
            echo $this->Form->control('published_date');
            echo $this->Form->control('subjects._ids', ['options' => $subjects]);
            ?>
        
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

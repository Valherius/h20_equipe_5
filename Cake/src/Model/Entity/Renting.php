<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Renting Entity
 *
 * @property int $PK_rent_id
 * @property int $item_type
 * @property int $FK_item_id
 * @property int $FK_user_id
 * @property string $rent_beginDate
 * @property string $rent_endDate
 * @property string|null $rent_returnDate
 * @property float|null $fine
 *
 * @property \App\Model\Entity\Book $book
 * @property \App\Model\Entity\User $user
 */
class Renting extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'item_type' => true,
        'FK_item_id' => true,
        'FK_user_id' => true,
        'rent_beginDate' => true,
        'rent_endDate' => true,
        'rent_returnDate' => true,
        'is_paid' => true,
        'book' => true,
        'user' => true,        
        'mentor' => true,
        'room' => true,
    ];
}

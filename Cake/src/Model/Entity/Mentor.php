<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Mentor Entity
 *
 * @property int $PK_id
 * @property string $firstname
 * @property string $lastname
 * @property string|null $contactNumber
 * @property string $email
 * @property string $image
 * @property \Cake\I18n\FrozenTime|null $createdAt
 * @property \Cake\I18n\FrozenTime|null $modifiedAt
 * @property \Cake\I18n\FrozenTime|null $deletedAt
 */
class Mentor extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'firstname' => true,
        'lastname' => true,
        'contactNumber' => true,
        'email' => true,
        'image' => true,
        'createdAt' => true,
        'modifiedAt' => true,//to remove
        'deletedAt' => true,
        'skills' => true,
        
    ];
}

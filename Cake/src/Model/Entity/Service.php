<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Service Entity
 *
 * @property int $PK_id
 * @property string $name
 * @property string|null $description
 * @property \Cake\I18n\FrozenTime|null $createdAt
 * @property \Cake\I18n\FrozenTime|null $modifiedAt
 */
class Service extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'description' => true,
        'createdAt' => true,
        'modifiedAt' => true,
        'rooms' => true,
    ];
}

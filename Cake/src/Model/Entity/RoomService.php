<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RoomService Entity
 *
 * @property int $FK_room_id
 * @property int $FK_service_id
 *
 * @property \App\Model\Entity\Room $room
 * @property \App\Model\Entity\Service $service
 */
class RoomService extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'FK_room_id' => true,
        'FK_service_id' => true,
        'room' => true,
        'service' => true,
    ];
}

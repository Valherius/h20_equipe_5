<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Book Entity
 *
 * @property int $PK_id
 * @property string $name
 * @property string $author
 * @property string|null $synopsis
 * @property string $image
 * @property \Cake\I18n\FrozenTime|null $createdAt
 * @property \Cake\I18n\FrozenTime|null $modifiedAt
 * @property \Cake\I18n\FrozenTime|null $deletedAt
 * @property \Cake\I18n\FrozenTime|null $published_date
 */
class Book extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'author' => true,
        'synopsis' => true,
        'image' => true,
        'createdAt' => true,
        'modifiedAt' => true,
        'deletedAt' => true,
        'published_date' => true,
        'subjects' => true,
    ];
}

<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher; // Add this line
use Cake\ORM\Entity;
/**
 * User Entity
 *
 * @property int $PK_id
 * @property string $username
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property string $address
 * @property string $image
 * @property int $isAdmin
 * @property \Cake\I18n\FrozenTime|null $createdAt
 * @property \Cake\I18n\FrozenTime|null $modifiedAt
 * @property \Cake\I18n\FrozenTime|null $deletedAt
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'username' => true,
        'password' => true,
        'firstname' => true,
        'lastname' => true,
        'address' => true,
        'image' => true,
        'isAdmin' => true,
        'renting' => true,
        'createdAt' => true,
        'modifiedAt' => true,
        'deletedAt' => true,
    ];


    protected function _setPassword($value) 
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();

            return $hasher->hash($value);
        }
    }

    protected $_hidden = [
        'password',
    ];
}

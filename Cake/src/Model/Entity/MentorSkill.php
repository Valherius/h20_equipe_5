<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * MentorSkill Entity
 *
 * @property int $FK_mentor_id
 * @property int $FK_skill_id
 *
 * @property \App\Model\Entity\Mentor $mentor
 * @property \App\Model\Entity\Skill $skill
 */
class MentorSkill extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'FK_mentor_id' => true,
        'FK_skill_id' => true,
        'mentor' => true,
        'skill' => true,
    ];
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * BookSubject Entity
 *
 * @property int $FK_book_id
 * @property int $FK_subject_id
 *
 * @property \App\Model\Entity\Book $book
 * @property \App\Model\Entity\Subject $subject
 */
class BookSubject extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'FK_book_id' => true,
        'FK_subject_id' => true,
        'book' => true,
        'subject' => true,
    ];
}

<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Books Model
 *
 * @method \App\Model\Entity\Book get($primaryKey, $options = [])
 * @method \App\Model\Entity\Book newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Book[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Book|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Book saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Book patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Book[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Book findOrCreate($search, callable $callback = null, $options = [])
 */
class BooksTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('books');
        $this->setDisplayField('name');
        $this->setPrimaryKey('PK_id');

        $this->belongsToMany('Subjects', [
            'foreignKey' => 'FK_book_id',
            'targetForeignKey' => 'FK_subject_id',
            'joinTable' => 'book_subjects',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('PK_id')
            ->allowEmptyString('PK_id', null, 'create')
            ->add('PK_id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('name')
            ->maxLength('name', 45)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('author')
            ->maxLength('author', 45)
            ->requirePresence('author', 'create')
            ->notEmptyString('author');

        $validator
            ->scalar('synopsis')
            ->maxLength('synopsis', 500)
            ->allowEmptyString('synopsis');

        $validator
            ->scalar('image')
            ->maxLength('image', 4294967295)
            ->requirePresence('image', 'create')
            ->notEmptyFile('image');

        $validator
            ->dateTime('createdAt')
            ->allowEmptyDateTime('createdAt');

        $validator
            ->dateTime('modifiedAt')
            ->allowEmptyDateTime('modifiedAt');

        $validator
            ->dateTime('deletedAt')
            ->allowEmptyDateTime('deletedAt');

        $validator
            ->date('published_date')
            ->requirePresence('published_date', 'create')
            ->notEmptyDateTime('published_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['PK_id']));

        return $rules;
    }
}

<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Mentors Model
 *
 * @method \App\Model\Entity\Mentor get($primaryKey, $options = [])
 * @method \App\Model\Entity\Mentor newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Mentor[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Mentor|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mentor saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Mentor patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Mentor[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Mentor findOrCreate($search, callable $callback = null, $options = [])
 */
class MentorsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('mentors');
        $this->setDisplayField('lastname');
        $this->setPrimaryKey('PK_id');

        $this->belongsToMany('Skills', [
            'foreignKey' => 'FK_mentor_id',
            'targetForeignKey' => 'FK_skill_id',
            'joinTable' => 'mentor_skills',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('PK_id')
            ->allowEmptyString('PK_id', null, 'create')
            ->add('PK_id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 45)
            ->requirePresence('firstname', 'create')
            ->notEmptyString('firstname');

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 45)
            ->requirePresence('lastname', 'create')
            ->notEmptyString('lastname');

        $validator
            ->scalar('contactNumber')
            ->maxLength('contactNumber', 45)
            ->allowEmptyString('contactNumber');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmptyString('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('image')
            ->maxLength('image', 4294967295)
            ->requirePresence('image', 'create')
            ->notEmptyFile('image');

        $validator
            ->dateTime('createdAt')
            ->allowEmptyDateTime('createdAt');

        $validator
            ->dateTime('modifiedAt')
            ->allowEmptyDateTime('modifiedAt');

        $validator
            ->dateTime('deletedAt')
            ->allowEmptyDateTime('deletedAt');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['PK_id']));

        return $rules;
    }
}

<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rooms Model
 *
 * @method \App\Model\Entity\Room get($primaryKey, $options = [])
 * @method \App\Model\Entity\Room newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Room[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Room|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Room saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Room patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Room[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Room findOrCreate($search, callable $callback = null, $options = [])
 */
class RoomsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rooms');
        $this->setDisplayField('roomIdentifier');
        $this->setPrimaryKey('PK_id');

        $this->belongsToMany('Services', [
            'foreignKey' => 'FK_room_id',
            'targetForeignKey' => 'FK_service_id',
            'joinTable' => 'room_services',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('PK_id')
            ->allowEmptyString('PK_id', null, 'create')
            ->add('PK_id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('roomIdentifier')
            ->maxLength('roomIdentifier', 45)
            ->requirePresence('roomIdentifier', 'create')
            ->notEmptyString('roomIdentifier')
            ->add('roomIdentifier', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('location')
            ->maxLength('location', 45)
            ->requirePresence('location', 'create')
            ->notEmptyString('location');

        $validator
            ->integer('size')
            ->allowEmptyString('size');

        $validator
            ->scalar('image')
            ->maxLength('image', 4294967295)
            ->requirePresence('image', 'create')
            ->notEmptyFile('image');

        $validator
            ->dateTime('createdAt')
            ->allowEmptyDateTime('createdAt');

        $validator
            ->dateTime('modifiedAt')
            ->allowEmptyDateTime('modifiedAt');

        $validator
            ->dateTime('deletedAt')
            ->allowEmptyDateTime('deletedAt');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['PK_id']));
        $rules->add($rules->isUnique(['roomIdentifier']));

        return $rules;
    }
}

<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
/**
 * Renting Model
 *
 * @property \App\Model\Table\BooksTable&\Cake\ORM\Association\BelongsTo $Books
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Renting get($primaryKey, $options = [])
 * @method \App\Model\Entity\Renting newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Renting[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Renting|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Renting saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Renting patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Renting[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Renting findOrCreate($search, callable $callback = null, $options = [])
 */
class RentingTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('renting');
        $this->setDisplayField('PK_rent_id');
        $this->setPrimaryKey('PK_rent_id');


        $this->belongsTo('Mentors', [
            'foreignKey' => 'FK_item_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Rooms', [
            'foreignKey' => 'FK_item_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Books', [
            'foreignKey' => 'FK_item_id',
            'joinType' => 'INNER',
        ]);




        $this->belongsTo('Users', [
            'foreignKey' => 'FK_user_id',
            'joinType' => 'INNER',
        ]);
        
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {/*
        $validator
            ->nonNegativeInteger('PK_rent_id')
            ->allowEmptyString('PK_rent_id', null, 'create')
            ->add('PK_rent_id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('item_type')
            ->requirePresence('item_type', 'create')
            ->notEmptyString('item_type');

        $validator
            ->dateTime('rent_beginDate')
            ->requirePresence('rent_beginDate', 'create')
            ->notEmptyDateTime('rent_beginDate');

        $validator
            ->dateTime('rent_endDate')
            ->requirePresence('rent_endDate', 'create')
            ->notEmptyDateTime('rent_endDate');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['PK_rent_id']));
       
        $rules->add($rules->existsIn(['FK_user_id'], 'Users'));

       

        $rules->add(function ($entity, $options) use($rules) {
           
            if ($entity->item_type == '0') { // Mentor
                $rule = $rules->existsIn('FK_item_id', 'Mentors');        
                return $rule($entity, $options);
            }
            if($entity->item_type == '1') { // Room
 
                $rule = $rules->existsIn('FK_item_id', 'Rooms');        
                return $rule($entity, $options);
            }
            if($entity->item_type == '2') { // Books
   
                $rule = $rules->existsIn('FK_item_id', 'Books');        
                return $rule($entity, $options);
            }
        
            return false;
        }, 'ItemType');

        return $rules;
    }
}

<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RoomServices Model
 *
 * @property \App\Model\Table\RoomsTable&\Cake\ORM\Association\BelongsTo $Rooms
 * @property \App\Model\Table\ServicesTable&\Cake\ORM\Association\BelongsTo $Services
 *
 * @method \App\Model\Entity\RoomService get($primaryKey, $options = [])
 * @method \App\Model\Entity\RoomService newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RoomService[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RoomService|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RoomService saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RoomService patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RoomService[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RoomService findOrCreate($search, callable $callback = null, $options = [])
 */
class RoomServicesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('room_services');
        $this->setDisplayField('FK_room_id');
        $this->setPrimaryKey(['FK_room_id', 'FK_service_id']);

        $this->belongsTo('Rooms', [
            'foreignKey' => 'FK_room_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Services', [
            'foreignKey' => 'FK_service_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['FK_room_id'], 'Rooms'));
        $rules->add($rules->existsIn(['FK_service_id'], 'Services'));

        return $rules;
    }
}

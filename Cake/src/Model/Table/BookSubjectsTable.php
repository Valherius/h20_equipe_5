<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BookSubjects Model
 *
 * @property \App\Model\Table\BooksTable&\Cake\ORM\Association\BelongsTo $Books
 * @property \App\Model\Table\SubjectsTable&\Cake\ORM\Association\BelongsTo $Subjects
 *
 * @method \App\Model\Entity\BookSubject get($primaryKey, $options = [])
 * @method \App\Model\Entity\BookSubject newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BookSubject[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BookSubject|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BookSubject saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BookSubject patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BookSubject[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BookSubject findOrCreate($search, callable $callback = null, $options = [])
 */
class BookSubjectsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('book_subjects');
        $this->setDisplayField('FK_book_id');
        $this->setPrimaryKey(['FK_book_id', 'FK_subject_id']);

        $this->belongsTo('Books', [
            'foreignKey' => 'FK_book_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Subjects', [
            'foreignKey' => 'FK_subject_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['FK_book_id'], 'Books'));
        $rules->add($rules->existsIn(['FK_subject_id'], 'Subjects'));

        return $rules;
    }
}

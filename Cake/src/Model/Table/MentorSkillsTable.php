<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * MentorSkills Model
 *
 * @property \App\Model\Table\MentorsTable&\Cake\ORM\Association\BelongsTo $Mentors
 * @property \App\Model\Table\SkillsTable&\Cake\ORM\Association\BelongsTo $Skills
 *
 * @method \App\Model\Entity\MentorSkill get($primaryKey, $options = [])
 * @method \App\Model\Entity\MentorSkill newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\MentorSkill[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\MentorSkill|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MentorSkill saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\MentorSkill patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\MentorSkill[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\MentorSkill findOrCreate($search, callable $callback = null, $options = [])
 */
class MentorSkillsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('mentor_skills');
        $this->setDisplayField('FK_mentor_id');
        $this->setPrimaryKey(['FK_mentor_id', 'FK_skill_id']);

        $this->belongsTo('Mentors', [
            'foreignKey' => 'FK_mentor_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Skills', [
            'foreignKey' => 'FK_skill_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['FK_mentor_id'], 'Mentors'));
        $rules->add($rules->existsIn(['FK_skill_id'], 'Skills'));

        return $rules;
    }
}

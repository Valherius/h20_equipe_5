<?php
namespace App\Controller;
use \Datetime;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;


class SubjectsController extends AppController
{  
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['books']);
        $this->loadComponent('RequestHandler');
    }

    public function index()
    {
        $recherche = $this->request->getData('searchData');
        //dd($recherche);
        if ($recherche!='') {
            $subjects = $this->Subjects->find()
                ->where(['OR' => ["name LIKE :searchlike", "description LIKE :searchlike", "MATCH(name, description) AGAINST(:searchbool IN BOOLEAN MODE)"]])
                ->order(['name' => 'ASC'])
                ->bind(':searchbool', $recherche)
                ->bind(':searchlike', '%' . $recherche . '%');
        } else 
        {
            $subjects = $this->Subjects->find('all');
        }
            $this->set(compact('subjects'));
            $this->set('_serialize', ['subjects']);    
       
        }
        

    public function view($id = null)
    {
        $subject = $this->Subjects->get($id, [
            'contain' => ['Books'],
        ]);

        $this->set('subject', $subject);
        $this->set('_serialize', ['subject']); 
    }

    public function add()
    {
        $subject = $this->Subjects->newEntity();
        if ($this->request->is('post')) {
            $subject = $this->Subjects->patchEntity($subject, $this->request->getData());
            $subject->createdAt=new DateTime('now');
            if ($this->Subjects->save($subject)) {
                $subject="SC1";
              
            }
            else{
                $subject="EC1";
                $this->Flash->error(__('The subject could not be saved. Please, try again.'));
          
            }
        }
        $this->set(compact('subject'));
        $this->set('_serialize', ['subject']); 
    }

    public function edit($id = null)
    {
        $subject = $this->Subjects->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $subject = $this->Subjects->patchEntity($subject, $this->request->getData());
            $subject->modifiedAt=new DateTime('now');
            if ($this->Subjects->save($subject)) {
                $subject="SE1";
               
            }
            else{
                $subject="EE1";
            }
        }
        $this->set(compact('subject'));
        $this->set('_serialize', ['subject']); 
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subject = $this->Subjects->get($id);
        if ($this->Subjects->delete($subject)) {
            $subject="SD1";
        }else 
        {
            $subject="ED1";
        }
        $this->set(compact('subject'));
        $this->set('_serialize', ['subject']); 
       
    }
}

<?php
namespace App\Controller;
use \Datetime;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;


class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['login', 'add']);
        $this->loadComponent('RequestHandler');
    }
 

    public function index()
    {

        $recherche = $this->request->getData('searchData');
        //dd($recherche);
        if($recherche !='')
        {
            $users = $this->Users->find()
                ->where(['OR'=> ["username LIKE :searchlike", "firstname LIKE :searchlike",
                "lastname LIKE :searchlike",
                 "MATCH(username,firstname,lastname) AGAINST(:searchbool IN BOOLEAN MODE)"]])
                ->order(['username '=> 'ASC'])
                ->bind(":searchbool", $recherche)
                ->bind(':searchlike', '%' . $recherche . '%');
        }else {
          

            $query = TableRegistry::getTableLocator()->get('Users');
            $users = $query
            ->find()
            ->where(['deletedAt IS' => null])
            ->order(['username'=>'ASC'])
            ->all();
        }
        $this->set(compact('users'));
        $this->set('_serialize', ['users']); 
    }


    public function indexAll()
    {
        $keyword = $this->request->query('keyword');

        if(!empty($keyword)){
        $users = $this->Users->find('all',[
            'conditions'=> array('OR'=>array(
            array('firstname LIKE'=>'%'.$keyword.'%'),
            array('lastname LIKE'=>'%'.$keyword.'%'),
            array('username LIKE'=>'%'.$keyword.'%'),))
        ]);
        }else
        {
            $users = $this->Users->find('all');
        }
        $this->set(compact('users'));
        $this->set('_serialize', ['users']); 
    }


    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }


    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            //dd($this->request->getData()); die;
            $user = $this->Users->patchEntity($user, $this->request->getData());

            $user->createdAt=new DateTime('now');

            $fileName=$this->request->data['image']['tmp_name'];
            $fileStream=fopen($fileName,"r");
            $fileData=fread ($fileStream, filesize($fileName));
            $fileEncoded= base64_encode($fileData);
            
           // $fileEncoded=str_replace('data:image/jpeg;base64,', '', $fileEncoded);
            $user->image=$fileEncoded;

            if ($this->Users->save($user)) {
                $user="SC1";
                //return $this->redirect(['action' => 'index']);
            }else
            {
                $user="EC1";
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']); 
    }


    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->modifiedAt=new DateTime('now');

            if ($this->Users->save($user)) {
                $user="SE1";
                //return $this->redirect(['action' => 'index']);
            }else
            {
                $user="EE1";
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']); 
    }


    public function delete($id = null)
    {   
        $user = $this->Users->get($id, [
        'contain' => [],
    ]);
    if ($this->request->is(['patch', 'post', 'put'])) {
        $user = $this->Users->patchEntity($user, $this->request->getData());
        $user->deletedAt=new DateTime('now');
        if ($this->Users->save($user)) {
            $user="SD1";
           // return $this->redirect(['action' => 'index']);
        }else
        {
            $user="ED1";
        }
    }
    $this->set(compact('user'));
    $this->set('_serialize', ['user']); 
    }

    public function login()
    {
        if ($this->request->is('post')) {
                $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $this->set(compact('user'));
                $this->set('_serialize', ['user']);
                //return $this->redirect(['action' => 'index']);
            }else
            {
                $isAdmin=34;
                $this->set(compact('isAdmin'));
                $this->set('_serialize', ['isAdmin']);
            }
            //$this->Flash->error('Your username or password is incorrect.');
        }
    }
     
    public function isAuthorized($user)
    {        
        // Admin peut accéder à toute action
    if (isset($user['isAdmin']) && $user['isAdmin'] === 1) {
        return true;
    }
        // By default deny access.
        return false;
    }

    public function logout()
    {
        $this->Flash->success('You are now logged out.');
        return $this->redirect($this->Auth->logout());
    }
}

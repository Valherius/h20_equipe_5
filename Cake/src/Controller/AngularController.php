<?php

namespace App\Controller;
use App\Controller\AppController;



class AngularController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['show']);
        $this->loadComponent('RequestHandler');
    }

    public function show()
    {
        try {
            return $this->response->withType('text/html')->withFile(WWW_ROOT.'index.html');
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception; // Si le DEBUG est actif
            }
            throw new NotFoundException();
        }

    }
}

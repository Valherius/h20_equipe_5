<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;


//TODO Annule toutes les redirections quand un json est demandé, permet a Angular de recevoir une réponse bien formaté plutôt qu'un code HTTP


class AppController extends Controller
{

    public function beforeRedirect($event, $url, $response){
        // Intercept la redirection seulement si on est authentifier ET la requete est en json
        // SI c'est angular qui cause un redirect
          if($this->request->is('json')){
            $authenticated = $this->Auth->user('id') != null;
            // Si je suis authentifier
            if($authenticated) {
                //TODO Il ne devrait pas y avoir de redirection dans les contrôleurs
                $this->setResponse($this->response->withStatus(500) );// Si un controlleur de l'api implémente un redirect on lève une erreur critique
            }else{
                //TODO Change le status code de la réponse pour un unauthorized et change le contenu de la requete selon la convention
                $this->setResponse($this->response->withStatus(403)->withType('application/json')->withStringBody(json_encode(['authenticated' => $authenticated])));
            }
        }
    }
    

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);

        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [            
            'authorize' => 'Controller',
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'unauthorizedRedirect' => $this->referer()
        ]);
        $this->Auth->deny();
    }

   public function isAuthorized($user)
    {
      
      
        $action = $this->request->getParam('action');
      
        // The add and tags actions are always allowed to logged in users.
        if(isset($user['isAdmin']) && $user['isAdmin'] === 1){
            

                return true;

        }else
        { 
            if (in_array($action, ['index'])) {
                return true;
            }else{

                return false;
            }
        }
//dd($action);

    }
    /*
    public function isAuthorized($user)
    {        
        // Admin peut accéder à toute action
    if (isset($user['isAdmin']) && $user['isAdmin'] === 1) {
        return true;
    }
        // By default deny access.
        return false;
    }*/
}

<?php
namespace App\Controller;
use \Datetime;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Rooms Controller
 *
 * @property \App\Model\Table\RoomsTable $Rooms
 *
 * @method \App\Model\Entity\Room[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RoomsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['services']);
        
        $this->loadComponent('RequestHandler');
    }

    public function index()
    {
        $recherche = $this->request->getData('searchData');

        if ($recherche!='') {
            $rooms = $this->Rooms->find()
            ->where(['OR' => ["roomIdentifier LIKE :searchlike", "location LIKE :searchlike", 
                "MATCH(roomIdentifier,location) AGAINST(:searchbool IN BOOLEAN MODE)"]])
            ->order(['roomIdentifier' => 'ASC'])
            ->bind(':searchbool', $recherche)
            ->bind(':searchlike', '%' . $recherche . '%');
        } else {
            $query = TableRegistry::getTableLocator()->get('Rooms');
            $rooms = $query
            ->find()
            ->where(['deletedAt IS' => null])
            ->order(['roomIdentifier' => 'ASC'])
            ->all();
        }
        
        $this->set(compact('rooms'));
        $this->set('_serialize', ['rooms']); 
    }


    public function view($id = null)
    {
        $room = $this->Rooms->get($id, [
            'contain' => ['Services'],
        ]);

        $this->set('room', $room);
        $this->set('_serialize', ['room']); 
    }


    public function add()
    {
        $room = $this->Rooms->newEntity();

        if ($this->request->is('post')) {
            $room = $this->Rooms->patchEntity($room, $this->request->getData());
            $room->createdAt=new DateTime('now');
            $fileName=$this->request->data['image']['tmp_name'];
            $fileStream=fopen($fileName,"r");
            $fileData=fread($fileStream, filesize($fileName));
            $fileEncoded= base64_encode($fileData);
            
            $room->image=$fileEncoded;
            
            $result=$this->Rooms->save($room);

            if ($result) {
                $room=$result->PK_id;
            }
            else{
                $room="EC1";
            }
        }
        
        $services = $this->Rooms->Services->find('list');
        $this->set(compact('room', 'services'));
        $this->set('_serialize', ['room']); 
        
    }


    public function edit($id = null)
    {
        $room = $this->Rooms->get($id, [
            'contain' => ['Services'],
        ]);

        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $room = $this->Rooms->patchEntity($room, $this->request->getData());
            $room->modifiedAt=new DateTime('now');
            
            if ($this->Rooms->save($room)) {
                
                $room="SE1";
                
            }
            else {
                $room="EE1";
            }
        }
        $services = $this->Rooms->Services->find('list');
        $this->set(compact('room', 'services'));
        $this->set('_serialize', ['room']); 
    }


    public function delete($id = null)
    {
        $room = $this->Rooms->get($id, [
            'contain' => [],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $room = $this->Rooms->patchEntity($room, $this->request->getData());
            $room->deletedAt=new DateTime('now');
            if ($this->Rooms->save($room)) 
            {
                $room="SD1";
            }else{
                $room="ED1";
            }
        }
        $this->set(compact('room'));
        $this->set('_serialize', ['room']);
    }



    
    public function rentList()
    {
            $query = TableRegistry::getTableLocator()->get('Rooms');
            $items = $query
            ->find()
            ->select(['PK_id', 'name'=>'roomIdentifier', 'info1'=>'size', 'info2'=>'location'])
            ->where(['deletedAt IS' => null])
            ->order(['roomIdentifier'=>'ASC'])
            ->all();
            
        $this->set(compact('items'));
        $this->set('_serialize', ['items']); 
    }
}

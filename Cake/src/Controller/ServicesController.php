<?php
namespace App\Controller;
use \Datetime;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class ServicesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['rooms']);
        $this->loadComponent('RequestHandler');
    }

    public function index()
    {
        $recherche = $this->request->getData('searchData');
        if($recherche !='')
        {
            $services = $this->Services->find()
            ->where(['OR' => ["name LIKE :searchlike", "description LIKE :searchlike", "MATCH(name, description) AGAINST(:searchbool IN BOOLEAN MODE)"]])
            ->order(['name' => 'ASC'])
            ->bind(':searchbool', $recherche)
            ->bind(':searchlike', '%' . $recherche . '%');
        }else {
            $services = $this->Services->find('all');
        }

        $this->set(compact('services'));
        $this->set('_serialize', ['services']); 
    }

    public function view($id = null)
    {
        $service = $this->Services->get($id, [
            'contain' => ['Rooms'],
        ]);
        $this->set('service', $service);
        $this->set('_serialize', ['service']); 
    }

    public function add()
    {
        $service = $this->Services->newEntity();
        if ($this->request->is('post')) {
            $service = $this->Services->patchEntity($service, $this->request->getData());
            $service->createdAt=new DateTime('now');
            if ($this->Services->save($service)) {
                $service="SC1";
            }
            else{
                $service="EC1";
            }
        }
        $this->set(compact('service'));
        $this->set('_serialize', ['service']); 
    }

    public function edit($id = null)
    {
        $service = $this->Services->get($id, [
            'contain' => ['Rooms'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $service = $this->Services->patchEntity($service, $this->request->getData());
            $service->modifiedAt=new DateTime('now');
            if ($this->Services->save($service)) {
                $service="SE1";
            }else
            {
                $service="EE1";
            }
        }
        $this->set(compact('service'));
        $this->set('_serialize', ['service']); 
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $service = $this->Services->get($id);
        if ($this->Services->delete($service)) {
            $service="SD1";
        }else 
        {
            $service="ED1";
        }
        $this->set(compact('service'));
        $this->set('_serialize', ['service']); 
    }
}

<?php
namespace App\Controller;
use \Datetime;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class SkillsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['mentors']);
        $this->loadComponent('RequestHandler');
    }

    public function index()
    {
        $recherche = $this->request->getData('searchData');
        //dd($recherche);
        if ($recherche!='') {
            $skills = $this->Skills->find()
                ->where(['OR' => ["name LIKE :searchlike", "description LIKE :searchlike", "MATCH(name, description) AGAINST(:searchbool IN BOOLEAN MODE)"]])
                ->order(['name' => 'ASC'])
                ->bind(':searchbool', $recherche)
                ->bind(':searchlike', '%' . $recherche . '%');
        } else 
        {
            $skills = $this->Skills->find('all', array(
                'order' => 'name ASC'));
        }
        
        if($skills)
        {
            $this->set(compact('skills'));
            $this->set('_serialize', ['skills']);    
        }else
        {
            $skills ='EC1';
            $this->set(compact('skills'));
            $this->set('_serialize', ['skills']);   
        }

    }

    public function view($id = null)
    {
        $skill = $this->Skills->get($id, [
            'contain' => ['Mentors'],
        ]);
        $this->set('skill', $skill);
        $this->set('_serialize', ['skill']); 
    }

    public function add()
    {
        $skill = $this->Skills->newEntity();
        if ($this->request->is('post')) {
            $skill = $this->Skills->patchEntity($skill, $this->request->getData());
            $skill->createdAt=new DateTime('now');
            if ($this->Skills->save($skill)) {
                $skills ='SC1';
               // return $this->redirect(['action' => 'index']);
            }else
            {
                $skills ='EC1';
            }
        }
        $this->set(compact('skill'));
        $this->set('_serialize', ['skill']); 
    }

    public function edit($id = null)
    {
        $skill = $this->Skills->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $skill = $this->Skills->patchEntity($skill, $this->request->getData());
            $skill->modifiedAt=new DateTime('now');
            if ($this->Skills->save($skill)) {
                $skill ='SE1';
               //return $this->redirect(['action' => 'index']);
            }else
            {
                $skill ='EE1';
            }
        }
        $this->set(compact('skill'));        
        $this->set('_serialize', ['skill']); 
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $skill = $this->Skills->get($id);
        if ($this->Skills->delete($skill)) 
        {
            $skill ='SD1';
        }else 
        {
            $skill ='ED1';
        }
        $this->set(compact('skill'));        
        $this->set('_serialize', ['skill']); 
       // return $this->redirect(['action' => 'index']);
    }
}

<?php
namespace App\Controller;
use \Datetime;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class MentorsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['skills']);
        $this->loadComponent('RequestHandler');
    }

    public function index()
    {
        $recherche = $this->request->getData('searchData');
        //dd($recherche);
        if ($recherche!='') {
            $mentors = $this->Mentors->find()
                ->where(['OR' => ["firstname LIKE :searchlike", "lastname LIKE :searchlike",
                 "email LIKE :searchlike", 
                 "MATCH(firstname, lastname, email) AGAINST(:searchbool IN BOOLEAN MODE)"]])
                ->order(['lastname' => 'ASC'])
                ->bind(':searchbool', $recherche)
                ->bind(':searchlike', '%' . $recherche . '%');
        } 
        else 
        {
            $query = TableRegistry::getTableLocator()->get('Mentors');
            $mentors = $query
            ->find()
            ->where(['deletedAt IS' => null])
            ->order(['lastname'=>'ASC'])
            ->all();
        }
        $this->set(compact('mentors'));
        $this->set('_serialize', ['mentors']); 
    }

    public function view($id = null)
    {
        $mentor = $this->Mentors->get($id, [
            'contain' => ['Skills'],
        ]);
        $this->set('mentor', $mentor);
        $this->set('_serialize', ['mentor']); 
    }

    public function add()
    {
        $mentor = $this->Mentors->newEntity();
        if ($this->request->is('post')) {
   
            $mentor = $this->Mentors->patchEntity($mentor, $this->request->getData());            
            $mentor->createdAt=new DateTime('now');

            $fileName=$this->request->data['image']['tmp_name'];
            $fileStream=fopen($fileName,"r");
            $fileData=fread ($fileStream, filesize($fileName));
            $fileEncoded= base64_encode($fileData);
            
            $mentor->image=$fileEncoded;

            $result=$this->Mentors->save($mentor);
            if ($result) {
                $mentor="SC1";
            }else
            {
                $mentor="EC1";
            }
        }
        $skills = $this->Mentors->Skills->find('list');
        $this->set(compact('mentor', 'skills'));
        $this->set('_serialize', ['mentor']); 
    }

    public function edit($id = null)
    {
        $mentor = $this->Mentors->get($id, [
            'contain' => ['Skills'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mentor = $this->Mentors->patchEntity($mentor, $this->request->getData());
            $mentor->modifiedAt=new DateTime('now');
            
            if ($this->Mentors->save($mentor)) {
                $mentor="SE1";
            }else
            {
                $mentor="EE1";
            }
        }
        $skills = $this->Mentors->Skills->find('list');
        $this->set(compact('mentor', 'skills'));
        $this->set('_serialize', ['mentor']); 
    }

    public function delete($id = null)
    {
        $mentor = $this->Mentors->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mentor = $this->Mentors->patchEntity($mentor, $this->request->getData());
            $mentor->deletedAt=new DateTime('now');
            if ($this->Mentors->save($mentor)) {
               $mentor="SD1";
            }else{
                $mentor="ED1";
            }
        }
        $this->set(compact('mentor'));
        $this->set('_serialize', ['mentor']); 
    }


    public function rentList()
    {
            $query = TableRegistry::getTableLocator()->get('Mentors');
            $items = $query
            ->find()
            ->select(['PK_id', 'name'=>'fullname', 'info1'=>'email', 'info2'=>'contactNumber'])
            ->where(['deletedAt IS' => null])
            ->order(['fullname'=>'ASC'])
            ->all();
            
        $this->set(compact('items'));
        $this->set('_serialize', ['items']); 
    }
}


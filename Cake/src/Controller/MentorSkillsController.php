<?php
namespace App\Controller;
use \Datetime;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class MentorSkillsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function index()
    {
        $mentorSkills = $this->MentorSkills->find('all')
        ->contain(['Mentors', 'Skills']);

        $this->set(compact('mentorSkills'));
        $this->set('_serialize', ['mentorSkills']); 
    }

    public function view($id = null)
    {
        $mentorSkill = $this->MentorSkills->get($id, [
            'contain' => ['Mentors', 'Skills'],
        ]);
        $this->set('mentorSkill', $mentorSkill);
        $this->set('_serialize', ['mentorSkill']);
    }

    public function add()
    {
        $mentorSkill = $this->MentorSkills->newEntity();
        if ($this->request->is('post')) {
            $mentorSkill = $this->MentorSkills->patchEntity($mentorSkill, $this->request->getData());
            if ($this->MentorSkills->save($mentorSkill)) {
                $mentorSkill="SC1";
            }
            else{
                $mentorSkill="EC1";
            }
        }
        $mentors = $this->MentorSkills->Mentors->find('list');
        $skills = $this->MentorSkills->Skills->find('list');
        $this->set(compact('mentorSkill', 'mentors', 'skills'));
        $this->set('_serialize', ['mentorSkill']);
    }

    public function edit($id = null)
    {
        $mentorSkill = $this->MentorSkills->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $mentorSkill = $this->MentorSkills->patchEntity($mentorSkill, $this->request->getData());
            if ($this->MentorSkills->save($mentorSkill)) {
                $mentorSkill="SE1";
            }
            else{
                $mentorSkill="EE1";
            }
        }
        $mentors = $this->MentorSkills->Mentors->find('list');
        $skills = $this->MentorSkills->Skills->find('list');
        $this->set(compact('mentorSkill', 'mentors', 'skills'));
        $this->set('_serialize', ['mentorSkill']);
    }


    public function delete($mentorid = null, $skillid = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $query = TableRegistry::getTableLocator()->get('MentorSkills');
        $mentorSkill = $query
        ->find()
        ->where(['FK_mentor_id' => $mentorid,'FK_skill_id'=>$skillid])
        ->first();

        if ($this->MentorSkills->delete($mentorSkill)) {
            $mentorSkill="SD1";
        }else
        {
            $mentorSkill="ED1";
        }
        $this->set(compact('mentorSkill'));
        $this->set('_serialize', ['mentorSkill']);
    }
}

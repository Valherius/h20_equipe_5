<?php
namespace App\Controller;
use \Datetime;
use Cake\I18n\Time;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;

class ReportsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }
    
    public function ReportMentor()
    {
        if ($this->request->is('post')) {
   
            $start_date=$this->request->getData('start');
            
            $end_date=$this->request->getData('end');
    
            $this->connection = ConnectionManager::get('default');
            $results=$this->connection->execute("CALL `ReportMentor`('$start_date','$end_date')")->fetchAll('assoc');
        }        
        $this->set(compact('results'));
        $this->set('_serialize', ['results']);
    }

    public function ReportBook()
    {
        if ($this->request->is('post')) {
   
            $start_date=$this->request->getData('start');
            $end_date=$this->request->getData('end');
            $this->connection = ConnectionManager::get('default');
            $results=$this->connection->execute("CALL `ReportBook`('$start_date','$end_date')")->fetchAll('assoc');

        }        
        $this->set(compact('results'));
        $this->set('_serialize', ['results']);
    }

    public function ReportRoom()
    {
        if ($this->request->is('post')) {
   
            $start_date=$this->request->getData('start');
            
            $end_date=$this->request->getData('end');

            $this->connection = ConnectionManager::get('default');
            $results=$this->connection->execute("CALL `ReportRoom`('$start_date','$end_date')")->fetchAll('assoc');

        }        
        $this->set(compact('results'));
        $this->set('_serialize', ['results']);
    }
}

<?php
namespace App\Controller;
use \Datetime;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;

/**
 * BookSubjects Controller
 *
 * @property \App\Model\Table\BookSubjectsTable $BookSubjects
 *
 * @method \App\Model\Entity\BookSubject[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BookSubjectsController extends AppController
{
    public function initialize()
    {
        parent::initialize();       
        $this->loadComponent('RequestHandler');
    }
    
    public function index()
    {
        $bookSubjects = $this->BookSubjects->find('all')
        ->contain(['Books', 'Subjects']);

        $this->set(compact('bookSubjects'));
        $this->set('_serialize', ['bookSubjects']); 
    }

    /**
     * View method
     *
     * @param string|null $id Book Subject id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bookSubject = $this->BookSubjects->get($id, [
            'contain' => ['Books', 'Subjects'],
        ]);

        $this->set('bookSubject', $bookSubject);
        $this->set('_serialize', ['bookSubject']); 
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bookSubject = $this->BookSubjects->newEntity();
        if ($this->request->is('post')) {
            $bookSubject = $this->BookSubjects->patchEntity($bookSubject, $this->request->getData());
            if ($this->BookSubjects->save($bookSubject)) {
                $bookSubject="SC1";
            }
            else{
                $bookSubject="EC1";
            }
            
        $books = $this->BookSubjects->Books->find('list');
        $subjects = $this->BookSubjects->Subjects->find('list');
        $this->set(compact('bookSubject', 'books', 'subjects'));
        $this->set('_serialize', ['bookSubject']); 
        }
    }

    /**
     * Edit method
     *
     * @param string|null $id Book Subject id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bookSubject = $this->BookSubjects->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bookSubject = $this->BookSubjects->patchEntity($bookSubject, $this->request->getData());
            if ($this->BookSubjects->save($bookSubject)) {
                $bookSubject="SE1";
            }
            else{
                $bookSubject="EE1";
            }
           
        }
        $books = $this->BookSubjects->Books->find('list');
        $subjects = $this->BookSubjects->Subjects->find('list');
        $this->set(compact('bookSubject', 'books', 'subjects'));
        $this->set('_serialize', ['bookSubject']); 
    }

    /**
     * Delete method
     *
     * @param string|null $id Book Subject id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($bookid = null,$subjectid = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $query = TableRegistry::getTableLocator()->get('BookSubjects');
        $bookSubject = $query
        ->find()
        ->where(['FK_book_id' => $bookid,'FK_subject_id'=>$subjectid])
        ->first();

        if ($this->BookSubjects->delete($bookSubject)) {
            $bookSubject="SD1";
        } else {
            $bookSubject="ED1";
        }

        $this->set(compact('bookSubject'));
        $this->set('_serialize', ['bookSubject']); 
    }
}

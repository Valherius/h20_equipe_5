<?php
namespace App\Controller;

use \Datetime;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;


class BooksController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['subjects']);       
        $this->loadComponent('RequestHandler');
    }

    //////////////////////////////////////////
    public function index()
    { 
        $recherche = $this->request->getData('searchData');
        
        if ($recherche!='') {
            $books = $this->Books->find()
                ->where(['OR' => ["name LIKE :searchlike", "author LIKE :searchlike",
                 "synopsis LIKE :searchlike", 
                 "MATCH(name, author, synopsis) AGAINST(:searchbool IN BOOLEAN MODE)"]])
                ->order(['name' => 'ASC'])
                ->bind(':searchbool', $recherche)
                ->bind(':searchlike', '%' . $recherche . '%');
        } else {

         
      // Si aucun parametre, on fait une liste complete sans recherche      
        $query = TableRegistry::getTableLocator()->get('Books');
        $books = $query
        ->find()
        ->where(['deletedAt IS' => null])
        ->order(['name' => 'ASC'])
        ->all();
    }
       

        $this->set(compact('books'));
        $this->set('_serialize', ['books']); 
    }

    ///////////////////////////////////////////////////

    
    public function view($id = null)
    {
        $book = $this->Books->get($id, [
            'contain' => ['Subjects'],
        ]);

        $this->set('book', $book);
        $this->set('_serialize', ['book']); 
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $book = $this->Books->newEntity();
        if ($this->request->is('post')) {
            $book =$this->Books->patchEntity($book, $this->request->getData());
            $book->published_date=new DateTime('now');
          
            $fileName=$this->request->data['image']['tmp_name'];
            $fileStream=fopen($fileName,"r");
            $fileData=fread ($fileStream, filesize($fileName));
            $fileEncoded= base64_encode($fileData);
            
            $book->image=$fileEncoded;


            if ($this->Books->save($book)) {
                $book="SC1";
            }
            else
            {
                $book="EC1";
            }
           
        }
        $subjects = $this->Books->Subjects->find('list');
        $this->set(compact('book','subjects'));
        $this->set('_serialize', ['book']); 
    }

    
    public function edit($id = null)
    {
        $book = $this->Books->get($id, [
            'contain' => ['Subjects'],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $book = $this->Books->patchEntity($book, $this->request->getData());   
            $book->modifiedAt=new DateTime('now');

            if ($this->Books->save($book)) {
                $book="SE1";
            } else{
                $error=$book->errors();
                $book="EE1";
            }
        }
        $subjects = $this->Books->Subjects->find('list');
        $this->set(compact('book','subjects','error'));
        $this->set('_serialize', ['book','error']); 
    }
    

    /**
     * Delete method
     *
     * @param string|null $id Book id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $book = $this->Books->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $book = $this->Books->patchEntity($book, $this->request->getData());
            $book->deletedAt=new DateTime('now');
        
            if ($this->Books->save($book)) {
                $book="SD1";
            }else{
                $book="ED1";
            }
        }
        $this->set(compact('book'));
        $this->set('_serialize', ['book']); 
    }



    public function rentList()
    {
            $query = TableRegistry::getTableLocator()->get('Books');
            $items = $query
            ->find()
            ->select(['PK_id', 'name', 'info1'=>'author', 'info2'=>'published_date'])
            ->where(['deletedAt IS' => null])
            ->order(['name'=>'ASC'])
            ->all();
            
        $this->set(compact('items'));
        $this->set('_serialize', ['items']); 
    }
}

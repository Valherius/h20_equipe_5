<?php
namespace App\Controller;
use \Datetime;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class RoomServicesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        
        $this->loadComponent('RequestHandler');
    }

    public function index()
    {
        $roomServices = $this->RoomServices->find('all')
        ->contain(['Rooms', 'Services']);
        /*$this->paginate = [
            'contain' => ['Rooms', 'Services'],
        ];
        $roomServices = $this->paginate($this->RoomServices);*/

        $this->set(compact('roomServices'));
        $this->set('_serialize', ['roomServices']);
    }

    /**
     * View method
     *
     * @param string|null $id Room Service id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $roomService = $this->RoomServices->get($id, [
            'contain' => ['Rooms', 'Services'],
        ]);

        $this->set('roomService', $roomService);
        $this->set('_serialize', ['roomService']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $roomService = $this->RoomServices->newEntity();
        if ($this->request->is('post')) {
            $roomService = $this->RoomServices->patchEntity($roomService, $this->request->getData());
            if ($this->RoomServices->save($roomService)) {
                $roomService="SC1";
            }
            else{
                $roomService="EC1";
            }
        }
        $rooms = $this->RoomServices->Rooms->find('list', ['limit' => 200]);
        $services = $this->RoomServices->Services->find('list', ['limit' => 200]);
        $this->set(compact('roomService', 'rooms', 'services'));
        $this->set('_serialize', ['roomService']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Room Service id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $roomService = $this->RoomServices->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $roomService = $this->RoomServices->patchEntity($roomService, $this->request->getData());
            if ($this->RoomServices->save($roomService)) {
                //$this->Flash->success(__('The room service has been saved.'));
                $roomService="SE1";
                //return $this->redirect(['action' => 'index']);
            }
            else{
                $roomService="EE1";
                //$this->Flash->error(__('The room service could not be saved. Please, try again.'));
            }
            
        }
        $rooms = $this->RoomServices->Rooms->find('list');
        $services = $this->RoomServices->Services->find('list');
        $this->set(compact('roomService', 'rooms', 'services'));
        $this->set('_serialize', ['roomService']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Room Service id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($roomid = null, $serviceid = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        
        $query = TableRegistry::getTableLocator()->get('RoomServices');
        $roomService = $query
        ->find()
        ->where(['FK_room_id' => $roomid,'FK_service_id'=>$serviceid])
        ->first();


        $roomService = $this->RoomServices->get($id);
        if ($this->RoomServices->delete($roomService)) {
            $roomService="SD1";
            //$this->Flash->success(__('The room service has been deleted.'));
        } else {
            $roomService="ED1";
            //$this->Flash->error(__('The room service could not be deleted. Please, try again.'));
        }

        //return $this->redirect(['action' => 'index']);
    }
}

<?php
namespace App\Controller;
use \Datetime;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class RentingController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['mentors','rooms','books']);
        $this->loadComponent('RequestHandler');
    }

    public function index()
    {
       
        $renting = $this->paginate($this->Renting);

        $query = TableRegistry::getTableLocator()->get('Renting');
        $renting = $query
        ->find()
        ->where(['FK_user_id' => $this->Auth->user('PK_id')])
        ->contain(['Users', 'Mentors', 'Books', 'Rooms']);
      
        $this->set(compact('renting'));
        $this->set('_serialize', ['renting']); 
    }

    public function userIndex($id = null)
    {
        
        $query = TableRegistry::getTableLocator()->get('Renting');
        
        $urenting = $query
        ->find('all')
        ->where(['FK_user_id' => $this->request->getParam('id')])
        ->contain(['Users', 'Mentors', 'Books', 'Rooms']);
        
        $this->set(compact('urenting'));
        $this->set('_serialize', ['urenting']); 
    }

    /**
     * View method
     *
     * @param string|null $id Renting id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $renting = $this->Renting->get($id, [
            'contain' => ['Books','Mentors','Rooms', 'Users'],
        ]);
           
        $this->set('renting', $renting);
        $this->set('_serialize', ['renting']); 
    }

   
    public function add()
    {
        $renting = $this->Renting->newEntity();
        if ($this->request->is('post')) {
            
            $renting = $this->Renting->patchEntity($renting, $this->request->getData());
            
            if ($this->Renting->save($renting)) {
                $renting="SC1";
            }
            else{
                $renting="EC1";
            }
        }
        
        $this->set(compact('renting'));
        $this->set('_serialize', ['renting']); 
    }

    public function return($id = null)
    {
        $renting = $this->Renting->get($id, [
            'contain' => ['Books','Mentors','Rooms', 'Users'],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $renting = $this->Renting->patchEntity($renting, $this->request->getData());
            $renting->rent_returnDate=new DateTime('now');
            if ($this->Renting->save($renting)) 
            {
                $renting="SR1";
            }else{
                $renting="ER1";
            }
        }
        $this->set(compact('renting', 'books', 'users', 'mentors', 'rooms'));
        $this->set('_serialize', ['renting']); 
    }

    public function setIsPaid($id = null)
    {
        $renting = $this->Renting->get($id, [
            'contain' => ['Books','Mentors','Rooms', 'Users'],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $renting = $this->Renting->patchEntity($renting, $this->request->getData());
            $renting->is_paid=true;
            if ($this->Renting->save($renting)) 
            {
                $renting="SR1";
            }else{
                $renting="ER1";
            }
        }
        $this->set(compact('renting', 'books', 'users', 'mentors', 'rooms'));
        $this->set('_serialize', ['renting']); 
    }

    public function SelectedLoans()
    {
        $query = TableRegistry::getTableLocator()->get('Renting');
        $irenting = $query
        ->find()
        ->where(['FK_item_id' => $this->request->getData('PK_id'), 'item_type' => $this->request->getData('item_type'), 'rent_returnDate IS NULL'])
        ->all();

        $this->set(compact('irenting'));
        $this->set('_serialize', ['irenting']); 
    }
}

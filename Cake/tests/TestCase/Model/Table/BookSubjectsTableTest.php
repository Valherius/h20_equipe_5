<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BookSubjectsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BookSubjectsTable Test Case
 */
class BookSubjectsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\BookSubjectsTable
     */
    public $BookSubjects;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.BookSubjects',
        'app.Books',
        'app.Subjects',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('BookSubjects') ? [] : ['className' => BookSubjectsTable::class];
        $this->BookSubjects = TableRegistry::getTableLocator()->get('BookSubjects', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->BookSubjects);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

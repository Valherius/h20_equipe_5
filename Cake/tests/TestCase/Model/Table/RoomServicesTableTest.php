<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RoomServicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RoomServicesTable Test Case
 */
class RoomServicesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RoomServicesTable
     */
    public $RoomServices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.RoomServices',
        'app.Rooms',
        'app.Services',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('RoomServices') ? [] : ['className' => RoomServicesTable::class];
        $this->RoomServices = TableRegistry::getTableLocator()->get('RoomServices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->RoomServices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

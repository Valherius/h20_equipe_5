<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * MentorSkillsFixture
 */
class MentorSkillsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'FK_mentor_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'FK_skill_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'mentorskill_FK_skill_id_idx' => ['type' => 'index', 'columns' => ['FK_skill_id'], 'length' => []],
            'mentorskill_FK_mentor_id_idx' => ['type' => 'index', 'columns' => ['FK_mentor_id'], 'length' => []],
        ],
        '_constraints' => [
            'FK_mentor_id' => ['type' => 'foreign', 'columns' => ['FK_mentor_id'], 'references' => ['mentors', 'PK_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'FK_skill_id' => ['type' => 'foreign', 'columns' => ['FK_skill_id'], 'references' => ['skills', 'PK_id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'FK_mentor_id' => 1,
                'FK_skill_id' => 1,
            ],
        ];
        parent::init();
    }
}

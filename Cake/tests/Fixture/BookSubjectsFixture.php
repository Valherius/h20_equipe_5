<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * BookSubjectsFixture
 */
class BookSubjectsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'FK_book_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'FK_subject_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'booksubject_FK_book_id_idx' => ['type' => 'index', 'columns' => ['FK_book_id'], 'length' => []],
            'booksubject_FK_subject_id_idx' => ['type' => 'index', 'columns' => ['FK_subject_id'], 'length' => []],
        ],
        '_constraints' => [
            'FK_book_id' => ['type' => 'foreign', 'columns' => ['FK_book_id'], 'references' => ['books', 'PK_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'FK_subject_id' => ['type' => 'foreign', 'columns' => ['FK_subject_id'], 'references' => ['subjects', 'PK_id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'FK_book_id' => 1,
                'FK_subject_id' => 1,
            ],
        ];
        parent::init();
    }
}

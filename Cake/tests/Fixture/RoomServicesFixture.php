<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RoomServicesFixture
 */
class RoomServicesFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'FK_room_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'FK_service_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'roomservice_FK_service_id_idx' => ['type' => 'index', 'columns' => ['FK_room_id'], 'length' => []],
            'roomservice_FK_room_id_idx' => ['type' => 'index', 'columns' => ['FK_service_id'], 'length' => []],
        ],
        '_constraints' => [
            'FK_room_id' => ['type' => 'foreign', 'columns' => ['FK_room_id'], 'references' => ['rooms', 'PK_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'FK_service_id' => ['type' => 'foreign', 'columns' => ['FK_service_id'], 'references' => ['services', 'PK_id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'FK_room_id' => 1,
                'FK_service_id' => 1,
            ],
        ];
        parent::init();
    }
}

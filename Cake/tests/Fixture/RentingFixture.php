<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RentingFixture
 */
class RentingFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'renting';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'PK_rent_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'item_type' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'FK_item_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'FK_user_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'rent_beginDate' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'rent_endDate' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'rent_returnDate' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'fine' => ['type' => 'decimal', 'length' => 10, 'precision' => 0, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'renting_FK_user_id_idx' => ['type' => 'index', 'columns' => ['FK_user_id'], 'length' => []],
            'renting_FK_item_id_Book_idx' => ['type' => 'index', 'columns' => ['FK_item_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['PK_rent_id'], 'length' => []],
            'PK_rent_id' => ['type' => 'unique', 'columns' => ['PK_rent_id'], 'length' => []],
            'PK_rent_id_UNIQUE' => ['type' => 'unique', 'columns' => ['PK_rent_id'], 'length' => []],
            'renting_FK_item_id_Book' => ['type' => 'foreign', 'columns' => ['FK_item_id'], 'references' => ['books', 'PK_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'renting_FK_item_id_Mentor' => ['type' => 'foreign', 'columns' => ['FK_item_id'], 'references' => ['mentors', 'PK_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'renting_FK_item_id_Room' => ['type' => 'foreign', 'columns' => ['FK_item_id'], 'references' => ['rooms', 'PK_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'renting_FK_user_id' => ['type' => 'foreign', 'columns' => ['FK_user_id'], 'references' => ['users', 'PK_id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'PK_rent_id' => 1,
                'item_type' => 1,
                'FK_item_id' => 1,
                'FK_user_id' => 1,
                'rent_beginDate' => 'Lorem ipsum dolor sit amet',
                'rent_endDate' => 'Lorem ipsum dolor sit amet',
                'rent_returnDate' => 'Lorem ipsum dolor sit amet',
                'fine' => 1.5,
            ],
        ];
        parent::init();
    }
}
